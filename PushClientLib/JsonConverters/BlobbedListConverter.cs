﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PushClientLib.JsonConverters
{
    internal class BlobbedListConverter : JsonConverter
    {
        private const char SEPERATOR = ',';
        public override bool CanConvert(Type type)
        {
            return type is IList<string>;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value != null && reader.Value is string s)
            {
                return s.Split(SEPERATOR).ToList();
            }
            return new List<string>();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is IList<string> list)
            {
                string blobbed = string.Empty;
                foreach (string s in list)
                {
                    blobbed += s + SEPERATOR;
                }
                if (blobbed.Length > 0)
                {
                    blobbed = blobbed.Substring(0, blobbed.Length - 1);
                    writer.WriteValue(blobbed);
                    return;
                }
            }
            writer.WriteNull();
        }
    }
}
