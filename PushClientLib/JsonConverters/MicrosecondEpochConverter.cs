﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RokerPrime.Util;
using System;

namespace PushClientLib.JsonConverters
{
    public class MicrosecondEpochConverter : DateTimeConverterBase
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value is DateTime dateTime)
            {
                writer.WriteRawValue(DateTimeUtil.ToJavaTimeStamp(dateTime).ToString());
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value != null && reader.Value is long miliseconds)
            {
                return DateTimeUtil.FromJavaTimestamp(miliseconds).ToUniversalTime();
            }
            return null;
        }
    }
}
