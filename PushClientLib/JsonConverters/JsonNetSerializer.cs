﻿using Newtonsoft.Json;
using PushClientLib.Web.Models;
using RestSharp;
using RestSharp.Serialization;
using RokerPrime.Json;

namespace PushClientLib.JsonConverters
{
    public class JsonNetSerializer : IRestSerializer
    {
        public string Serialize(object obj) =>
            JsonConvert.SerializeObject(obj,
                            Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });

        public string Serialize(Parameter parameter)
        {

            var jsonResolver = new IgnorableSerializerContractResolver();
            // ignore your specific property
            jsonResolver.Ignore(typeof(BaseUserEntity), "WebSynced");
            jsonResolver.Ignore(typeof(AlbumList), "PreferedColumnCount");

            var jsonSettings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ContractResolver = jsonResolver };

            return JsonConvert.SerializeObject(parameter.Value,
                            Formatting.None,
                            jsonSettings);
        }


        public T Deserialize<T>(IRestResponse response) =>
            JsonConvert.DeserializeObject<T>(response.Content);

        public string[] SupportedContentTypes { get; } =
        {
            "application/json", "text/json", "text/x-json", "text/javascript", "*+json"
        };

        public string ContentType { get; set; } = "application/json";

        public DataFormat DataFormat { get; } = DataFormat.Json;
    }
}
