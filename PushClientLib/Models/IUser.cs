﻿using System;
using System.Collections.Generic;

namespace PushClientLib.Models
{
    public interface IUser : IBaseEntity
    {
        List<IFriendship> Friendships { get; set; }
        List<IFriendship> IncomingFriendshipRequests { get; set; }
        DateTime LastActive { get; set; }
        List<IFriendship> OutgoingFriendshipRequests { get; set; }
        int SavedAlbumCount { get; set; }
        string SpotifyId { get; set; }
        string UserName { get; set; }
    }
}