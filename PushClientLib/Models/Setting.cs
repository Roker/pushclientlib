﻿using SQLite;

namespace PushClientLib.Models
{
    public class Setting
    {
        [PrimaryKey]
        public string Key { get; set; }
        public string Value { get; set; }

        public Setting()
        {
        }

        public Setting(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
