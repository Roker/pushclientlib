﻿using AutoMapper;
using PushClientLib.SQLite.Models;
using PushClientLib.Web.Models;

namespace PushClientLib.Models
{
    internal static class ModelMapper
    {
        private static readonly IMapper _mapper = new MapperConfiguration(config =>
        {
            config.CreateMap<IAlbumList, SQLiteAlbumList>();
            config.CreateMap<IAlbumList, AlbumList>();

        }).CreateMapper();

        internal static IMapper Mapper => _mapper;
    }
}
