﻿using System;

namespace PushClientLib.Models
{
    public interface IBaseEntity
    {
        DateTime Created { get; set; }
        DateTime Updated { get; set; }
        string Id { get; set; }
    }
}