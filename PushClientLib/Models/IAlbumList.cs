﻿using System.Collections.Generic;

namespace PushClientLib.Models
{
    public interface IAlbumList : IBaseUserEntity
    {
        string Name { get; set; }
        List<string> SpotifyIds { get; set; }
        int Year { get; set; }
    }
}