﻿using System.Collections.Generic;

namespace PushClientLib.Models.EqualityComparers
{
    public class SameUserEntityEqualityComparer<T> : IEqualityComparer<T> where T : IBaseUserEntity
    {
        public bool Equals(T x, T y)
        {
            return x.Uuid == y.Uuid;
        }

        public int GetHashCode(T obj)
        {
            int hashCode = -201119940;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.Uuid);
            return hashCode;
        }

    }

}
