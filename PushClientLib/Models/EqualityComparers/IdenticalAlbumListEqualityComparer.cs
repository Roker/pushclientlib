﻿using System.Collections.Generic;

namespace PushClientLib.Models.EqualityComparers
{
    public class IdenticalAlbumListEqualityComparer : IEqualityComparer<IAlbumList>
    {
        public bool Equals(IAlbumList x, IAlbumList y)
        {
            return x.Uuid == y.Uuid &&
                x.Created == y.Created &&
                x.Updated == y.Updated &&
                x.UserId == y.UserId &&
                x.Visibility == y.Visibility &&
                x.Deleted == y.Deleted &&
                x.Name == y.Name &&
                EqualityComparer<List<string>>.Default.Equals(x.SpotifyIds, y.SpotifyIds) &&
                x.Year == y.Year;
        }

        public int GetHashCode(IAlbumList obj)
        {
            int hashCode = -201119940;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.Uuid);
            hashCode = hashCode * -1521134295 + obj.Created.GetHashCode();
            hashCode = hashCode * -1521134295 + obj.Updated.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.UserId);
            hashCode = hashCode * -1521134295 + obj.Visibility.GetHashCode();
            hashCode = hashCode * -1521134295 + obj.Deleted.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj.Name);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<string>>.Default.GetHashCode(obj.SpotifyIds);
            hashCode = hashCode * -1521134295 + obj.Year.GetHashCode();
            return hashCode;
        }

    }

}
