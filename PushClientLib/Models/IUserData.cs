﻿namespace PushClientLib.Models
{
    public interface IUserData
    {
        string AuthToken { get; }
        IUser CurrentUser { get; }
    }
}