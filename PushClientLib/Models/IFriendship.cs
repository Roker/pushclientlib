﻿namespace PushClientLib.Models
{
    public interface IFriendship
    {
        bool Accepted { get; set; }
        string ReceiverUuid { get; set; }
        string SenderUuid { get; set; }
    }
}