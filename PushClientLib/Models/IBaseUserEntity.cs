﻿namespace PushClientLib.Models
{
    public interface IBaseUserEntity : IBaseEntity
    {
        string Uuid { get; set; }
        int Version { get; set; }
        string UserId { get; set; }
        Visibility Visibility { get; set; }
        bool Deleted { get; set; }
    }
    public enum Visibility
    {
        PRIVATE,
        FRIENDS,
        PUBLIC
    }
}