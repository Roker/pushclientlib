﻿using System;

namespace PushClientLib.Exceptions
{
    internal class CachingRepositoryConflictException<T> : Exception
    {
        private readonly T _conflicting;

        public CachingRepositoryConflictException(T conflicting)
        {
            _conflicting = conflicting;
        }

        public T ConflictingEntity { get; internal set; }
    }
}
