﻿using PushClientLib.Models;
using PushClientLib.Repositories;
using PushClientLib.SQLite.Models;
using SQLite;
using System;

namespace PushClientLib.SQLite.Repositories
{
    public static class UserEntitySQLiteRepositoryFactory
    {
        //public static IBaseUserEntityRepository<T> NewBaseUserEntityRepo<T>(SQLiteAsyncConnection con) where T : IBaseUserEntity
        //{
        //    if (typeof(T) == typeof(IAlbumList))
        //    {
        //        return (IBaseUserEntityRepository<T>)new BaseMappedUserEntityRepository<IAlbumList, SQLiteAlbumList>(
        //            new BaseSQLiteAsyncUserEntityRepository<SQLiteAlbumList>(con));
        //    }
        //    throw new Exception("type not found");

        //}

        internal static IAsyncUserEntityCachingRepository<T> NewUserEntitySQLiteCachingRepository<T>(SQLiteAsyncConnection con) where T : IBaseUserEntity
        {
            if (typeof(T) == typeof(IAlbumList))
            {
                return (IAsyncUserEntityCachingRepository<T>)new MappedUserEntityCachingRepository<IAlbumList, SQLiteAlbumList>(
                    new SQLiteAsyncUserEntityCachingRepository<SQLiteAlbumList>(
                        con));
            }

            //TODO: better error
            throw new Exception("type not found");
        }

    }
}
