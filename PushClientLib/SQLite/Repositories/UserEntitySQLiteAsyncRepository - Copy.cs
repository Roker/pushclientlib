﻿//using PushClientLib.Models;
//using PushClientLib.Repositories;
//using PushClientLib.SQLite.Models;
//using SQLite;
//using SQLiteNetExtensionsAsync.Extensions;
//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Threading.Tasks;

namespace PushClientLib.SQLite.Repositories
{
//namespace PushClientLib.SQLite.Repositories
//{
//    internal class UserEntitySQLiteAsyncRepository<T> : IAsyncUserEntityRepository<T> where T : SQLiteBaseUserEntity, new()
//    {
//        private readonly SQLiteAsyncConnection _connection;

//        protected SQLiteAsyncConnection Connection => _connection;

//        public UserEntitySQLiteAsyncRepository(SQLiteAsyncConnection connection)
//        {
//            _connection = connection;
//            _connection.CreateTableAsync<T>().Wait();
//        }

//        public virtual async Task<long> GetTotalAsync()
//        {
//            return (long)await _connection.Table<T>().CountAsync(x => !x.Deleted).ConfigureAwait(false);
//        }

//        public virtual async Task<T> AddAsync(T t)
//        {
//            await _connection.InsertAsync(t).ConfigureAwait(false);
//            return t;
//        }

//        public virtual async Task<List<T>> GetRangeAsync(int offset, int limit)
//        {
//            return await _connection.Table<T>().Skip(offset).Take(limit).ToListAsync().ConfigureAwait(false);
//        }

//        public virtual Task<T> GetAsync(string uuid)
//        {
//            return _connection.GetAsync<T>(uuid);
//        }

//        public virtual Task<List<T>> GetAsync(IEnumerable<string> uuids)
//        {
//            return _connection.GetAllWithChildrenAsync<T>(x => uuids.Contains(x.Uuid));
//        }

//        public virtual async Task<List<T>> GetAllAsync()
//        {
//            return await _connection.GetAllWithChildrenAsync<T>().ConfigureAwait(false);
//        }
   

//        public virtual async Task<T> DeleteAsync(T t)
//        {
//            await _connection.DeleteAsync(t).ConfigureAwait(false);
//            return t;
//        }

//        public virtual async Task<T> UpdateAsync(T t)
//        {
//            await _connection.InsertOrReplaceAsync(t).ConfigureAwait(false);
//            return t;
//        }

//        public virtual async Task<List<T>> UpdateAsync(IEnumerable<T> ts)
//        {
//            await _connection.UpdateAllAsync(ts).ConfigureAwait(false);
//            return ts.ToList();
//        }

//        public Task<List<T>> GetAsync(Expression<Func<T, bool>> expr)
//        {
//            return _connection.Table<T>().Where(expr).ToListAsync();
//        }

//        public async Task<List<T>> GetAddedSinceAsync(DateTime since)
//        {
//            return (await _connection.GetAllWithChildrenAsync<T>(x => x.Created > since && x.Deleted == false).ConfigureAwait(false)).Cast<T>().ToList();
//        }

//        public async Task<List<T>> GetUpdatedSinceAsync(DateTime since)
//        {
//            return (await _connection.GetAllWithChildrenAsync<T>(x => x.Updated > since && x.Created < since && x.Deleted == false).ConfigureAwait(false)).Cast<T>().ToList();
//        }

//        public async Task<List<T>> GetDeletedSinceAsync(DateTime since)
//        {
//            return (await _connection.GetAllWithChildrenAsync<T>(x => x.Updated > since && x.Deleted == true).ConfigureAwait(false)).Cast<T>().ToList();
//        }
//    }
//}
}