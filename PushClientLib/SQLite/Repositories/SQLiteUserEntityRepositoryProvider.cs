﻿using PushClientLib.Models;
using PushClientLib.Repositories;
using SQLite;

namespace PushClientLib.SQLite.Repositories
{
    internal class SQLiteUserEntityRepositoryProvider<T> where T : IBaseUserEntity
    {
        private IAsyncUserEntityRepository<T> _asyncUserEntityRepository;
        private IAsyncUserEntityCachingRepository<T> _cachingUserEntityRepository;

        public SQLiteUserEntityRepositoryProvider(SQLiteAsyncConnection con)
        {
            _cachingUserEntityRepository = UserEntitySQLiteRepositoryFactory.NewUserEntitySQLiteCachingRepository<T>(con);
            _asyncUserEntityRepository = new UserEntityRepository<T>(_cachingUserEntityRepository);
        }

        public IAsyncUserEntityRepository<T> UserEntityRepository { get => _asyncUserEntityRepository; }

        public IAsyncUserEntityCachingRepository<T> CachingUserEntityRepository { get => _cachingUserEntityRepository; }
    }
}
