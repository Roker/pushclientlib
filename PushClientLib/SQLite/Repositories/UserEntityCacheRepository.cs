﻿//using PushClientLib.Models;
//using PushClientLib.Repositories;
//using SQLite;
//using SQLiteNetExtensionsAsync.Extensions;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace PushClientLib.SQLite.Repositories
{
//namespace PushClientLib.SQLite.Repositories
//{
//    public class UserEntityCacheRepository<T> : IAsyncUserEntityCachingRepository<T> where T : IBaseUserEntity
//    {
//        private IAsyncUserEntityRepository<T> _baseRepo;

//        public UserEntityCacheRepository(IAsyncUserEntityRepository<T> baseRepo)
//        {
//            _baseRepo = baseRepo;
//        }

//        public async Task CacheAsync(IEnumerable<T> ts)
//        {
//            foreach(T t in ts)
//            {
//                await _baseRepo.UpdateAsync(t).ConfigureAwait(false);
//            }
//        }

//        public async Task UncacheAsync(IEnumerable<T> ts)
//        {
//            foreach (T t in ts)
//            {
//                await _baseRepo.DeleteAsync(t).ConfigureAwait(false);
//            }
//        }

//        public Task<T> AddAsync(T t)
//        {
//            t.Created = DateTime.UtcNow;
//            t.Updated = DateTime.UtcNow;
//            return _baseRepo.AddAsync(t);
//        }
//        public Task<T> UpdateAsync(T t)
//        {
//            t.Updated = DateTime.UtcNow;
//            return _baseRepo.UpdateAsync(t);
//        }
//        public Task<T> DeleteAsync(T t)
//        {
//            t.Deleted = true;
//            return UpdateAsync(t);
//        }

//        public Task<List<T>> GetAddedSinceAsync(DateTime since)
//        {
//            return _baseRepo.GetAddedSinceAsync(since);
//        }

//        public Task<List<T>> GetAllAsync()
//        {
//            return _baseRepo.GetAllAsync();
//        }

//        public Task<T> GetAsync(string uuid)
//        {
//            return _baseRepo.GetAsync(uuid);
//        }

//        public Task<List<T>> GetDeletedSinceAsync(DateTime since)
//        {
//            return _baseRepo.GetDeletedSinceAsync(since);
//        }

//        public Task<List<T>> GetRangeAsync(int offset = 0, int limit = 20)
//        {
//            return _baseRepo.GetRangeAsync(offset, limit);
//        }

//        public Task<long> GetTotalAsync()
//        {
//            return _baseRepo.GetTotalAsync();
//        }

//        public Task<List<T>> GetUpdatedSinceAsync(DateTime since)
//        {
//            return _baseRepo.GetUpdatedSinceAsync(since);
//        }


//    }
//}
}