﻿using PushClientLib.Repositories;
using PushClientLib.SQLite.Models;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.SQLite.Repositories
{
    internal class SQLiteAsyncUserEntityRepository<T> : IAsyncUserEntityRepository<T> where T : SQLiteBaseUserEntity, new()
    {
        private readonly SQLiteAsyncConnection _connection;

        public event EventHandler<T> EntityAdded;
        public event EventHandler<T> EntityDeleted;
        public event EventHandler<T> EntityUpdated;

        internal SQLiteAsyncUserEntityRepository(SQLiteAsyncConnection connection)
        {
            _connection = connection;
            _connection.CreateTableAsync<T>().Wait();
        }

        public virtual async Task<T> AddAsync(T t)
        {
            EntityAdded?.Invoke(this, t);
            await _connection.InsertOrReplaceAsync(t).ConfigureAwait(false);
            return t;
        }

        public virtual async Task DeleteAsync(T t)
        {
            EntityDeleted?.Invoke(this, t);
            await _connection.DeleteAsync(t).ConfigureAwait(false);
        }

        public virtual async Task<T> UpdateAsync(T t)
        {
            EntityUpdated?.Invoke(this, t);
            await _connection.UpdateAsync(t).ConfigureAwait(false);
            return t;
        }

        public virtual Task<List<T>> GetAllAsync()
        {
            return _connection.GetAllWithChildrenAsync<T>();
        }

        public virtual Task<T> GetAsync(string uuid)
        {
            return _connection.GetAsync<T>(uuid);
        }

        public virtual Task<List<T>> GetRangeAsync(int offset = 0, int limit = 20)
        {
            return _connection.Table<T>().Skip(offset).Take(limit).ToListAsync();

        }

        public virtual async Task<long> GetTotalAsync()
        {
            return await _connection.Table<T>().CountAsync(x => x.Deleted == false).ConfigureAwait(false);
        }


    }
}
