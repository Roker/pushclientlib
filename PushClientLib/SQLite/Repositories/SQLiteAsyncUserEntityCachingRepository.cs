﻿using PushClientLib.Repositories;
using PushClientLib.SQLite.Models;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushClientLib.SQLite.Repositories
{
    internal class SQLiteAsyncUserEntityCachingRepository<T> : SQLiteAsyncUserEntityRepository<T>,
        IAsyncUserEntityCachingRepository<T>
        where T : SQLiteBaseUserEntity, new()
    {
        private readonly SQLiteAsyncConnection _connection;

        protected SQLiteAsyncConnection Connection => _connection;

        public SQLiteAsyncUserEntityCachingRepository(SQLiteAsyncConnection connection) : base(connection)
        {
            _connection = connection;
        }

        public async Task<List<T>> GetAddedSinceAsync(DateTime since)
        {
            return (await _connection.GetAllWithChildrenAsync<T>(x => x.Created >= since && x.Deleted == false).ConfigureAwait(false)).Cast<T>().ToList();
        }

        public async Task<List<T>> GetUpdatedSinceAsync(DateTime since)
        {
            return (await _connection.GetAllWithChildrenAsync<T>(x => x.Updated >= since && x.Created < since && x.Deleted == false).ConfigureAwait(false)).Cast<T>().ToList();
        }

        public async Task<List<T>> GetDeletedSinceAsync(DateTime since)
        {
            return (await _connection.GetAllWithChildrenAsync<T>(x => x.Updated >= since && x.Deleted == true).ConfigureAwait(false)).Cast<T>().ToList();
        }

    }
}
