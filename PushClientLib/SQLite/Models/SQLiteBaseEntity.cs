﻿using PushClientLib.Models;
using SQLite;
using System;

namespace PushClientLib.SQLite.Models
{
    internal abstract class SQLiteBaseEntity : IBaseEntity
    {
        [Unique]
        public string Id { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;
        public DateTime Updated { get; set; } = DateTime.UtcNow;
    }
}
