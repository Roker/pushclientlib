﻿using PushClientLib.Models;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace PushClientLib.SQLite.Models
{
    internal class SQLiteAlbumList : SQLiteBaseUserEntity, IAlbumList
    {
        private List<string> _spotifyIds;

        public string Name { get; set; }

        [TextBlob(nameof(SpotifyIdsBlobbed))]
        public List<string> SpotifyIds {
            get {
                if (_spotifyIds == null)
                {
                    return new List<string>();
                }
                return _spotifyIds;
            }
            set {
                _spotifyIds = value;
            }
        }
        public string SpotifyIdsBlobbed { get; set; }
        public int Year { get; set; }

    }
}
