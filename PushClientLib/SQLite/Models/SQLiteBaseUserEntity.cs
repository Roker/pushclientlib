﻿using PushClientLib.Models;
using SQLite;

namespace PushClientLib.SQLite.Models
{
    internal abstract class SQLiteBaseUserEntity : SQLiteBaseEntity, IBaseUserEntity
    {
        [PrimaryKey]
        public string Uuid { get; set; }
        public int Version { get; set; }
        public string UserId { get; set; }
        public Visibility Visibility { get; set; }
        public bool Deleted { get; set; }
    }
}
