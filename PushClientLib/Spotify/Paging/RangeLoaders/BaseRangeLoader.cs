﻿using PushClientLib.CollectionLoading;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Paging.RangeLoaders
{
    public abstract class BaseRangeLoader<T> : IRangeLoader<T>
    {
        private Func<int, int, Task<IEnumerable<T>>> _rangeLoadingFunc;
        private Func<Task<int>> _getTotalFunc;

        public event EventHandler<NotifyCollectionChangedEventArgs> CollectionChanged;

        public BaseRangeLoader(Func<int, int, Task<IEnumerable<T>>> rangeLoadingFunc, Func<Task<int>> getTotalFunc)
        {
            _rangeLoadingFunc = rangeLoadingFunc;
            _getTotalFunc = getTotalFunc;
        }

        public abstract void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args);

        public Task<int> GetTotalAsync()
        {
            return _getTotalFunc.Invoke();
        }

        public Task<IEnumerable<T>> LoadRange(int startOffset, int limit)
        {
            return _rangeLoadingFunc.Invoke(startOffset, limit);
        }

        public abstract void AddItem(T o);
        public abstract void InsertItem(int index, T o);
        public abstract bool RemoveItem(T o);
        public abstract void MoveItem(int oldIndex, int newIndex);
    }
}
