﻿using PushClientLib.Spotify.Models;
using PushClientLib.Spotify.Repositories;
using PushClientLib.Spotify.Services.SpotifyAlbums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services.SpotifySavedAlbums
{
    public static class SpotifySavedAlbumServiceExtensions
    {
        public static async Task<List<ISpotifyAlbum>> GetAsSpotifyAlbumsAsync(
            this ISpotifySavedAlbumService spotifySavedAlbumService, ISpotifyAlbumService spotifyAlbumService,
            int offset = 0, int limit = 20)
        {
            IEnumerable<ISpotifySavedAlbum> savedAlbums = await spotifySavedAlbumService.GetAsync(offset: offset, limit: limit).ConfigureAwait(false);
            return await spotifyAlbumService.SpotifyAlbumsFromSavedAlbumsAsync(savedAlbums).ConfigureAwait(false);
        }

        public static async Task<IPage<ISpotifyAlbum>> GetPageAsSpotifyAlbumsAsync(
            this ISpotifySavedAlbumService spotifySavedAlbumService, ISpotifyAlbumService spotifyAlbumService,
            int offset = 0, int limit = 20)
        {
            IPage<ISpotifySavedAlbum> savedAlbumPage =
                await spotifySavedAlbumService.GetPageAsync(offset: offset, limit: limit).ConfigureAwait(false);

            Console.WriteLine("AsPage Page: " + savedAlbumPage);

            IEnumerable<ISpotifyAlbum> spotifyAlbums =
                await spotifyAlbumService.GetAsync(savedAlbumPage.Items.Select(x => x.SpotifyAlbumId).ToList()).ConfigureAwait(false);
            return new SpotifySavedAlbumAsSpotifyAlbumPage(savedAlbumPage, spotifyAlbums, spotifyAlbumService);
        }
    }
}
