﻿using PushClientLib.Spotify.Models;
using PushClientLib.Spotify.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services.SpotifySavedAlbums
{
    internal class SpotifySavedAlbumAsSpotifyAlbumPage : IPage<ISpotifyAlbum>
    {
        private IPage<ISpotifySavedAlbum> _savedAlbumPage;
        private IEnumerable<ISpotifyAlbum> _spotifyAlbums;
        private ISpotifyAlbumService _spotifyAlbumService;

        public SpotifySavedAlbumAsSpotifyAlbumPage(IPage<ISpotifySavedAlbum> savedAlbumPage, IEnumerable<ISpotifyAlbum> spotifyAlbums, ISpotifyAlbumService spotifyAlbumService)
        {
            _savedAlbumPage = savedAlbumPage;
            _spotifyAlbums = spotifyAlbums;
            _spotifyAlbumService = spotifyAlbumService;
        }

        public IEnumerable<ISpotifyAlbum> Items => _spotifyAlbums;

        public int Total => _savedAlbumPage.Total;

        public int Offset => _savedAlbumPage.Offset;

        public int Limit => _savedAlbumPage.Limit;

        public async Task<IPage<ISpotifyAlbum>> GetNextPageAsync()
        {
            Console.WriteLine("SpotifySavedAlbumAsSpotifyAlbumPage - GET NEXT PAGE");
            Console.WriteLine("Offset: " + Offset);
            Console.WriteLine("Limit: " + Limit);
            IPage<ISpotifySavedAlbum> page = await _savedAlbumPage.GetNextPageAsync().ConfigureAwait(false);
            Console.WriteLine("NewOffset: " + page.Offset);
            Console.WriteLine("NewLimit: " + page.Limit);

            IEnumerable<ISpotifyAlbum> list = await _spotifyAlbumService.GetAsync(page.Items.Select(x => x.SpotifyAlbumId).ToList()).ConfigureAwait(false);
            Console.WriteLine("SpotifySavedAlbumAsSpotifyAlbumPage - got ISpotifyAlbum list" + list.Count());


            return new SpotifySavedAlbumAsSpotifyAlbumPage(page, list, _spotifyAlbumService);
        }

        public async Task<IPage<ISpotifyAlbum>> GetPreviousPageAsync()
        {
            IPage<ISpotifySavedAlbum> page = await _savedAlbumPage.GetPreviousPageAsync().ConfigureAwait(false);
            IEnumerable<ISpotifyAlbum> list = await _spotifyAlbumService.GetAsync(_savedAlbumPage.Items.Select(x => x.SpotifyAlbumId).ToList()).ConfigureAwait(false);
            return new SpotifySavedAlbumAsSpotifyAlbumPage(page, list, _spotifyAlbumService);
        }

        public bool HasNextPage()
        {
            return _savedAlbumPage.HasNextPage();
        }

        public bool HasPreviousPage()
        {
            return _savedAlbumPage.HasPreviousPage();
        }
    }
}