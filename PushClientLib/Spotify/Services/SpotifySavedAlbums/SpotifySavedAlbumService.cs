﻿using PushClientLib.Spotify.Models;
using PushClientLib.Spotify.Repositories;
using PushClientLib.Spotify.Repositories.SpotifySavedAlbums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services.SpotifySavedAlbums
{
    public class SpotifySavedAlbumService : ISpotifySavedAlbumService
    {
        private ISpotifySavedAlbumReader _savedAlbumReader;
        public SpotifySavedAlbumService(ISpotifySavedAlbumReader savedAlbumreader)
        {
            _savedAlbumReader = savedAlbumreader;
        }

        public int Limit => _savedAlbumReader.Limit;

        public Task<IEnumerable<bool>> CheckAsync(IEnumerable<string> ids)
        {
            return _savedAlbumReader.CheckAsync(ids);
        }

        public Task<IPage<ISpotifySavedAlbum>> GetPageAsync(int offset = 0, int limit = 20)
        {
            return _savedAlbumReader.GetPageAsync(offset: offset, limit: limit);
        }

        public Task<IEnumerable<ISpotifySavedAlbum>> GetAsync(int offset = 0, int limit = 20)
        {
            return _savedAlbumReader.GetAsync(offset: offset, limit: limit);
        }

        public Task<int> GetTotalAsync()
        {
            return _savedAlbumReader.GetTotalAsync();
        }

        //public IPageThree<ISpotifySavedAlbum> GetLoadedPage()
        //{
        //    return _savedAlbumReader.GetLoadedPage();
        //}
    }
}
