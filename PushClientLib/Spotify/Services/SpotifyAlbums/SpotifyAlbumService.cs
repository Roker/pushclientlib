﻿//using PushClientLib.Spotify.Models;
//using PushClientLib.Spotify.Repositories;
//using PushClientLib.Spotify.Repositories.SpotifyAlbums;
//using SpotifyAPI.Web.Models;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services.SpotifyAlbums
{
//namespace PushClientLib.Spotify.Services.SpotifyAlbums
//{
//    public class SpotifyAlbumService : ISpotifyAlbumService
//    {
//        private ISpotifyAlbumReader _reader;

//        private ISpotifyWebService _spotifyWebService;

//        public SpotifyAlbumService(ISpotifyAlbumReader reader, ISpotifyWebService spotifyWebService)
//        {
//            _reader = reader;
//            _spotifyWebService = spotifyWebService;
//        }

//        public Task<ISpotifyAlbum> GetAsync(string id)
//        {
//            return _reader.GetAsync(id);
//        }

//        public Task<IEnumerable<ISpotifyAlbum>> GetAsync(IEnumerable<string> ids)
//        {
//            return _reader.GetAsync(ids);
//        }

//        public async Task<IPage<ISpotifyAlbum>> GetArtistAlbumsAsync(string id, int offset, int limit = 20)
//        {
//            Paging<SimpleAlbum> paging = await _spotifyWebService.GetArtistsAlbumsAsync(id).ConfigureAwait(false);
//            return new ArtistAlbumsPage(this, paging, id);
//        }

//        public async Task<IPage<ISpotifyAlbum>> SearchAlbumsAsync(string text, int year = 0, int offset = 0, int limit = 20)
//        {
//            if (string.IsNullOrEmpty(text))
//            {
//                return new EmptyPage<ISpotifyAlbum>();
//            }
//            SearchItem searchItem = await _spotifyWebService.SearchAlbumsAsync(
//                    text, limit: limit, offset: offset, year: year).ConfigureAwait(false);
      
//            return new SpotifyAlbumSearchPage(this, searchItem, text, year);
//        }

//        //public IPageThree<ISpotifyAlbum> SearchAlbumsPage(string text, int year = 0)
//        //{
//        //    return new BaseSpotifyPageThree<ISpotifyAlbum>(
//        //        new PageRangeLoader<ISpotifyAlbum>((lim, off) => SearchAlbumsAsync(text, year, lim, off)));
//        //}

//        //public IPageThree<ISpotifyAlbum> GetArtistAlbumsPage(string id)
//        //{
//        //    throw new NotImplementedException();
//        //}

//        //private class SpotifyAlbumSearchPage : IPage<ISpotifyAlbum>
//        //{
//        //    private ISpotifyAlbumService _spotifyAlbumService;
//        //    private int _offset, _limit, _year;
//        //    private string _searchText;

//        //    public SpotifyAlbumSearchPage(
//        //        ISpotifyAlbumService spotifyAlbumService, SearchItem searchItem, int year, string searchText)
//        //    {
//        //        _spotifyAlbumService = spotifyAlbumService;
//        //        _offset = searchItem.Albums.Offset;
//        //        _limit = searchItem.Albums.Limit;
//        //        Total = searchItem.Albums.Total;
//        //        Items = SpotifyMapper.SimpleAlbumsToSpotifyAlbums(searchItem.Albums.Items);
//        //        _year = year;
//        //        _searchText = searchText;
//        //    }

//        //    public List<ISpotifyAlbum> Items { get; private set; }

//        //    public int Total { get; private set; }

//        //    public Task<IPage<ISpotifyAlbum>> GetNextPageAsync()
//        //    {
//        //        return _spotifyAlbumService.SearchAlbumsAsync(_searchText, _offset + _limit, _limit, _year);
//        //    }

//        //    public Task<IPage<ISpotifyAlbum>> GetPreviousPageAsync()
//        //    {
//        //        return _spotifyAlbumService.SearchAlbumsAsync(_searchText, _offset - _limit, _limit, _year);
//        //    }

//        //    public bool HasNextPage()
//        //    {
//        //        return _offset + _limit < Total;
//        //    }

//        //    public bool HasPreviousPage()
//        //    {
//        //        return _offset > 0;

//        //    }
//        //}
//    }
//}
}