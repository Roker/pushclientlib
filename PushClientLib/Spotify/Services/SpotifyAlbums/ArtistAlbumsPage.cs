﻿using PushClientLib.Spotify.Models;
using PushClientLib.Spotify.Repositories;
using SpotifyAPI.Web.Models;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services.SpotifyAlbums
{
    internal class ArtistAlbumsPage : BaseSpotifyPage<ISpotifyAlbum>
    {
        private ISpotifyAlbumService _spotifyAlbumService;
        private string _artistId;

        internal ArtistAlbumsPage(ISpotifyAlbumService spotifyAlbumService, Paging<SimpleAlbum> paging, string artistId)
            : base(paging.Offset, paging.Limit, paging.Total, SpotifyMapper.SimpleAlbumsToSpotifyAlbums(paging.Items))
        {
            _spotifyAlbumService = spotifyAlbumService;
            _artistId = artistId;
        }

        public override Task<IPage<ISpotifyAlbum>> GetPage(int offset, int limit)
        {
            return _spotifyAlbumService.GetArtistAlbumsAsync(_artistId, offset: offset, limit: limit);
        }
    }
}
