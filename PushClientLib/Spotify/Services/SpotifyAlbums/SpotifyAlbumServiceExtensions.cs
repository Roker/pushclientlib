﻿using PushClientLib.Spotify.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services.SpotifyAlbums
{
    public static class SpotifyAlbumServiceExtensions
    {
        public static async Task<List<ISpotifyAlbum>> SpotifyAlbumsFromSavedAlbumsAsync(
            this ISpotifyAlbumService albumService, IEnumerable<ISpotifySavedAlbum> savedAlbums)
        {
            List<string> idsToRetrieve = new List<string>();
            foreach (ISpotifySavedAlbum spotifySavedAlbum in savedAlbums)
            {
                if (spotifySavedAlbum.SpotifyAlbum == null)
                {
                    idsToRetrieve.Add(spotifySavedAlbum.SpotifyAlbumId);
                }
            }
            IEnumerable<ISpotifyAlbum> retrievedAlbums = await albumService.GetAsync(idsToRetrieve).ConfigureAwait(false);

            List<ISpotifyAlbum> spotifyAlbums = new List<ISpotifyAlbum>();
            foreach (ISpotifySavedAlbum spotifySavedAlbum in savedAlbums)
            {
                if (spotifySavedAlbum.SpotifyAlbum != null)
                {
                    spotifyAlbums.Add(spotifySavedAlbum.SpotifyAlbum);
                }
                else
                {
                    spotifyAlbums.Add(retrievedAlbums.First(x => x.Id == spotifySavedAlbum.SpotifyAlbumId));
                }
            }
            return spotifyAlbums;
        }
    }
}
