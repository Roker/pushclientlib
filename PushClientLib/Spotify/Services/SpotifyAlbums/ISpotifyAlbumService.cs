﻿using PushClientLib.Spotify.Models;
using PushClientLib.Spotify.Repositories;
using PushClientLib.Spotify.Repositories.SpotifyAlbums;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services.SpotifyAlbums
{
    public interface ISpotifyAlbumService : ISpotifyAlbumReader
    {
        //string Market { get; }

        //IPageThree<ISpotifyAlbum> SearchAlbumsPage(string text, int year = 0);
        //IPageThree<ISpotifyAlbum> GetArtistAlbumsPage(string id);

        Task<IPage<ISpotifyAlbum>> SearchAlbumsAsync(string text, int year = 0, int offset = 0, int limit = 20);
        Task<IPage<ISpotifyAlbum>> GetArtistAlbumsAsync(string id, int offset = 0, int limit = 20);
    }
}
