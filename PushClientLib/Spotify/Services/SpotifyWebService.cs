﻿using SpotifyAPI.Web;
using SpotifyAPI.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services
{
    public class SpotifyWebService : ISpotifyWebService
    {
        public const int DEFAULT_LIMIT = 20;

        private ISpotifyTokenProvider _tokenProvider;

        private static string _market;

        public string Market => _market;

        public SpotifyWebService(ISpotifyTokenProvider tokenProvider)
        {
            _tokenProvider = tokenProvider;
        }

        public ListResponse<bool> CheckSavedAlbums(IEnumerable<string> ids)
        {
            return GetApi().CheckSavedAlbums(ids.ToList());
        }

        public async Task<ListResponse<bool>> CheckSavedAlbumsAsync(IEnumerable<string> ids)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).CheckSavedAlbumsAsync(ids.ToList()).ConfigureAwait(false);
        }

        public FullAlbum GetAlbum(string id)
        {
            return GetApi().GetAlbum(id);
        }

        public async Task<FullAlbum> GetAlbumAsync(string id)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetAlbumAsync(id).ConfigureAwait(false);
        }

        public List<FullAlbum> GetAlbums(IEnumerable<string> ids)
        {
            return GetApi().GetSeveralAlbums(ids.ToList()).Albums;
        }

        public async Task<SeveralAlbums> GetAlbumsAsync(IEnumerable<string> ids)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetSeveralAlbumsAsync(ids.ToList()).ConfigureAwait(false);
        }

        public FullArtist GetArtist(string id)
        {
            return GetApi().GetArtist(id);
        }
        public async Task<FullArtist> GetArtistAsync(string id)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetArtistAsync(id).ConfigureAwait(false);
        }
        public Paging<SimpleAlbum> GetArtistAlbums(string id)
        {
            return GetApi().GetArtistsAlbums(id);
        }

        public async Task<Paging<SimpleAlbum>> GetArtistsAlbumsAsync(string id)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetArtistsAlbumsAsync(id).ConfigureAwait(false);
        }

        public FollowedArtists GetFollowedArtists(string after = "", int limit = 20)
        {
            return GetApi().GetFollowedArtists(SpotifyAPI.Web.Enums.FollowType.Artist, after: after, limit: limit);
        }

        public async Task<FollowedArtists> GetFollowedArtistsAsync(string after = "", int limit = 20)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetFollowedArtistsAsync(SpotifyAPI.Web.Enums.FollowType.Artist, after: after, limit: limit).ConfigureAwait(false);
        }

        public NewAlbumReleases GetNewAlbumReleases(int offset = 0, int limit = 20)
        {
            return GetApi().GetNewAlbumReleases(offset: offset, limit: limit);
        }

        public async Task<NewAlbumReleases> GetNewAlbumReleasesAsync(int offset = 0, int limit = 20)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetNewAlbumReleasesAsync(offset: offset, limit: limit, country: Market).ConfigureAwait(false);
        }

        public PrivateProfile GetPrivateProfile()
        {
            return GetApi().GetPrivateProfile();
        }

        public async Task<PrivateProfile> GetPrivateProfileAsync()
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetPrivateProfileAsync().ConfigureAwait(false);
        }

        public CursorPaging<PlayHistory> GetRecentlyPlayedTracks(int limit = 20, DateTime? after = null, DateTime? before = null)
        {
            return GetApi().GetUsersRecentlyPlayedTracks(limit: limit, after: after, before: before);
        }

        public async Task<CursorPaging<PlayHistory>> GetRecentlyPlayedTracksAsync(int limit = 20, DateTime? after = null, DateTime? before = null)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetUsersRecentlyPlayedTracksAsync(limit: limit, after: after, before: before).ConfigureAwait(false);
        }

        public Paging<SavedAlbum> GetSavedAlbums(int offset = 0, int limit = 20)
        {
            return GetApi().GetSavedAlbums(offset: offset, limit: limit);
        }

        public async Task<Paging<SavedAlbum>> GetSavedAlbumsAsync(int offset = 0, int limit = 20)
        {
            try
            {
                return await (await GetApiAsync().ConfigureAwait(false)).GetSavedAlbumsAsync(offset: offset, limit: limit).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public SearchItem SearchAlbums(string text, int offset = 0, int limit = 20, int year = 0)
        {
            return GetApi().SearchItemsEscaped(
                PrepareSearchText(text, year), SpotifyAPI.Web.Enums.SearchType.Album,
                limit: limit, offset: offset, market: Market);
        }
        public async Task<SearchItem> SearchAlbumsAsync(string text, int offset = 0, int limit = 20, int year = 0)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).SearchItemsEscapedAsync(
                PrepareSearchText(text, year), SpotifyAPI.Web.Enums.SearchType.Album,
                limit: limit, offset: offset, market: Market);

        }
        public SearchItem SearchArtists(string text, int offset = 0, int limit = 20, int year = 0)
        {
            return GetApi().SearchItemsEscaped(
                PrepareSearchText(text, year), SpotifyAPI.Web.Enums.SearchType.Artist,
                limit: limit, offset: offset, market: Market);
        }
        public async Task<SearchItem> SearchArtistsAsync(string text, int offset = 0, int limit = 20, int year = 0)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).SearchItemsEscapedAsync(
                PrepareSearchText(text, year), SpotifyAPI.Web.Enums.SearchType.Artist,
                limit: limit, offset: offset, market: Market);
        }
        public SearchItem SearchTracks(string text, int offset = 0, int limit = 20, int year = 0)
        {
            return GetApi().SearchItemsEscaped(
                PrepareSearchText(text, year), SpotifyAPI.Web.Enums.SearchType.Track,
                limit: limit, offset: offset, market: Market);
        }
        public async Task<SearchItem> SearchTracksAsync(string text, int offset = 0, int limit = 20, int year = 0)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).SearchItemsEscapedAsync(
                PrepareSearchText(text, year), SpotifyAPI.Web.Enums.SearchType.Track,
                limit: limit, offset: offset, market: Market).ConfigureAwait(false);
        }

        public Paging<T> GetNextPage<T>(Paging<T> paging)
        {
            return GetApi().GetNextPage(paging);
        }

        public async Task<Paging<T>> GetNextPageAsync<T>(Paging<T> paging)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetNextPageAsync(paging).ConfigureAwait(false);
        }

        public CursorPaging<T> GetNextPage<T>(CursorPaging<T> paging)
        {
            return GetApi().GetNextPage(paging);
        }

        public async Task<CursorPaging<T>> GetNextPageAsync<T>(CursorPaging<T> paging)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetNextPageAsync(paging).ConfigureAwait(false);
        }


        public Paging<T> GetPreviousPage<T>(Paging<T> paging)
        {
            return GetApi().GetPreviousPage(paging);
        }

        public async Task<Paging<T>> GetPreviousPageAsync<T>(Paging<T> paging)
        {
            return await (await GetApiAsync().ConfigureAwait(false)).GetPreviousPageAsync(paging).ConfigureAwait(false);
        }

        private SpotifyWebAPI GetApi()
        {
            SetMarket();
            SpotifyWebAPI api = _tokenProvider.GetApi();
            api = ConfigureApi(api);
            return api;
        }

        private async Task<SpotifyWebAPI> GetApiAsync()
        {
            SetMarket();
            SpotifyWebAPI api = await _tokenProvider.GetApiAsync().ConfigureAwait(false);
            api = ConfigureApi(api);

            return api;
        }
        private SpotifyWebAPI ConfigureApi(SpotifyWebAPI api)
        {
            api.UseAutoRetry = true;
            return api;
        }

        private void SetMarket()
        {
            if (string.IsNullOrEmpty(_market))
            {
                Console.WriteLine("Setting Market");
                SpotifyWebAPI api = _tokenProvider.GetApi();
                Console.WriteLine(api);

                _market = api.GetPrivateProfile().Country;

                Console.WriteLine("Market: " + _market);

            }
        }

        protected string PrepareSearchText(string text, int year = 0)
        {
            text = text.Trim();
            text = text + "**";
            if (year != 0)
            {
                text += " year:" + year;
            }
            return text;
        }
    }
}
