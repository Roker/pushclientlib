﻿using SpotifyAPI.Web.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Services
{
    public interface ISpotifyWebService
    {
        string Market { get; }
        ListResponse<bool> CheckSavedAlbums(IEnumerable<string> ids);
        Task<ListResponse<bool>> CheckSavedAlbumsAsync(IEnumerable<string> ids);
        FullAlbum GetAlbum(string id);
        Task<FullAlbum> GetAlbumAsync(string id);
        List<FullAlbum> GetAlbums(IEnumerable<string> ids);
        Task<SeveralAlbums> GetAlbumsAsync(IEnumerable<string> ids);
        FullArtist GetArtist(string id);
        Paging<SimpleAlbum> GetArtistAlbums(string id);
        Task<FullArtist> GetArtistAsync(string id);
        Task<Paging<SimpleAlbum>> GetArtistsAlbumsAsync(string id);
        FollowedArtists GetFollowedArtists(string after = "", int limit = 20);
        Task<FollowedArtists> GetFollowedArtistsAsync(string after = "", int limit = 20);
        NewAlbumReleases GetNewAlbumReleases(int offset = 0, int limit = 20);
        Task<NewAlbumReleases> GetNewAlbumReleasesAsync(int offset = 0, int limit = 20);
        Paging<T> GetNextPage<T>(Paging<T> paging);
        CursorPaging<T> GetNextPage<T>(CursorPaging<T> paging);
        Task<Paging<T>> GetNextPageAsync<T>(Paging<T> paging);
        Task<CursorPaging<T>> GetNextPageAsync<T>(CursorPaging<T> paging);
        Paging<T> GetPreviousPage<T>(Paging<T> paging);
        Task<Paging<T>> GetPreviousPageAsync<T>(Paging<T> paging);
        PrivateProfile GetPrivateProfile();
        Task<PrivateProfile> GetPrivateProfileAsync();
        CursorPaging<PlayHistory> GetRecentlyPlayedTracks(int limit = 20, DateTime? after = null, DateTime? before = null);
        Task<CursorPaging<PlayHistory>> GetRecentlyPlayedTracksAsync(int limit = 20, DateTime? after = null, DateTime? before = null);
        Paging<SavedAlbum> GetSavedAlbums(int offset = 0, int limit = 20);
        Task<Paging<SavedAlbum>> GetSavedAlbumsAsync(int offset = 0, int limit = 20);
        SearchItem SearchAlbums(string text, int offset = 0, int limit = 20, int year = 0);
        Task<SearchItem> SearchAlbumsAsync(string text, int offset = 0, int limit = 20, int year = 0);
        SearchItem SearchArtists(string text, int offset = 0, int limit = 20, int year = 0);
        Task<SearchItem> SearchArtistsAsync(string text, int offset = 0, int limit = 20, int year = 0);
        SearchItem SearchTracks(string text, int offset = 0, int limit = 20, int year = 0);
        Task<SearchItem> SearchTracksAsync(string text, int offset = 0, int limit = 20, int year = 0);
    }
}