﻿using System;

namespace PushClientLib.Spotify.Models
{
    public class SpotifySavedAlbum : ISpotifySavedAlbum
    {
        public string SpotifyAlbumId { get; set; }
        public ISpotifyAlbum SpotifyAlbum { get; set; }
        public DateTime AddedAt { get; set; }

        private DateTime _originalAddedAt;
        public DateTime OriginalAddedAt {
            get {
                if (_originalAddedAt == null || _originalAddedAt == default)
                {
                    return AddedAt;
                }

                return _originalAddedAt;
            }
            set => _originalAddedAt = value;
        }

    }
}
