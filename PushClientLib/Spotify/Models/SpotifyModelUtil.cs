﻿using System;

namespace PushClientLib.Spotify.Models
{
    public static class SpotifyModelUtil
    {
        public static string DurationToString(double durationMs)
        {
            TimeSpan timeSpan = TimeSpan.FromMilliseconds(durationMs);
            if (durationMs == 0 || timeSpan == null) return string.Empty;


            return timeSpan.ToString(@"mm\:ss");
        }
    }
}
