﻿namespace PushClientLib.Spotify.Models
{
    public class SpotifyImage : ISpotifyImage
    {
        public string Url { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
