﻿using System.Collections.Generic;

namespace PushClientLib.Spotify.Models
{
    public class SpotifyTrack : ISpotifyTrack
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int DurationMs { get; set; }

        public ISpotifyAlbum Album { get; set; }
        public List<ISpotifyArtist> Artists { get; set; }
    }
}
