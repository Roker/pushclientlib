﻿namespace PushClientLib.Spotify.Models
{
    public interface ISpotifyImage
    {
        string Url { get; set; }
        int Height { get; set; }
        int Width { get; set; }
    }
}
