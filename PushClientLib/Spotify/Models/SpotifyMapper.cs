﻿using SpotifyAPI.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace PushClientLib.Spotify.Models
{
    public static class SpotifyMapper
    {
        private static readonly CultureInfo PROVIDER = CultureInfo.InvariantCulture;

        public static ISpotifyAlbum FullAlbumToSpotifyAlbum(FullAlbum fullAlbum)
        {

            return new SpotifyAlbum
            {
                Id = fullAlbum.Id,
                Name = fullAlbum.Name,
                Artists = SimpleArtistsToArtists(fullAlbum.Artists),
                Images = AlbumImagesToImages(fullAlbum.Images),
                Tracks = SimpleTacksToTracks(fullAlbum.Tracks),
                ReleaseDate = ParseDate(fullAlbum.ReleaseDate, fullAlbum.ReleaseDatePrecision),
                ReleaseDatePrecision = fullAlbum.ReleaseDatePrecision,
                AlbumType = fullAlbum.AlbumType,
                TrackCount = fullAlbum.TotalTracks,
                Genres = fullAlbum.Genres,
                //ImportType = ImportType.FULL_ALBUM,
                Markets = fullAlbum.AvailableMarkets,
                Popularity = fullAlbum.Popularity
            };
        }

        private static List<ISpotifyTrack> SimpleTacksToTracks(Paging<SimpleTrack> simpleTracks)
        {
            List<ISpotifyTrack> tracks = new List<ISpotifyTrack>();
            foreach (SimpleTrack track in simpleTracks.Items)
            {
                tracks.Add(SimpleTrackToTrack(track));
            }
            return tracks;
        }

        private static ISpotifyTrack SimpleTrackToTrack(SimpleTrack simpleTrack)
        {
            return new SpotifyTrack()
            {
                Id = simpleTrack.Id,
                Name = simpleTrack.Name,
                DurationMs = simpleTrack.DurationMs,
                Artists = SimpleArtistsToSpotifyArtists(simpleTrack.Artists)
            };
        }

        private static List<ISpotifyArtist> SimpleArtistsToSpotifyArtists(List<SimpleArtist> simpleArtists)
        {
            List<ISpotifyArtist> artists = new List<ISpotifyArtist>();
            foreach (SimpleArtist simpleArtist in simpleArtists)
            {
                artists.Add(SimpleArtistToSpotifyArtist(simpleArtist));
            }
            return artists;
        }

        public static ISpotifyAlbum SimpleAlbumToSpotifyAlbum(SimpleAlbum simpleAlbum)
        {
            return new SpotifyAlbum
            {
                Id = simpleAlbum.Id,
                Name = simpleAlbum.Name,
                Artists = SimpleArtistsToArtists(simpleAlbum.Artists),
                Images = AlbumImagesToImages(simpleAlbum.Images),
                ReleaseDate = ParseDate(simpleAlbum.ReleaseDate, simpleAlbum.ReleaseDatePrecision),
                ReleaseDatePrecision = simpleAlbum.ReleaseDatePrecision,
                AlbumType = simpleAlbum.AlbumType,
                TrackCount = simpleAlbum.TotalTracks,
                //ImportType = ImportType.SIMPLE_ALBUM,
                Markets = simpleAlbum.AvailableMarkets
            };
        }

        public static List<ISpotifyAlbum> FullAlbumsToSpotifyAlbums(IEnumerable<FullAlbum> fullAlbums)
        {
            List<ISpotifyAlbum> spotifyAlbums = new List<ISpotifyAlbum>();
            foreach (FullAlbum album in fullAlbums)
            {
                spotifyAlbums.Add(FullAlbumToSpotifyAlbum(album));
            }
            return spotifyAlbums;
        }

        public static List<ISpotifyAlbum> SimpleAlbumsToSpotifyAlbums(IEnumerable<SimpleAlbum> simpleAlbums)
        {
            List<ISpotifyAlbum> spotifyAlbums = new List<ISpotifyAlbum>();
            foreach (SimpleAlbum album in simpleAlbums)
            {
                spotifyAlbums.Add(SimpleAlbumToSpotifyAlbum(album));
            }
            return spotifyAlbums;
        }




        //internal static SpotifyAlbum FullAlbumsToSpotifyAlbums(List<FullAlbum> fullAlbums)
        //{
        //    List<SpotifyAlbum> 
        //}






        public static ISpotifyArtist FullArtistToSpotifyArtist(FullArtist artist)
        {
            return new SpotifyArtist
            {
                Id = artist.Id,
                Name = artist.Name
            };
        }
        public static ISpotifyArtist SimpleArtistToSpotifyArtist(SimpleArtist artist)
        {
            return new SpotifyArtist
            {
                Id = artist.Id,
                Name = artist.Name
            };
        }

        public static ISpotifySavedAlbum SavedAlbumToSpotifySavedAlbum(SavedAlbum savedAlbum)
        {
            return new SpotifySavedAlbum()
            {
                SpotifyAlbumId = savedAlbum.Album.Id,
                SpotifyAlbum = FullAlbumToSpotifyAlbum(savedAlbum.Album),
                AddedAt = savedAlbum.AddedAt
            };
        }

        public static List<ISpotifySavedAlbum> SavedAlbumsToSpotifySavedAlbums(IEnumerable<SavedAlbum> savedAlbums)
        {
            List<ISpotifySavedAlbum> spotifySavedAlbums = new List<ISpotifySavedAlbum>();
            if (savedAlbums == null) return spotifySavedAlbums;
            foreach (SavedAlbum savedAlbum in savedAlbums)
            {
                spotifySavedAlbums.Add(SavedAlbumToSpotifySavedAlbum(savedAlbum));
            }
            //if (savedAlbums.Count != spotifySavedAlbums.Count)
            //{
            //    throw new Exception("Mapping error: SavedAlbumCount does not match SpotifySavedAlbumCount");
            //}
            return spotifySavedAlbums;
        }

        //private
        private static List<ISpotifyArtist> SimpleArtistsToArtists(IEnumerable<SimpleArtist> artists)
        {
            List<ISpotifyArtist> spotifyArtists = new List<ISpotifyArtist>();
            foreach (SimpleArtist artist in artists)
            {
                spotifyArtists.Add(SimpleArtistToSpotifyArtist(artist));
            }

            return spotifyArtists;
        }
        private static List<ISpotifyImage> AlbumImagesToImages(IEnumerable<Image> images)
        {
            List<ISpotifyImage> albumImages = new List<ISpotifyImage>();
            foreach (Image image in images)
            {
                albumImages.Add(new SpotifyImage
                {
                    Height = image.Height,
                    Width = image.Width,
                    Url = image.Url
                });
            }
            return albumImages;
        }
        private static DateTime ParseDate(string releaseDate, string releaseDatePrecision)
        {
            switch (releaseDatePrecision)
            {
                case "year":
                    return DateTime.ParseExact(releaseDate, "yyyy", PROVIDER);
                case "month":
                    return DateTime.ParseExact(releaseDate, "yyyy-MM", PROVIDER);
                case "day":
                    return DateTime.ParseExact(releaseDate, "yyyy-MM-dd", PROVIDER);
            }
            throw new NotImplementedException();
        }


    }
}
