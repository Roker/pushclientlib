﻿using System;

namespace PushClientLib.Spotify.Models
{
    public interface ISpotifySavedAlbum
    {
        string SpotifyAlbumId { get; set; }
        ISpotifyAlbum SpotifyAlbum { get; set; }

        DateTime AddedAt { get; set; }

        //DateTime OriginalAddedAt { get; set; }
    }
}
