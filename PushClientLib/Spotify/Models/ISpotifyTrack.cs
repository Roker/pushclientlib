﻿using System.Collections.Generic;

namespace PushClientLib.Spotify.Models
{
    public interface ISpotifyTrack
    {
        string Id { get; set; }
        string Name { get; set; }
        int DurationMs { get; set; }

        //ISpotifyAlbum Album { get; set; }
        List<ISpotifyArtist> Artists { get; set; }

    }
}
