﻿namespace PushClientLib.Spotify.Models
{

    public interface ISpotifyAlbumArtist
    {
        int Id { get; set; }
        string AlbumId { get; set; }
        string ArtistId { get; set; }
    }
}
