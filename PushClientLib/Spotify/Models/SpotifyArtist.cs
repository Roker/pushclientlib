﻿namespace PushClientLib.Spotify.Models
{
    public class SpotifyArtist : ISpotifyArtist
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
