﻿using System;
using System.Collections.Generic;

namespace PushClientLib.Spotify.Models
{
    public class SpotifyAlbum : ISpotifyAlbum
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public List<ISpotifyArtist> Artists { get; set; }

        public List<ISpotifyImage> Images { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string ReleaseDatePrecision { get; set; }
        public string AlbumType { get; set; }
        public int TrackCount { get; set; }
        public List<string> Genres { get; set; }

        public int Popularity { get; set; }
        public List<ISpotifyTrack> Tracks { get; set; }

        public List<string> Markets { get; set; }

        public bool Equals(ISpotifyAlbum other)
        {
            return Id == other.Id;
        }
    }


}
