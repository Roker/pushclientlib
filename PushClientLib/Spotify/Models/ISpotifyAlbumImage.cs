﻿namespace PushClientLib.Spotify.Models
{
    public interface ISpotifyAlbumImage
    {
        int Id { get; set; }
        string AlbumId { get; set; }
        string ImageUrl { get; set; }
    }
}
