﻿namespace PushClientLib.Spotify.Models
{
    public interface ISpotifyItem
    {
        string Id { get; set; }
    }
}
