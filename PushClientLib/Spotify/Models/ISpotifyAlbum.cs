﻿using System;
using System.Collections.Generic;

namespace PushClientLib.Spotify.Models
{
    public interface ISpotifyAlbum : ISpotifyItem, IEquatable<object>, IEquatable<ISpotifyAlbum>
    {
        string Name { get; set; }
        List<ISpotifyArtist> Artists { get; set; }
        List<ISpotifyImage> Images { get; set; }
        DateTime ReleaseDate { get; set; }
        string ReleaseDatePrecision { get; set; }
        string AlbumType { get; set; }
        int TrackCount { get; set; }
        List<string> Genres { get; set; }
        int Popularity { get; set; }
        List<string> Markets { get; set; }
        List<ISpotifyTrack> Tracks { get; set; }
    }
}