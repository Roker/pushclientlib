﻿namespace PushClientLib.Spotify.Models
{
    public interface ISpotifyArtist : ISpotifyItem
    {
        string Name { get; set; }
    }
}
