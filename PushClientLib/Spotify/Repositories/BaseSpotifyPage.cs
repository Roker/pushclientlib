﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories
{
    public abstract class BaseSpotifyPage<T> : IPage<T>
    {
        public int Offset { get; protected set; }
        public int Limit { get; protected set; }
        public int Total { get; protected set; }
        public IEnumerable<T> Items { get; protected set; }

        public Task<IPage<T>> GetNextPageAsync()
        {
            int offset = Offset + Limit;
            int limit = Math.Min(Limit, Total - offset);
            return GetPage(offset: offset, limit: limit);
        }

        public Task<IPage<T>> GetPreviousPageAsync()
        {
            int offset = Math.Max(0, Offset - Limit);
            int limit = Math.Min(Limit, Offset);
            return GetPage(offset: offset, limit: limit);
        }

        public abstract Task<IPage<T>> GetPage(int offset, int limit);

        protected BaseSpotifyPage(int offset, int limit, int total, IEnumerable<T> items)
        {
            Offset = offset;
            Limit = limit;
            Total = total;
            Items = items;
        }

        public bool HasNextPage()
        {
            return Offset + Limit < Total;
        }

        public bool HasPreviousPage()
        {
            return Offset > 0;
        }

    }
}
