﻿using PushClientLib.Spotify.Models;
using SpotifyAPI.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifySavedAlbums
{
    public class SpotifySavedAlbumPage : BaseSpotifyPage<ISpotifySavedAlbum>
    {
        private ISpotifySavedAlbumReader _reader;

        public SpotifySavedAlbumPage(ISpotifySavedAlbumReader reader, Paging<SavedAlbum> paging)
            : base(paging.Offset, paging.Limit, paging.Total, SpotifyMapper.SavedAlbumsToSpotifySavedAlbums(paging.Items))
        {
            _reader = reader;
        }

        public SpotifySavedAlbumPage(ISpotifySavedAlbumReader reader, int offset, int limit, int total, IEnumerable<ISpotifySavedAlbum> items)
            : base(offset, limit, total, items)
        {
            _reader = reader;
        }


        public override Task<IPage<ISpotifySavedAlbum>> GetPage(int offset, int limit)
        {
            return _reader.GetPageAsync(offset: offset, limit: limit);

        }

        public void SetReader(ISpotifySavedAlbumReader reader)
        {
            _reader = reader;
        }
    }
}
