﻿using PushSpotifyLib.Models;
using PushSpotifyLib.Paging;
using PushSpotifyLib.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifySavedAlbums
{
    public class WebSpotifySavedAlbumReader : ISpotifySavedAlbumReader
    {
        private const int DEFAULT_LIMIT = 20;

        private ISpotifySavedAlbumService _spotifySavedAlbumService;


        public WebSpotifySavedAlbumReader(ISpotifySavedAlbumService spotifySavedAlbumService)
        {
            _spotifySavedAlbumService = spotifySavedAlbumService;
        }

        public int Limit => DEFAULT_LIMIT;


        public Task<int> GetTotalAsync()
        {
            return _spotifySavedAlbumService.GetTotalAsync();
        }

        public Task<List<bool>> CheckAsync(IEnumerable<string> ids)
        {
            return _spotifySavedAlbumService.CheckSavedAlbumsAsync(ids);
        }

        //TODO: fix limit
        public Task<IPage<ISpotifySavedAlbum>> GetPageAsync(int offset = 0, int limit = 20)
        {
            return _spotifySavedAlbumService.GetSavedAlbumsAsync(offset, limit);
        }

    }
}
