﻿using PushSpotifyLib.Models;
using PushSpotifyLib.Paging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifySavedAlbums
{
    public static class SpotifySavedAlbumReaderExtensions
    {
        public static async Task<IPage<ISpotifySavedAlbum>> GetLastPageAsync(this ISpotifySavedAlbumReader spotifySavedAlbumReader)
        {
            int total = await spotifySavedAlbumReader.GetTotalAsync().ConfigureAwait(false);
            int limit = spotifySavedAlbumReader.Limit;

            int offset = Math.Max(0, total - limit);

            return await spotifySavedAlbumReader.GetPageAsync(offset: offset, limit: limit).ConfigureAwait(false);
        }

        public static async Task<IPage<ISpotifySavedAlbum>> GetPageEndingAt(
            this ISpotifySavedAlbumReader spotifySavedAlbumReader, DateTime date)
        {
            List<ISpotifySavedAlbum> savedAlbums = await spotifySavedAlbumReader.GetAsync(offset: 0, limit: 1).ConfigureAwait(false);
            int total = await spotifySavedAlbumReader.GetTotalAsync().ConfigureAwait(false);
            //TODO: fix limit
            int limit = 20;

            return await spotifySavedAlbumReader.GetPageAsync(offset: total - limit, limit: limit).ConfigureAwait(false);
        }
    }
}
