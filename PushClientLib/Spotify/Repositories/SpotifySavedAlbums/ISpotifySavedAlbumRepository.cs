﻿using PushSpotifyLib.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifySavedAlbums
{
    public interface ISpotifySavedAlbumRepository : ISpotifySavedAlbumReader
    {

        Task AddAsync(IEnumerable<ISpotifySavedAlbum> savedAlbums);
        Task AddAsync(ISpotifySavedAlbum savedAlbum);
        Task RemoveAsync(IEnumerable<string> ids);
        Task RemoveAsync(string id);

        Task<IEnumerable<string>> GetMissingAlbumIds();
    }
}