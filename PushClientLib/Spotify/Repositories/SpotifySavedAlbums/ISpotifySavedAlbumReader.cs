﻿using PushSpotifyLib.Models;
using PushSpotifyLib.Paging;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifySavedAlbums
{
    public interface ISpotifySavedAlbumReader
    {

        int Limit { get; }
        Task<IPage<ISpotifySavedAlbum>> GetPageAsync(int offset = 0, int limit = 20);
        Task<int> GetTotalAsync();
    }
}