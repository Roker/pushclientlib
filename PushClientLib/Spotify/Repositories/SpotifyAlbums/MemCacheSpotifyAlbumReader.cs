﻿//using PushClientLib.Spotify.Models;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifyAlbums
{
    //namespace PushClientLib.Spotify.Repositories.SpotifyAlbums
    //{
    //    public class MemCacheSpotifyAlbumReader : ISpotifyAlbumReader
    //    {
    //        private Dictionary<string, ISpotifyAlbum> _cachedAlbums = new Dictionary<string, ISpotifyAlbum>();
    //        private ISpotifyAlbumReader _baseReader;
    //        public MemCacheSpotifyAlbumReader(ISpotifyAlbumReader spotifyAlbumReader)
    //        {
    //            _baseReader = spotifyAlbumReader;
    //        }
    //        public async Task<List<ISpotifyAlbum>> GetAsync(List<string> ids)
    //        {
    //            List<ISpotifyAlbum> cachedAlbums = new List<ISpotifyAlbum>();
    //            List<string> retrieveIds = new List<string>();
    //            foreach (string id in ids)
    //            {
    //                ISpotifyAlbum cachedAlbum;

    //                if (_cachedAlbums.TryGetValue(id, out cachedAlbum))
    //                {
    //                    cachedAlbums.Add(cachedAlbum);
    //                }
    //                else
    //                {
    //                    retrieveIds.Add(id);
    //                }
    //            }

    //            if (retrieveIds.Count > 0)
    //            {
    //                try
    //                {
    //                    //use array for sorting
    //                    ISpotifyAlbum[] returnAlbums = new ISpotifyAlbum[ids.Count];
    //                    //fill array with albums that were retrieved
    //                    foreach (ISpotifyAlbum cachedAlbum in cachedAlbums)
    //                    {
    //                        returnAlbums[ids.IndexOf(cachedAlbum.Id)] = cachedAlbum;
    //                    }

    //                    List<ISpotifyAlbum> retrievedAlbums = await _baseReader.GetAsync(retrieveIds).ConfigureAwait(false);
    //                    foreach (ISpotifyAlbum retrievedAlbum in retrievedAlbums)
    //                    {
    //                        returnAlbums[ids.IndexOf(retrievedAlbum.Id)] = retrievedAlbum;
    //                        CacheAlbum(retrievedAlbum);
    //                    }

    //                    List<ISpotifyAlbum> returnList = new List<ISpotifyAlbum>();
    //                    foreach (ISpotifyAlbum spotifyAlbum in returnAlbums)
    //                    {
    //                        if (spotifyAlbum == null)
    //                        {
    //                            Console.WriteLine("SPOTIFYALBUMMEMCACHEREADER: COULD NOT LOAD ALL ALBUMS, FALLING BACK ON BASEREADER");
    //                            throw new Exception();
    //                        }
    //                        returnList.Add(spotifyAlbum);
    //                    }
    //                    return returnList;
    //                }
    //                catch
    //                {
    //                    return await _baseReader.GetAsync(ids).ConfigureAwait(false);
    //                }

    //            }
    //            else
    //            {
    //                return cachedAlbums;
    //            }
    //        }

    //        public async Task<ISpotifyAlbum> GetAsync(string id)
    //        {
    //            ISpotifyAlbum cachedAlbum;

    //            if (_cachedAlbums.TryGetValue(id, out cachedAlbum))
    //            {
    //                return cachedAlbum;
    //            }

    //            cachedAlbum = await _baseReader.GetAsync(id).ConfigureAwait(false);
    //            CacheAlbum(cachedAlbum);

    //            return cachedAlbum;
    //        }

    //        private void CacheAlbum(ISpotifyAlbum retrievedAlbum)
    //        {
    //            _cachedAlbums[retrievedAlbum.Id] = retrievedAlbum;
    //        }
    //    }
    //}
}