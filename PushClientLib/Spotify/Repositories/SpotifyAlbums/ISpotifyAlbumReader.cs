﻿using PushSpotifyLib.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifyAlbums
{
    public interface ISpotifyAlbumReader
    {
        Task<List<ISpotifyAlbum>> GetAsync(IEnumerable<string> ids);
        Task<ISpotifyAlbum> GetAsync(string id);
    }
}