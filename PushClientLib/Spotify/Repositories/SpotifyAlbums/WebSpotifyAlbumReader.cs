﻿using PushSpotifyLib.Models;
using PushSpotifyLib.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifyAlbums
{
    public class WebSpotifyAlbumReader : ISpotifyAlbumReader
    {
        private ISpotifyAlbumService _spotifyWebService;
        public WebSpotifyAlbumReader(ISpotifyAlbumService spotifyWebService)
        {
            _spotifyWebService = spotifyWebService;
        }

        public virtual Task<ISpotifyAlbum> GetAsync(string id)
        {
            return _spotifyWebService.GetAlbumAsync(id);
        }

        public virtual Task<List<ISpotifyAlbum>> GetAsync(IEnumerable<string> ids)
        {
            return _spotifyWebService.GetAlbumsAsync(ids);
        }
    }
}
