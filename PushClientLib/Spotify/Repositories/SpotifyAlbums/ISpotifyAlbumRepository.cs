﻿using PushSpotifyLib.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories.SpotifyAlbums
{
    public interface ISpotifyAlbumRepository : ISpotifyAlbumReader
    {
        Task RemoveAsync(string spotifyAlbum);

        Task RemoveAsync(IEnumerable<string> spotifyAlbums);
        Task AddAsync(ISpotifyAlbum spotifyAlbum);

        Task AddAsync(IEnumerable<ISpotifyAlbum> spotifyAlbums);
    }
}
