﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories
{
    public class EmptyPage<T> : IPage<T>
    {
        public IEnumerable<T> Items => default;
        public int Total => 0;
        public int Offset => 0;
        public int Limit => 0;

        public async Task<IPage<T>> GetNextPageAsync()
        {
            return this;
        }

        public async Task<IPage<T>> GetPreviousPageAsync()
        {
            return this;
        }

        public bool HasNextPage()
        {
            return false;
        }

        public bool HasPreviousPage()
        {
            return false;
        }
    }
}
