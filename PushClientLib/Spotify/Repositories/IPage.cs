﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Spotify.Repositories
{
    public interface IPage<T>
    {
        IEnumerable<T> Items { get; }
        int Total { get; }
        int Offset { get; }
        int Limit { get; }

        bool HasNextPage();
        bool HasPreviousPage();

        Task<IPage<T>> GetNextPageAsync();
        Task<IPage<T>> GetPreviousPageAsync();
    }
}