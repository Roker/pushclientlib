﻿using PushClientLib.Web.Models;
using SpotifyAPI.Web;
using System.Threading.Tasks;

namespace PushClientLib.Spotify
{
    public interface ISpotifyTokenProvider
    {
        Task<SpotifyToken> GetTokenAsync();
        //Task<SpotifyWebAPI> GetApiAsync();
        //SpotifyWebAPI GetApi();
    }
}
