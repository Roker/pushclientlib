﻿using System;
using System.Threading.Tasks;

namespace PushClientLib.Util
{
    public class CachedResultManager<T>
    {
        private readonly object _lock = new object();

        private readonly DateTime _lastChecked = default;
        private Task _updateTask;
        private T _result;

        private TimeSpan _cacheTimeSpan;

        public CachedResultManager(TimeSpan cacheTimeSpan)
        {
            _cacheTimeSpan = cacheTimeSpan;
        }

        public async Task<T> GetAsync(Task<T> retrieveTask)
        {
            lock (_lock)
            {
                if (_updateTask == null || _updateTask.IsCompleted && (DateTime.Now - _lastChecked > _cacheTimeSpan))
                {
                    _updateTask = Task.Run(() => retrieveTask);
                }
            }
            await _updateTask.ConfigureAwait(false);
            _result = retrieveTask.Result;
            return _result;
        }
    }
}
