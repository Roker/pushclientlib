﻿using System;
using System.Threading.Tasks;

namespace PushClientLib.Util
{
    public static class TaskUtil
    {
        public static T RunAsync<T>(Func<Task<T>> p)
        {
            Task<T> task = Task.Run(p);
            Console.WriteLine("task running");
            task.Wait();
            return task.Result;
        }
    }
}
