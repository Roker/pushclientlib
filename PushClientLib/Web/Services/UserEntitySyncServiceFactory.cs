﻿using PushClientLib.KeyValueStorage;
using PushClientLib.Models;
using PushClientLib.Repositories;
using PushClientLib.SQLite.Repositories;
using PushClientLib.Web.Repositories;
using PushClientLib.Web.Requests;
using SQLite;

namespace PushClientLib.Web.Services
{
    internal static class UserEntitySyncServiceFactory
    {
        public static UserEntitySyncService<T> NewSQLiteSyncService<T>(
            IKeyValueStorage keyValueStorage, IPushRequestHandler handler, SQLiteAsyncConnection con) where T : IBaseUserEntity
        {
            IAsyncUserEntityCachingRepository<T> webCache
                = WebUserEntityRepositoryFactory.NewWebUserEntityCachingRepository<T>(handler);

            IAsyncUserEntityCachingRepository<T> dbCache
                = UserEntitySQLiteRepositoryFactory.NewUserEntitySQLiteCachingRepository<T>(con);

            return new UserEntitySyncService<T>(webCache, dbCache, keyValueStorage);
        }

        public static UserEntitySyncService<T> NewSQLiteSyncService<T>(
            IKeyValueStorage keyValueStorage,
            IPushRequestHandler handler,
            IAsyncUserEntityCachingRepository<T> cacheDb) where T : IBaseUserEntity
        {
            IAsyncUserEntityCachingRepository<T> webCache
                = WebUserEntityRepositoryFactory.NewWebUserEntityCachingRepository<T>(handler);


            return new UserEntitySyncService<T>(webCache, cacheDb, keyValueStorage);
        }
        //public static async Task<UserEntitySyncService<T>> NewSQLiteSyncServiceAsync<T>(
        //    IKeyValueStorage keyValueStorage, IPushUserRequestService pushRequestService, SQLiteAsyncConnection con) where T : IBaseUserEntity
        //{
        //    IAsyncUserEntityCachingRepository<T> webCache
        //        = await WebUserEntityRepositoryFactory.NewWebUserEntityCachingRepositoryAsync<T>(pushRequestService).ConfigureAwait(false);

        //    IAsyncUserEntityCachingCacheRepository<T> dbCache
        //        = UserEntitySQLiteRepositoryFactory.NewUserEntitySQLiteCachingCacheRepository<T>(con);

        //    return new UserEntitySyncService<T>(webCache, dbCache, keyValueStorage);
        //}
    }
}
