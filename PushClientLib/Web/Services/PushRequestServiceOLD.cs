﻿using PushClientLib.Authenticators;
using PushClientLib.JsonConverters;
using PushClientLib.Web.Requests;
using PushClientLib.Web.Requests.Builders;
using RestSharp;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.Web.Services
{
    public class PushRequestService : IPushRequestService
    {
        private readonly string _url;
        private readonly string _deviceId;

        private string _authToken;

        //private User _user;

        private readonly Task _getUserTask;

        private readonly IUserRequestBuilder _userRequestBuilder;

        public PushRequestService(string url, string deviceId)
        {
            _url = url;
            _deviceId = deviceId;
        }


        public void SetAuthToken(string authToken)
        {
            _authToken = authToken;
        }


        private void ConfigRequest(ref IRestRequest request)
        {
            request.RequestFormat = DataFormat.Json;

            request.AddHeader("device_id", _deviceId);
            request.AddHeader("Accept", "application/json");
        }
        public Task<T> MakeUrlRequestAsync<T>(string url, bool authed = true)
        {
            url = url.Replace(_url, "");
            IRestRequest request = new RestRequest(url);
            return MakeRequestAsync<T>(request, authed);
        }
        public async Task<T> MakeRequestAsync<T>(IRestRequest request, bool authed = true)
        {

            IRestClient client = NewRestClient();
            Console.WriteLine("Making Request..: " + _url + request.Resource);


            if (authed)
            {
                while (_authToken == null)
                {
                    await Task.Delay(200).ConfigureAwait(false);
                }
            }
            ConfigRequest(ref request);

            return await RequestsUtil.MakeRequestAsync<T>(client, request, authed);
        }

        public async Task<EntityResponse<T>> MakeBundledRequestAsync<T>(IRestRequest request, bool authed = true)
        {

            IRestClient client = NewRestClient();
            Console.WriteLine("Making Request..: " + _url + request.Resource);


            if (authed)
            {
                while (_authToken == null)
                {
                    await Task.Delay(200).ConfigureAwait(false);
                }
            }
            ConfigRequest(ref request);

            return await RequestsUtil.MakeBundledRequestAsync<T>(client, request, authed);
        }

        private IRestClient NewRestClient(bool noBaseUrl = false)
        {
            string url = noBaseUrl ? "" : _url;
            RestClient restClient = new RestClient(url);
            restClient.UseSerializer<JsonNetSerializer>();

            if (_authToken != null)
            {
                restClient.Authenticator = new BearerAuthenticator(_authToken);
            }

            return restClient;
        }

        public Task<string> GetUserUuidAsync()
        {
            throw new NotImplementedException();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
