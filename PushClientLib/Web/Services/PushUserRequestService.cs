﻿//using PushClientLib.Authenticators;
//using PushClientLib.Models;
//using PushClientLib.Web.Requests.Builders;
//using RestSharp;

//namespace PushClientLib.Web.Services
//{
//    public class PushUserRequestService : PushRequestService, IPushUserRequestService
//    {
//        private readonly IUserData _userData;

//        private readonly IUserRequestBuilder _userRequestBuilder = new UserRequestBuilder();

//        private readonly IPushAuthTokenProvider _authTokenProvider;

//        public string UserUuid => _userData.CurrentUser.Uuid;

//        public PushUserRequestService(string url, string deviceId, IUserData userData)
//            : base(url, deviceId)
//        {
//            _userData = userData;
//        }



//        protected override IRestClient NewRestClient(bool noBaseUrl = false)
//        {
//            IRestClient restClient = base.NewRestClient(noBaseUrl);
//            restClient.Authenticator = new BearerAuthenticator(_userData.AuthToken);
//            return restClient;
//        }
//    }
//}
