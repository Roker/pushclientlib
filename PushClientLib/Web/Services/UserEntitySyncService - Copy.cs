﻿//using Microsoft.Extensions.Hosting;
//using PushClientLib.KeyValueStorage;
//using PushClientLib.Web.Models;
//using PushClientLib.Repositories;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

namespace PushClientLib.Web.Services
{
    //namespace PushClientLib.Web.Services
    //{
    //    public class UserEntitySyncService<T> : BackgroundService where T : IBaseUserEntity
    //    {
    //        private const int SYNC_INTERVAL_MILI = 60000;
    //        private const string LAST_SYNCED_KEY_BASE = "USER_ENTITY_SYNC_SERVICE_LAST_UPDATED";

    //        private readonly string _lastSyncedKey;

    //        private IAsyncUserEntityRepository<T> _source;
    //        private IAsyncUserEntityCacheRepository<T> _cache;
    //        private IKeyValueStorage _keyValueStorage;

    //        private CancellationTokenSource _syncDelayTokenSource;

    //        public UserEntitySyncService(
    //            IAsyncUserEntityRepository<T> source,
    //            IAsyncUserEntityCacheRepository<T> cache,
    //            IKeyValueStorage keyValueStorage)
    //        {
    //            _source = source;
    //            _cache = cache;
    //            _keyValueStorage = keyValueStorage;
    //            _lastSyncedKey = LAST_SYNCED_KEY_BASE + "_" + typeof(T).Name;
    //        }

    //        public bool IsSynced { get; private set; }

    //        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    //        {
    //            while (!stoppingToken.IsCancellationRequested)
    //            {
    //                try
    //                {
    //                    IsSynced = false;
    //                    _syncDelayTokenSource = new CancellationTokenSource();
    //                    Console.WriteLine("Syncing...");
    //                    await SyncAsync().ConfigureAwait(false);
    //                    Console.WriteLine("Syncing Done");
    //                    IsSynced = true;

    //                    CancellationTokenSource linkedToken = 
    //                        CancellationTokenSource.CreateLinkedTokenSource(stoppingToken, _syncDelayTokenSource.Token);

    //                    await Task.Delay(SYNC_INTERVAL_MILI, linkedToken.Token).ConfigureAwait(false);
    //                }
    //                catch(Exception e) {
    //                    Console.Write("Syncing Exception: " + e.Message);

    //                }
    //            }
    //        }

    //        public void ForceSync()
    //        {
    //            IsSynced = false;
    //            _syncDelayTokenSource.Cancel();
    //        }

    //        private async Task SyncAsync()
    //        {
    //            DateTime newLastSynced = DateTime.UtcNow;
    //            DateTime lastSynced = GetLastSynced();
    //            Console.WriteLine("LAST SYNCED: " + lastSynced);

    //            try
    //            {
    //                await UpdateSourceAsync(lastSynced).ConfigureAwait(false);
    //                await CacheFromSourceAsync(lastSynced).ConfigureAwait(false);

    //                SetLastSynced(newLastSynced);
    //            }
    //            catch(Exception e) {
    //                Console.WriteLine(e.Message);
    //            }
    //        }

    //        private async Task UpdateSourceAsync(DateTime lastSynced)
    //        {
    //            Console.WriteLine("Updating Source...");
    //            await SendUpdatesToSource(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("SendNewToSource...");
    //            await SendNewToSource(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("SendDeletesToSource...");
    //            await SendDeletesToSource(lastSynced).ConfigureAwait(false);
    //        }


    //        private async Task CacheFromSourceAsync(DateTime lastSynced)
    //        {
    //            Console.WriteLine("Updating From Source...");
    //            await CacheNewFromSourceAsync(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("CacheUpdatedFromSourceAsync...");
    //            await CacheUpdatedFromSourceAsync(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("UncacheDeletedFromSourceAsync...");
    //            await UncacheDeletedFromSourceAsync(lastSynced).ConfigureAwait(false);
    //        }

    //        private async Task SendDeletesToSource(DateTime lastSynced)
    //        {
    //            List<T> deletedEntities = await _cache.GetDeletedSinceAsync(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("sending deletes: " + deletedEntities.Count);

    //            foreach (T t in deletedEntities)
    //            {
    //                await _source.DeleteAsync(t).ConfigureAwait(false);
    //            }

    //            await _cache.UncacheAsync(deletedEntities).ConfigureAwait(false);
    //        }

    //        private async Task SendNewToSource(DateTime lastSynced)
    //        {
    //            List<T> addedEntities = await _cache.GetAddedSinceAsync(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("sending new: " + addedEntities.Count);

    //            foreach (T t in addedEntities)
    //            {
    //                await _source.AddAsync(t).ConfigureAwait(false);
    //            }
    //        }

    //        private async Task SendUpdatesToSource(DateTime lastSynced)
    //        {
    //            List<T> updatedEntities = await _cache.GetUpdatedSinceAsync(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("sending updates: " + updatedEntities.Count);

    //            for (int i = 0; i < updatedEntities.Count; i++)
    //            {
    //                T updatedEntity = updatedEntities[i];
    //                try
    //                {
    //                    T sourceEntity = await _source.GetAsync(updatedEntity.Uuid).ConfigureAwait(false);

    //                    if (sourceEntity != null && sourceEntity.Updated > lastSynced)
    //                    {
    //                        Console.WriteLine("SendUpdatesToSource Conflict");
    //                        Console.WriteLine("LastSynced: " + lastSynced);
    //                        Console.WriteLine("Updated: " + sourceEntity.Updated);

    //                        updatedEntity = await ResolveConflictAsync(updatedEntities[i], sourceEntity).ConfigureAwait(false);
    //                        //RESOLVE CONFLICT!!!
    //                    }


    //                    await _source.UpdateAsync(updatedEntity).ConfigureAwait(false);
    //                }
    //                catch (InvalidOperationException e) {
    //                    Console.WriteLine(e.Message);
    //                }
    //            }
    //        }

    //        private async Task CacheUpdatedFromSourceAsync(DateTime lastSynced)
    //        {
    //            List<T> updatedEntities = await _source.GetUpdatedSinceAsync(lastSynced).ConfigureAwait(false);

    //            Console.WriteLine("caching updates: " + updatedEntities.Count);

    //            for (int i = 0; i < updatedEntities.Count; i++)
    //            {
    //                T updatedEntity = updatedEntities[i];

    //                try
    //                {
    //                    T cachedEntity = await _cache.GetAsync(updatedEntity.Uuid).ConfigureAwait(false);

    //                    //CONFLICT!!!
    //                    if (cachedEntity != null && cachedEntity.Updated > lastSynced)
    //                    {
    //                        Console.WriteLine("CacheUpdatedFromSourceAsync Conflict");
    //                        Console.WriteLine("LastSynced: "+ lastSynced);
    //                        Console.WriteLine("Updated: "+ cachedEntity.Updated);

    //                        updatedEntities[i] = await ResolveConflictAsync(updatedEntities[i], cachedEntity).ConfigureAwait(false);
    //                        //RESOLVE CONFLICT!!!
    //                    }
    //                }
    //                catch (InvalidOperationException) { }

    //            }

    //            await _cache.CacheAsync(updatedEntities).ConfigureAwait(false);
    //        }

    //        private async Task CacheNewFromSourceAsync(DateTime lastSynced)
    //        {
    //            List<T> addedEntities = await _source.GetAddedSinceAsync(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("caching new: " + addedEntities.Count);
    //            await _cache.CacheAsync(addedEntities).ConfigureAwait(false);
    //        }

    //        private async Task UncacheDeletedFromSourceAsync(DateTime lastSynced)
    //        {
    //            List<T> deletedEntities = await _source.GetDeletedSinceAsync(lastSynced).ConfigureAwait(false);
    //            Console.WriteLine("uncaching deleted: " + deletedEntities.Count);
    //            await _cache.UncacheAsync(deletedEntities).ConfigureAwait(false);
    //        }

    //        private Task<T> ResolveConflictAsync(T t, T cachedEntity)
    //        {
    //            throw new NotImplementedException();
    //        }

    //        private DateTime GetLastSynced()
    //        {
    //             return _keyValueStorage.Get(_lastSyncedKey, new DateTime());
    //        }

    //        private void SetLastSynced(DateTime newLastSynced)
    //        {
    //            _keyValueStorage.Set(_lastSyncedKey, newLastSynced);
    //        }
    //    }
    //}
}