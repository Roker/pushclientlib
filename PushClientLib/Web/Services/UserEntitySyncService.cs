﻿using Microsoft.Extensions.Hosting;
using PushClientLib.KeyValueStorage;
using PushClientLib.Models;
using PushClientLib.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.Web.Services
{
    internal class UserEntitySyncService<T> : BackgroundService where T : IBaseUserEntity
    {
        private const int SYNC_INTERVAL_MILI = 60000;
        private const string LAST_SYNCED_KEY_BASE = "USER_ENTITY_SYNC_SERVICE_LAST_UPDATED";

        private readonly string _lastSyncedKey;

        private readonly IAsyncUserEntityCachingRepository<T> _source;
        private readonly IAsyncUserEntityCachingRepository<T> _cache;
        private readonly IKeyValueStorage _keyValueStorage;

        private CancellationTokenSource _syncDelayTokenSource;

        public UserEntitySyncService(
            IAsyncUserEntityCachingRepository<T> source,
            IAsyncUserEntityCachingRepository<T> cache,
            IKeyValueStorage keyValueStorage)
        {
            _source = source;
            _cache = cache;
            _keyValueStorage = keyValueStorage;
            _lastSyncedKey = LAST_SYNCED_KEY_BASE + "_" + typeof(T).Name;
        }

        public bool SyncInProgress { get; private set; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    SyncInProgress = true;
                    _syncDelayTokenSource = new CancellationTokenSource();
                    Console.WriteLine("Syncing...");
                    await SyncAsync().ConfigureAwait(false);
                    Console.WriteLine("Syncing Done");
                    SyncInProgress = false;

                    CancellationTokenSource linkedToken =
                        CancellationTokenSource.CreateLinkedTokenSource(stoppingToken, _syncDelayTokenSource.Token);

                    await Task.Delay(SYNC_INTERVAL_MILI, linkedToken.Token).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    Console.Write("Syncing Exception: " + e.Message);
                }
            }
        }

        public void ForceSync()
        {
            SyncInProgress = false;
            _syncDelayTokenSource.Cancel();
        }

        private async Task SyncAsync()
        {
            DateTime newLastSynced = DateTime.UtcNow;
            DateTime lastSynced = GetLastSynced();
            Console.WriteLine("LAST SYNCED: " + lastSynced);

            try
            {
                Console.Write("SYNCING SOURCE TO CACHE");
                await SyncReposAsync(_source, _cache, lastSynced).ConfigureAwait(false);

                SetLastSynced(newLastSynced);
                Console.Write("SYNCING TIME UPDATED");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task SyncReposAsync(IAsyncUserEntityCachingRepository<T> source, IAsyncUserEntityCachingRepository<T> destination, DateTime lastSynced)
        {
            await SyncUpdatedAsync(lastSynced).ConfigureAwait(false);
            await SyncAddedAsync(lastSynced).ConfigureAwait(false);
            await SyncDeletedAsync(lastSynced).ConfigureAwait(false);
        }

        private async Task SyncAddedAsync(DateTime lastSynced)
        {
            await SyncAddedAsync(lastSynced, _cache, _source).ConfigureAwait(false);
            await SyncAddedAsync(lastSynced, _source, _cache).ConfigureAwait(false);
        }

        private async Task SyncAddedAsync(DateTime lastSynced,
            IAsyncUserEntityCachingRepository<T> source,
            IAsyncUserEntityCachingRepository<T> destination)
        {
            List<T> sourceAdded = await source.GetAddedSinceAsync(lastSynced).ConfigureAwait(false);

            await AddToRepoAsync(sourceAdded, destination).ConfigureAwait(false);
        }

        private async Task SyncDeletedAsync(DateTime lastSynced)
        {
            await SyncDeletedAsync(lastSynced, _cache, _source).ConfigureAwait(false);
            await SyncDeletedAsync(lastSynced, _source, _cache).ConfigureAwait(false);
        }
        private async Task SyncDeletedAsync(DateTime lastSynced,
            IAsyncUserEntityCachingRepository<T> source,
            IAsyncUserEntityCachingRepository<T> destination)
        {
            List<T> sourceDeleted = await source.GetDeletedSinceAsync(lastSynced).ConfigureAwait(false);

            await DeleteFromRepoAsync(sourceDeleted, destination).ConfigureAwait(false);
        }


        private async Task SyncUpdatedAsync(DateTime lastSynced)
        {
            List<T> sourceUpdated = await _source.GetUpdatedSinceAsync(lastSynced).ConfigureAwait(false);
            Console.Write("Updating from source: " + sourceUpdated.Count);

            List<T> cacheUpdated = await _cache.GetUpdatedSinceAsync(lastSynced).ConfigureAwait(false);
            Console.Write("Updating from cache: " + cacheUpdated.Count);

            await ResolveAnyConflicts(sourceUpdated, cacheUpdated).ConfigureAwait(false);

            Console.Write("updating to cache: " + sourceUpdated.Count);
            await UpdateRepoAsync(sourceUpdated, _cache).ConfigureAwait(false);
            Console.Write("updating to source: " + cacheUpdated.Count);
            await UpdateRepoAsync(cacheUpdated, _source).ConfigureAwait(false);
        }

        private async Task ResolveAnyConflicts(List<T> sourceUpdated, List<T> cacheUpdated)
        {
            for (int i = 0; i < cacheUpdated.Count; i++)
            {
                T updatedEntity = cacheUpdated[i];
                try
                {
                    //if sourceUpdated also contains updatedEntity - resolve conflict
                    T sourceDuplicate = sourceUpdated.First(x => x.Uuid == updatedEntity.Uuid);

                    updatedEntity = await ResolveConflictAsync(updatedEntity, sourceDuplicate);

                    //remove entity from a list depending on which was chosen in resolve conflict

                    if (updatedEntity.Equals(sourceDuplicate))
                    {
                        cacheUpdated.Remove(updatedEntity);
                    }
                    else
                    {
                        sourceUpdated.Remove(sourceDuplicate);
                    }
                }
                catch (InvalidOperationException)
                {
                    //no conflict
                }
            }
        }

        private async Task AddToRepoAsync(List<T> toAdd, IAsyncUserEntityCachingRepository<T> repo)
        {
            foreach (T t in toAdd)
            {
                await repo.AddAsync(t).ConfigureAwait(false);
            }
        }

        private async Task UpdateRepoAsync(List<T> toUpdate, IAsyncUserEntityCachingRepository<T> repo)
        {
            foreach (T t in toUpdate)
            {
                await repo.UpdateAsync(t).ConfigureAwait(false);
            }
        }

        private async Task DeleteFromRepoAsync(List<T> toDelete, IAsyncUserEntityCachingRepository<T> repo)
        {
            foreach (T t in toDelete)
            {
                await repo.DeleteAsync(t).ConfigureAwait(false);
            }
        }

        private Task<T> ResolveConflictAsync(T t, T cachedEntity)
        {
            Console.WriteLine("CONFLICT!!!");
            throw new NotImplementedException();
        }

        private DateTime GetLastSynced()
        {
            return _keyValueStorage.Get(_lastSyncedKey, new DateTime());
        }

        private void SetLastSynced(DateTime newLastSynced)
        {
            _keyValueStorage.Set(_lastSyncedKey, newLastSynced);
        }
    }
}
