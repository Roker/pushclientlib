﻿using PushClientLib.JsonConverters;
using PushClientLib.Web.Rest.Requests;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace PushClientLib.Web.Services
{
    public class PushRequestService : IPushRequestService
    {
        private readonly string _url;
        private readonly string _deviceId;

        private string _authToken;

        public PushRequestService(string url, string deviceId)
        {
            _url = url;
            _deviceId = deviceId;
        }

        public virtual Task<T> MakeUrlRequestAsync<T>(string url)
        {
            url = url.Replace(_url, "");
            IRestRequest request = new RestRequest(url);
            return MakeRequestAsync<T>(request);
        }

        public virtual async Task<T> MakeRequestAsync<T>(IRestRequest request)
        {
            IRestClient client = NewRestClient();
            Console.WriteLine("Making Request..: " + _url + request.Resource);

            ConfigRequest(ref request);

            return await RestRequestsUtil.MakeRequestAsync<T>(client, request);
        }

        protected virtual void ConfigRequest(ref IRestRequest request)
        {
            request.RequestFormat = DataFormat.Json;

            request.AddHeader("device_id", _deviceId);
            request.AddHeader("Accept", "application/json");
        }


        protected virtual IRestClient NewRestClient(bool noBaseUrl = false)
        {
            string url = noBaseUrl ? "" : _url;
            RestClient restClient = new RestClient(url);
            restClient.UseSerializer<JsonNetSerializer>();

            return restClient;
        }
    }
}
