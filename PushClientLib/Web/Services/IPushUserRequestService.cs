﻿namespace PushClientLib.Web.Services
{
    public interface IPushUserRequestService : IPushRequestService
    {
        string UserUuid { get; }
    }
}
