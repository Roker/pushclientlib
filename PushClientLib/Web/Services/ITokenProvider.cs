﻿using System.Threading.Tasks;

namespace PushClientLib.Web.Services
{
    public interface IAsyncTokenProvider
    {
        Task<string> GetTokenAsync();
    }
}
