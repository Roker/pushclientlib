﻿using RestSharp;
using System.Threading.Tasks;

namespace PushClientLib.Web.Services
{
    public interface IPushRequestService
    {
        Task<T> MakeRequestAsync<T>(IRestRequest request);
        Task<T> MakeUrlRequestAsync<T>(string url);
    }
}