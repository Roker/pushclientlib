﻿//using PushClientLib.Authenticators;
//using PushClientLib.Models;
//using PushClientLib.Web.Models;
//using PushClientLib.Web.Requests.Builders;
//using RestSharp;
//using System;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.Web.Services
//{
//    public class TokenProviderPushUserRequestService : PushRequestService, IPushUserRequestService, IDisposable
//    {
//        private string _authToken;
//        private IUser _user;

//        private readonly IUserRequestBuilder _userRequestBuilder = new UserRequestBuilder();

//        private readonly IPushAuthTokenProvider _authTokenProvider;

//        private Task _executingTask;
//        private readonly CancellationTokenSource _stoppingCts = new CancellationTokenSource();

//        public string UserUuid => _user.Uuid;

//        public TokenProviderPushUserRequestService(string url, string deviceId, IPushAuthTokenProvider authTokenProvider)
//            : base(url, deviceId)
//        {
//            _authTokenProvider = authTokenProvider;
//        }

//        public async Task<string> GetUserUuidAsync()
//        {
//            await RetrieveUserAsync().ConfigureAwait(false);
//            return _user.Uuid;
//        }

//        protected override IRestClient NewRestClient(bool noBaseUrl = false)
//        {
//            IRestClient restClient = base.NewRestClient(noBaseUrl);
//            restClient.Authenticator = new BearerAuthenticator(_authToken);
//            return restClient;
//        }

//        private async Task RetrieveUserAsync()
//        {
//            while (_user == null)
//            {
//                Console.WriteLine("retrieving user...");
//                try
//                {
//                    _user = await MakeRequestAsync<User>(_userRequestBuilder.BuildMeRequest());

//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine(e.Message);
//                }
//                Console.WriteLine("retrieving user: " + _user);
//                Console.WriteLine("retrieving user: " + _user?.Uuid);
//            }
//        }

//        private async Task RetrieveAuthTokenAsync()
//        {
//            while (string.IsNullOrEmpty(_authToken))
//            {
//                Console.WriteLine("retrieving auth token..");
//                _authToken = await _authTokenProvider.GetAuthTokenAsync().ConfigureAwait(false);
//            }
//        }

//        //IHostedService Funcs
//        protected async Task ExecuteAsync(CancellationToken stoppingToken)
//        {
//            while (!stoppingToken.IsCancellationRequested)
//            {
//                if (string.IsNullOrEmpty(_authToken))
//                {
//                    //TODO: EVENT INDICATING USER LOGGED OUT / AUTH PROBLEMS
//                    _user = null;
//                    await RetrieveAuthTokenAsync().ConfigureAwait(false);
//                }

//                if (_user == null)
//                {
//                    await RetrieveUserAsync().ConfigureAwait(false);
//                }
//            }
//        }
//        public virtual Task StartAsync(CancellationToken cancellationToken)
//        {
//            // Store the task we're executing
//            _executingTask = ExecuteAsync(_stoppingCts.Token);

//            // If the task is completed then return it, this will bubble cancellation and failure to the caller
//            if (_executingTask.IsCompleted)
//            {
//                return _executingTask;
//            }

//            // Otherwise it's running
//            return Task.CompletedTask;
//        }

//        public virtual async Task StopAsync(CancellationToken cancellationToken)
//        {
//            // Stop called without start
//            if (_executingTask == null)
//            {
//                return;
//            }

//            try
//            {
//                // Signal cancellation to the executing method
//                _stoppingCts.Cancel();
//            }
//            finally
//            {
//                // Wait until the task completes or the stop token triggers
//                await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
//            }

//        }

//        public virtual void Dispose()
//        {
//            _stoppingCts.Cancel();
//        }

//    }
//}
