﻿using Newtonsoft.Json;
using PushClientLib.JsonConverters;
using System.Collections.Generic;

namespace PushClientLib.Web.Models
{
    internal class CatalogEntry : BaseUserEntity
    {
        [JsonProperty(PropertyName = "catalogUuid")]
        public string CatalogUUID { get; set; }

        [JsonProperty(PropertyName = "entryType")]
        public CatalogEntryType EntryType { get; set; }

        [JsonProperty(PropertyName = "rating")]
        public int Rating { get; set; }

        [JsonProperty(PropertyName = "comments")]
        public string Comments { get; set; }

        [JsonConverter(typeof(BlobbedListConverter))]
        [JsonProperty(PropertyName = "tags")]
        public List<CatalogTag> Tags { get; set; }

    }

    public enum CatalogEntryType
    {
        ALBUM,
        TRACK
    }
}
