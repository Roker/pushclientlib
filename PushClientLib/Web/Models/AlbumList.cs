﻿using Newtonsoft.Json;
using PushClientLib.JsonConverters;
using PushClientLib.Models;
using System;
using System.Collections.Generic;

namespace PushClientLib.Web.Models
{
    public class AlbumList : BaseUserEntity, IAlbumList
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "year", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int Year { get; set; }

        [JsonConverter(typeof(BlobbedListConverter))]
        [JsonProperty(PropertyName = "spotifyIds")]
        public List<string> SpotifyIds { get; set; } = new List<string>();

        public int PreferedColumnCount { get; set; }

        public AlbumList()
        {
        }

        public AlbumList(string name)
        {
            Name = name;
            Uuid = Guid.NewGuid().ToString();
        }
    }
}
