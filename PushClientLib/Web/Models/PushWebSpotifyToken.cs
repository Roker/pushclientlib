﻿using Newtonsoft.Json;
using PushClientLib.JsonConverters;
using System;

namespace PushClientLib.Models
{
    public class PushWebSpotifyToken
    {
        [JsonProperty(PropertyName = "accessToken")]
        public string AccessToken { get; set; }

        [JsonConverter(typeof(MicrosecondEpochConverter))]
        [JsonProperty(PropertyName = "expiry")]
        public DateTime Expiry { get; set; }
    }
}
