﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushClientLib.Web.Models
{
    public class SpotifyIdOperation
    {
        [JsonProperty(PropertyName = "spotifyId")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "oldPos")]
        public int OldPosition { get; set; }

        [JsonProperty(PropertyName = "newPos")]
        public int NewPosition { get; set; }

        [JsonProperty(PropertyName = "operationType")]
        public SpotifyIdOperationType OperationType { get; set; }
    }

    public enum SpotifyIdOperationType
    {
        ADD,
        REMOVE,
        MOVE
    }
}
