﻿using Newtonsoft.Json;
using PushClientLib.Models;

namespace PushClientLib.Web.Models
{
    public class UserData : IUserData
    {
        [JsonProperty("user")]
        internal User User { get; set; }
        [JsonProperty("authToken")]
        public string AuthToken { get; set; }
        public IUser CurrentUser => User;
    }
}
