﻿using Newtonsoft.Json;
using PushClientLib.Models;
using System;
using System.Collections.Generic;

namespace PushClientLib.Web.Models
{
    public class BaseUserEntity : BaseEntity, IBaseUserEntity
    {
        [JsonProperty(PropertyName = "uuid")]
        public string Uuid { get; set; }

        [JsonProperty(PropertyName = "version")]
        public int Version { get; set; }

        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }

        [JsonProperty(PropertyName = "visibility")]
        public Visibility Visibility { get; set; }

        [JsonProperty(PropertyName = "deleted")]
        public bool Deleted { get; set; }

        public override bool Equals(object obj)
        {
            return obj is BaseUserEntity entity &&
                   base.Equals(obj) &&
                   Uuid == entity.Uuid;
        }

        public override int GetHashCode()
        {
            int hashCode = -815604760;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Uuid);
            return hashCode;
        }

        //public SyncState SyncState { get; set; } = SyncState.NEW;


    }
}
