﻿using PushClientLib.Models;

namespace PushClientLib.Web.Models
{
    internal class Friendship : IFriendship
    {
        public string SenderUuid { get; set; }
        public string ReceiverUuid { get; set; }
        public bool Accepted { get; set; }
    }
}
