﻿using Newtonsoft.Json;
using PushClientLib.JsonConverters;
using PushClientLib.Models;
using System;
using System.Collections.Generic;

namespace PushClientLib.Web.Models
{
    internal class User : BaseEntity, IUser
    {
        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "savedAlbumCount")]
        public int SavedAlbumCount { get; set; }

        [JsonProperty(PropertyName = "spotifyId")]
        public string SpotifyId { get; set; }

        [JsonConverter(typeof(MicrosecondEpochConverter))]
        [JsonProperty(PropertyName = "lastActive", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime LastActive { get; set; }

        [JsonProperty(PropertyName = "friendships")]
        public List<IFriendship> Friendships { get; set; }

        [JsonProperty(PropertyName = "incomingFriendshipRequests")]
        public List<IFriendship> IncomingFriendshipRequests { get; set; }

        [JsonProperty(PropertyName = "outgoingFriendshipRequests")]
        public List<IFriendship> OutgoingFriendshipRequests { get; set; }

        public User()
        {
        }
    }


}
