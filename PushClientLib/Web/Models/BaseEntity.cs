﻿using Newtonsoft.Json;
using PushClientLib.JsonConverters;
using PushClientLib.Models;
using System;
using System.Collections.Generic;

namespace PushClientLib.Web.Models
{
    public abstract class BaseEntity : IBaseEntity
    {

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonConverter(typeof(MicrosecondEpochConverter))]
        [JsonProperty(PropertyName = "created", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime Created { get; set; }

        [JsonConverter(typeof(MicrosecondEpochConverter))]
        [JsonProperty(PropertyName = "updated", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime Updated { get; set; }

        public override bool Equals(object obj)
        {
            return obj is BaseEntity entity &&
                   Id == entity.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<string>.Default.GetHashCode(Id);
        }



        ////uuid used locally
        //[JsonProperty(PropertyName = "localUuid")]
        //public string LocalUuid { get; set; }
    }
}
