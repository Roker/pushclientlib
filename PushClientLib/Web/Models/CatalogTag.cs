﻿namespace PushClientLib.Web.Models
{
    internal class CatalogTag : BaseUserEntity
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return Uuid;
        }
    }
}