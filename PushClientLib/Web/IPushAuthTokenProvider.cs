﻿using System.Threading.Tasks;

namespace PushClientLib.Web
{
    public interface IPushAuthTokenProvider
    {
        Task<string> GetAuthTokenAsync();
    }
}
