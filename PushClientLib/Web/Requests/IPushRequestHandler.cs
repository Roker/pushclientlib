﻿using System.Threading.Tasks;

namespace PushClientLib.Web.Requests
{
    internal interface IPushRequestHandler
    {
        Task ExecuteAsync(PushRequest pushRequest);
        Task<T> ExecuteAsync<T>(PushRequest<T> pushRequest);
    }
}