﻿using System.Net;

namespace PushClientLib.Web.Requests
{
    public class EntityResponse<T>
    {
        public T Entity { get; private set; }
        public HttpStatusCode StatusCode { get; private set; }

        public EntityResponse(T entity, HttpStatusCode statusCode)
        {
            Entity = entity;
            StatusCode = statusCode;
        }
    }
}
