﻿//using PushClientLib.Web.Models;
//using PushClientLib.Web.Requests.Builders;
//using PushClientLib.Web.Requests.Paging;
//using PushClientLib.Web.Services;
//using RokerPrime.Util;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;

namespace PushClientLib.Web.Requests.Services
{
    //namespace PushClientLib.Web.Requests.Services
    //{
    //    public class UserEntityRequestService<T> : IUserEntityRequestService<T> where T : IBaseUserEntity
    //    {
    //        private readonly IUserEntityRequestBuilder<T> _userEntityRequestBuilder;
    //        private readonly IPushRequestService _pushRequestService;
    //        private readonly string _userUuid;

    //        public UserEntityRequestService(IUserEntityRequestBuilder<T> userEntityRequestBuilder,
    //            IPushRequestService pushRequestService,
    //            string userUuid)
    //        {
    //            _userEntityRequestBuilder = userEntityRequestBuilder;
    //            _pushRequestService = pushRequestService;
    //            _userUuid = userUuid;
    //        }

    //        public Task<T> GetAsync(string userUuid, string entityUuid)
    //        {
    //            return _pushRequestService.MakeRequestAsync<T>(_userEntityRequestBuilder.NewGetRequest(userUuid, entityUuid));
    //        }

    //        public async Task<List<T>> GetAllAsync(string userUuid, DateTime? since)
    //        {
    //            Page<T> page =
    //                await _pushRequestService.MakeRequestAsync<Page<T>>(
    //                    _userEntityRequestBuilder.GetAllRequest(userUuid, since)
    //                    ).ConfigureAwait(false);

    //            return await RequestsUtil.GetAllAsync(page, _pushRequestService).ConfigureAwait(false);

    //        }
    //        public Task<List<T>> GetAsync(string userUuid, IEnumerable<string> entityUuids)
    //        {
    //            return _pushRequestService.MakeRequestAsync<long>(_userEntityRequestBuilder.NewGetRequest(userUuid));

    //        }

    //        public Task<long> GetTotalAsync(string userUuid)
    //        {
    //            return _pushRequestService.MakeRequestAsync<long>(_userEntityRequestBuilder.GetTotalRequest(userUuid));
    //        }

    //        public async Task<DateTime> GetLastUpdatedAsync(string userUuid)
    //        {
    //            long javaTimestamp = await _pushRequestService.MakeRequestAsync<long>(_userEntityRequestBuilder.GetLastUpdatedRequest(userUuid)).ConfigureAwait(false);
    //            return DateTimeUtil.FromJavaTimestamp(javaTimestamp);
    //        }

    //        //USER FUNCS
    //        public Task<T> GetAsync(string entityUuid)
    //        {
    //            return GetAsync(_userUuid, entityUuid);
    //        }

    //        public Task<List<T>> GetAsync(IEnumerable<string> entityUuids)
    //        {
    //            return GetAsync(_userUuid, entityUuids);
    //        }

    //        public Task<List<T>> GetAllAsync(DateTime? since = null)
    //        {
    //            return GetAllAsync(_userUuid, since);
    //        }

    //        public Task<long> GetTotalAsync()
    //        {
    //            return GetTotalAsync(_userUuid);
    //        }

    //        //MOD FUNCS
    //        public Task<T> AddAsync(T entity)
    //        {
    //            return _pushRequestService.MakeRequestAsync<T>(_userEntityRequestBuilder.AddRequest(_userUuid, entity));
    //        }
    //        public Task<T> UpdateAsync(T entity)
    //        {
    //            return _pushRequestService.MakeRequestAsync<T>(_userEntityRequestBuilder.UpdateRequest(_userUuid, entity));
    //        }
    //        public Task<List<T>> UpdateAsync(IEnumerable<T> entities)
    //        {
    //            return _pushRequestService.MakeRequestAsync<List<T>>(_userEntityRequestBuilder.UpdateRequest(_userUuid, entities));
    //        }

    //        public Task<T> DeleteAsync(T entity)
    //        {
    //            return _pushRequestService.MakeRequestAsync<T>(_userEntityRequestBuilder.DeleteRequest(_userUuid, entity));
    //        }


    //    }
    //}
}