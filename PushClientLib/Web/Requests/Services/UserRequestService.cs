﻿using PushClientLib.Models;
using PushClientLib.Web.Requests.Builders;
using PushClientLib.Web.Services;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests.Services
{
    public class UserRequestService : IUserRequestService
    {
        private readonly IUserRequestBuilder _userRequestBuilder;
        private readonly IPushRequestService _pushRequestService;

        public UserRequestService(IUserRequestBuilder userRequestBuilder,
                        IPushRequestService pushRequestService)
        {
            _userRequestBuilder = userRequestBuilder;
            _pushRequestService = pushRequestService;
        }

        public Task<IUser> GetUserAsync(string userUuid)
        {
            return _pushRequestService.MakeRequestAsync<IUser>(_userRequestBuilder.UserRequest(userUuid));
        }

        public Task<IUser> GetCurrentUserAsync()
        {
            return _pushRequestService.MakeRequestAsync<IUser>(_userRequestBuilder.BuildMeRequest());
        }
    }
}
