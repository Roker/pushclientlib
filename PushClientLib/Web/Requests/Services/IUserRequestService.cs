﻿using PushClientLib.Models;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests.Services
{
    public interface IUserRequestService
    {
        Task<IUser> GetCurrentUserAsync();
        Task<IUser> GetUserAsync(string userUuid);
    }
}