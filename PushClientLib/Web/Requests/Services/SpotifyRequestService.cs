﻿//using PushClientLib.Models;
//using PushClientLib.Web.Requests.Builders;
//using PushClientLib.Web.Services;
//using System.Threading.Tasks;

//namespace PushClientLib.Web.Requests.Services
//{
//    public class SpotifyRequestService : ISpotifyRequestService
//    {
//        private readonly ISpotifyRequestBuilder _spotifyRequestBuilder;
//        private readonly IPushUserRequestService _pushRequestService;

//        public SpotifyRequestService(
//           ISpotifyRequestBuilder spotifyRequestBuilder,
//           IPushUserRequestService pushRequestService)
//        {
//            _spotifyRequestBuilder = spotifyRequestBuilder;
//            _pushRequestService = pushRequestService;
//        }

//        public Task<PushWebSpotifyToken> GetSpotifyTokenAsync()
//        {
//            return _pushRequestService.MakeRequestAsync<PushWebSpotifyToken>(_spotifyRequestBuilder.GetAccessTokenRequest());
//        }
//    }
//}
