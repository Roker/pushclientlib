﻿using PushClientLib.Models;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests.Services
{
    public interface ISpotifyRequestService
    {
        Task<PushWebSpotifyToken> GetSpotifyTokenAsync();
    }
}