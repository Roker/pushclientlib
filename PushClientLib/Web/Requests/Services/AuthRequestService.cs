﻿//using PushClientLib.Models;
//using PushClientLib.Web.Models;
//using PushClientLib.Web.Requests.Builders;
//using PushClientLib.Web.Services;
//using RestSharp;
//using System.Threading.Tasks;

//namespace PushClientLib.Web.Requests.Services
//{
//    public class AuthRequestService : IAuthRequestService
//    {
//        private readonly IAuthRequestBuilder _authRequestBuilder;
//        private readonly IPushRequestService _pushRequestService;

//        public AuthRequestService(
//            IAuthRequestBuilder authRequestBuilder,
//            IPushRequestService pushRequestService)
//        {
//            _authRequestBuilder = authRequestBuilder;
//            _pushRequestService = pushRequestService;
//        }

//        public async Task<IUserData> SpotifyCodeAuthAsync(string code)
//        {
//            IRestRequest request = _authRequestBuilder.SpotifyCodeAuthRequest(code);
//            try
//            {
//                return await _pushRequestService.MakeRequestAsync<UserData>(request).ConfigureAwait(false);
//            }
//            catch
//            {
//                return null;
//            }
//        }
//    }
//}
