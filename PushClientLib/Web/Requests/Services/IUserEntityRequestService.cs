﻿using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests.Services
{
    public interface IUserEntityRequestService<T> where T : IBaseUserEntity
    {
        Task<T> GetAsync(string entityUuid);
        Task<List<T>> GetAsync(IEnumerable<string> entityUuids);
        Task<List<T>> GetAllAsync(DateTime? since = null);
        Task<T> AddAsync(T entity);
        Task<T> DeleteAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task<List<T>> UpdateAsync(IEnumerable<T> entities);
        Task<long> GetTotalAsync();
        //Task<DateTime> GetLastUpdatedAsync(string userUuid);

        Task<T> GetAsync(string userUuid, string entityUuid);
        Task<List<T>> GetAsync(string userUuid, IEnumerable<string> entityUuids);
        Task<List<T>> GetAllAsync(string userUuid, DateTime? since = null);
        Task<long> GetTotalAsync(string userUuid);


    }
}