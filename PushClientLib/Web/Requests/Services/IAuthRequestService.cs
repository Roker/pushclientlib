﻿using PushClientLib.Models;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests.Services
{
    public interface IAuthRequestService
    {
        Task<IUserData> SpotifyCodeAuthAsync(string code);
    }
}