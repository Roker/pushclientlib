﻿using PushClientLib.Web.Requests.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests
{
    internal static class PushRequestUtil
    {
        internal static async Task<List<T>> GetAllPages<T>(PushRequest<Page<T>> request, IPushRequestHandler requestHandler)
        {
            List<T> entities = new List<T>();
            Page<T> page = await requestHandler.ExecuteAsync(request).ConfigureAwait(false);

            if(page.Entities.Count > 0)
            {
                entities.AddRange(page.Entities);
            }


            if (!string.IsNullOrEmpty(page.Links.Next))
            {
                entities.AddRange(await GetAllPages<T>(new PushRequest<Page<T>>(page.Links.Next), requestHandler).ConfigureAwait(false));
            }

            return entities;
        }
    }
}
