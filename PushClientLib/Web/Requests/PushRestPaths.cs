﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushClientLib.Web.Requests
{
    public static class PushRestPaths
    {
        public static readonly string USER_ENTITY_PATH = $"users/{{{PushPathParamKeys.USER_ID}}}/{{entityPath}}";
        public static readonly string SPOTIFY_IDS_PATH = $"users/{{{PushPathParamKeys.USER_ID}}}/{{entityPath}}";
    }
}
