﻿using System.Collections.Generic;

namespace PushClientLib.Web.Requests.Paging
{
    public class PageData<T>
    {
        public List<T> Entities { get; set; }
    }
}