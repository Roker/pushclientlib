﻿using Newtonsoft.Json;

namespace PushClientLib.Web.Requests.Paging
{
    public class PagingInfo
    {
        [JsonProperty(PropertyName = "size")]
        public int Limit { get; set; }

        [JsonProperty(PropertyName = "totalElements")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "totalPages")]
        public int PageCount { get; set; }

        [JsonProperty(PropertyName = "number")]
        public int PageNum { get; set; }
    }
}