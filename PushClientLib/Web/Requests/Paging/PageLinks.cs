﻿using Newtonsoft.Json;

namespace PushClientLib.Web.Requests.Paging
{
    public class PageLinks
    {

        public string Self => SelfLink?.Value;

        public string First => FirstLink?.Value;

        public string Last => LastLink?.Value;

        public string Next => NextLink?.Value;

        public string Previous => PreviousLink?.Value;

        [JsonProperty("self")]
        public PageLink SelfLink { get; set; }

        [JsonProperty("first")]
        public PageLink FirstLink { get; set; }

        [JsonProperty("last")]
        public PageLink LastLink { get; set; }

        [JsonProperty("next")]
        public PageLink NextLink { get; set; }

        [JsonProperty("previous")]
        public PageLink PreviousLink { get; set; }

    }
}