﻿using Newtonsoft.Json;

namespace PushClientLib.Web.Requests.Paging
{
    public class PageLink
    {
        [JsonProperty("href")]
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}