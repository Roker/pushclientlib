﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace PushClientLib.Web.Requests.Paging
{
    internal class Page<T>
    {

        public List<T> Entities {
            get {
                if (PageData != null)
                {
                    try
                    {
                        return JsonConvert.DeserializeObject<List<T>>(PageData.First.First.ToString());

                    }
                    catch
                    {
                    }
                }
                return new List<T>();
            }
        }
        [JsonProperty(PropertyName = "_links")]
        public PageLinks Links { get; set; }

        public int PageNum => PagingInfo.PageNum;

        public int Limit => PagingInfo.Limit;

        public int PageCount => PagingInfo.PageCount;

        public int Count => PagingInfo.Count;

        [JsonProperty(PropertyName = "page")]
        public PagingInfo PagingInfo { get; set; }

        [JsonConverter(typeof(JObjectConverter))]
        [JsonProperty(PropertyName = "_embedded")]
        public JObject PageData { get; set; }
    }
}
