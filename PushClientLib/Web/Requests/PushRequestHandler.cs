﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests
{
    internal class PushRequestHandler : IPushRequestHandler
    {
        private IRequestExecutor _requestExecutor;

        public PushRequestHandler(IRequestExecutor requestExecutor)
        {
            _requestExecutor = requestExecutor;
        }
        public async Task ExecuteAsync(PushRequest pushRequest)
        {
            PushResponse pushResponse = await _requestExecutor.ExecuteAsync(pushRequest).ConfigureAwait(false);
            if (!pushResponse.IsSuccessful)
            {
                //TODO: error handling
                throw new Exception("ERROR");
            }
        }

        public async Task<T> ExecuteAsync<T>(PushRequest<T> pushRequest)
        {
            PushResponse<T> pushResponse = await _requestExecutor.ExecuteAsync<T>(pushRequest).ConfigureAwait(false);
            if (pushResponse.IsSuccessful)
            {
                return pushResponse.Data;
            }
            else
            {
                //TODO: error handling
                throw new Exception("ERROR");
            }
        }

        
    }
}
