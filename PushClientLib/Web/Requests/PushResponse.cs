﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace PushClientLib.Web.Requests
{
    public class PushResponse : BaseWebCom
    {
        public PushResponse(string resource) : base(resource)
        {
        }

        public bool IsSuccessful => (int)StatusCode < 300;
        public HttpStatusCode StatusCode { get; set; }

        public string Content { get; set; }
    }

    public class PushResponse<T> : PushResponse
    {
        public PushResponse(string resource) : base(resource)
        {
        }

        public T Data { get; set; }
    }
}
