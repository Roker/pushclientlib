﻿using System.Collections.Generic;
using System.Linq;

namespace PushClientLib.Web.Requests
{
    public class PushRequest : BaseWebCom
    {
        private Dictionary<string, string> _queryParams = new Dictionary<string, string>();
        private Dictionary<string, string> _pathParams = new Dictionary<string, string>();

        public PushRequest(string resource) : base(resource)
        {
        }

        public object Body { get; set; }

        public List<KeyValuePair<string, string>> QueryParams { get => _queryParams.ToList(); }
        public List<KeyValuePair<string, string>> PathParams { get => _pathParams.ToList(); }

        public void AddQueryParam(string key, string value)
        {
            _queryParams[key] = value;
        }

        public void AddPathParam(string key, string value)
        {
            _pathParams[key] = value;
        }
    }

    public class PushRequest<T> : PushRequest
    {
        public PushRequest(string resource) : base(resource)
        {
        }
    }
}
