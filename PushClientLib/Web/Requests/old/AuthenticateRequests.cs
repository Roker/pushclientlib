﻿using RestSharp;

namespace PushClientLib.Web.Requests.old
{
    public static class AuthenticateRequests
    {
        public static IRestRequest SpotifyCodeAuthRequest(string code)
        {
            IRestRequest request = new RestRequest("authenticate/spotify-code/{code}");
            request.AddUrlSegment("code", code);
            return request;
        }
    }
}
