﻿using RestSharp;

namespace PushClientLib.Web.Requests.old
{
    internal static class SpotifyRequests
    {
        private const string URL = "/spotify";

        internal static IRestRequest GetAccessTokenRequest()
        {
            return new RestRequest(URL + "/access-token");
        }
    }
}
