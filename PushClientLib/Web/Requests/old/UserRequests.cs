﻿using RestSharp;

namespace PushClientLib.Web.Requests.old
{
    internal static class UserRequests
    {
        private const string BASE_USER_URL = "users";
        private const string USER_URL = BASE_USER_URL + "/{userUuid}";
        private const string USER_UUID_PATH = "userUuid";


        internal static IRestRequest BaseRequest()
        {
            IRestRequest request = new RestRequest(BASE_USER_URL);
            return request;
        }
        internal static IRestRequest UserRequest(string userUuid)
        {
            IRestRequest request = new RestRequest(USER_URL);
            return request.AddUrlSegment(USER_UUID_PATH, userUuid);
        }

        internal static IRestRequest BuildMeRequest()
        {
            IRestRequest request = BaseRequest();
            request.Resource += "/me";
            return request;
        }
    }
}
