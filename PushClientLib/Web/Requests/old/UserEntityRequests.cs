﻿using PushClientLib.Web.Models;
using RestSharp;
using Roker.Util;
using System;

namespace PushClientLib.Web.Requests.old
{
    internal class UserEntityRequests
    {
        private const string BASE_USER_ENTITIES_URL = "users/{userUuid}/{entityPath}";
        private const string USER_ENTITY_URL = BASE_USER_ENTITIES_URL + "/{entityUuid}";

        private static IRestRequest BaseEntityRequestBuilder<T>(string userUuid) where T : BaseEntity
        {
            IRestRequest request = new RestRequest(BASE_USER_ENTITIES_URL)
                .AddUrlSegment("userUuid", userUuid)
                .AddUrlSegment("entityPath", GetEntityPath<T>());

            return request;

        }
        private static IRestRequest EntityRequestBuilder<T>(string userUuid, string entityUuid) where T : BaseEntity
        {
            IRestRequest request = new RestRequest(USER_ENTITY_URL)
                .AddUrlSegment("userUuid", userUuid)
                .AddUrlSegment("entityPath", GetEntityPath<T>())
                .AddUrlSegment("entityUuid", entityUuid);
            return request;

        }

        internal static IRestRequest GetRequest<T>(string userUuid, string entityUuid) where T : BaseEntity
        {
            return EntityRequestBuilder<T>(userUuid, entityUuid);
        }

        internal static IRestRequest GetAllRequest<T>(string userUuid) where T : BaseEntity
        {
            return BaseEntityRequestBuilder<T>(userUuid);
        }

        internal static IRestRequest GetUpdatedSinceRequest<T>(string userUuid, DateTime since) where T : BaseEntity
        {
            IRestRequest req = BaseEntityRequestBuilder<T>(userUuid);
            req.AddQueryParameter("since", DateTimeUtil.ToJavaTimeStamp(since).ToString());
            return req;
        }

        internal static IRestRequest UpdateRequest<T>(string userUuid, T entity) where T : BaseEntity
        {
            IRestRequest request = EntityRequestBuilder<T>(userUuid, entity.Uuid);
            request.Method = Method.PUT;

            return request.AddJsonBody(entity);
        }



        internal static IRestRequest DeleteRequest<T>(string userUuid, T entity) where T : BaseEntity
        {
            IRestRequest request = EntityRequestBuilder<T>(userUuid, entity.Uuid);
            request.Method = Method.DELETE;

            return request;
        }

        internal static IRestRequest AddRequest<T>(string userUuid, T entity) where T : BaseEntity
        {
            IRestRequest request = BaseEntityRequestBuilder<T>(userUuid);
            request.Method = Method.POST;

            return request.AddJsonBody(entity);
        }

        internal static string GetEntityPath<T>() where T : BaseEntity
        {
            Console.WriteLine("get entity path");
            Type type = typeof(T);
            if (type == typeof(AlbumList))
            {
                return "lists";
            }
            else if (type == typeof(CatalogEntry))
            {
                return "catalogEntries";
            }
            else
            {
                throw new InvalidOperationException("UserEntityRequestsUtil type not recognized");
            }
        }
    }
}
