﻿using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PushClientLib.Web.Requests
{
    internal interface IRequestExecutor
    {
        IUserData UserData { get; set; }

        Task<PushResponse> ExecuteAsync(PushRequest request);
        Task<PushResponse<T>> ExecuteAsync<T>(PushRequest<T> request);
    }
}
