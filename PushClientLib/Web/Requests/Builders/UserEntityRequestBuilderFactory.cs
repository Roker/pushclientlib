﻿using PushClientLib.Models;
using PushClientLib.Web.Models;
using System;

namespace PushClientLib.Web.Requests.Builders
{
    internal class UserEntityRequestBuilderFactory
    {
        public static UserEntityRequestBuilder<T> NewUserEntityRequestBuilder<T>() where T : BaseUserEntity
        {
            string entityPath;

            if (typeof(T) == typeof(AlbumList))
            {
                entityPath = "lists";
            }
            else
            {
                throw new Exception("Type error");
            }

            return new UserEntityRequestBuilder<T>(entityPath);
        }
    }
}
