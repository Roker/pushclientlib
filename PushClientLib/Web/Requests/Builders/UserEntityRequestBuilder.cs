﻿//using PushClientLib.Models;
//using RestSharp;
//using RokerPrime.Util;
//using System;
//using System.Collections.Generic;

//namespace PushClientLib.Web.Requests.Builders
//{
//    public class UserEntityRequestBuilder<T> : IUserEntityRequestBuilder<T> where T : IBaseEntity
//    {
//        private const string BASE_USER_ENTITIES_URL = "users/{userUuid}/{entityPath}";
//        private const string USER_ENTITY_URL = BASE_USER_ENTITIES_URL + "/{entityUuid}";

//        private readonly string _entityPath;

//        public UserEntityRequestBuilder(string entityPath)
//        {
//            _entityPath = entityPath;
//        }

//        private IRestRequest NewUserRequest(string userUuid)
//        {
//            IRestRequest request = new RestRequest(BASE_USER_ENTITIES_URL)
//                .AddUrlSegment("userUuid", userUuid)
//                .AddUrlSegment("entityPath", _entityPath);


//            return request;

//        }
//        private IRestRequest NewEntityRequest(string userUuid, string entityUuid)
//        {
//            IRestRequest request = new RestRequest(USER_ENTITY_URL)
//                .AddUrlSegment("userUuid", userUuid)
//                .AddUrlSegment("entityPath", _entityPath)
//                .AddUrlSegment("entityUuid", entityUuid);
//            return request;

//        }

//        public IRestRequest GetRequest(string userUuid, string entityUuid)
//        {
//            return NewEntityRequest(userUuid, entityUuid);
//        }


//        public IRestRequest GetRequest(string userUuid, IEnumerable<string> entityUuid)
//        {
//            throw new NotImplementedException();
//        }

//        public IRestRequest GetAllRequest(string userUuid)
//        {
//            IRestRequest request = NewUserRequest(userUuid);

//            return request;
//        }

//        public IRestRequest GetAddedSinceRequest(string userUuid, DateTime since)
//        {
//            IRestRequest request = GetSinceRequest(userUuid, since);
//            request.Resource += "/new";
//            return request;
//        }

//        public IRestRequest GetUpdatedSinceRequest(string userUuid, DateTime since)
//        {
//            IRestRequest request = GetSinceRequest(userUuid, since);
//            return request;
//        }

//        public IRestRequest GetDeletedSinceRequest(string userUuid, DateTime since)
//        {
//            IRestRequest request = GetSinceRequest(userUuid, since);
//            request.Resource += "/deleted";
//            return request;
//        }


//        public IRestRequest UpdateRequest(string userUuid, T entity)
//        {
//            Console.WriteLine("Update REquest: " + entity.Uuid);
//            IRestRequest request = NewEntityRequest(userUuid, entity.Uuid);
//            request.Method = Method.PUT;

//            return request.AddJsonBody(entity);
//        }

//        public IRestRequest UpdateCheckConflictingRequest(string userUuid, T t, DateTime lastSynced)
//        {
//            IRestRequest request = UpdateRequest(userUuid, t);
//            request.AddQueryParameter("lastSynced", DateTimeUtil.ToJavaTimeStamp(lastSynced).ToString());
//            return request;
//        }

//        public IRestRequest DeleteRequest(string userUuid, T entity)
//        {
//            IRestRequest request = NewEntityRequest(userUuid, entity.Uuid);
//            request.Method = Method.DELETE;

//            return request;
//        }

//        public IRestRequest AddRequest(string userUuid, T entity)
//        {
//            IRestRequest request = NewUserRequest(userUuid);
//            request.Method = Method.POST;

//            return request.AddJsonBody(entity);
//        }

//        public IRestRequest GetTotalRequest(string userUuid)
//        {
//            IRestRequest request = NewUserRequest(userUuid);
//            request.Resource += "/count";
//            return request;
//        }

//        private IRestRequest GetSinceRequest(string userUuid, DateTime since)
//        {

//            IRestRequest request = NewUserRequest(userUuid);

//            if (since != null)
//            {
//                request.AddQueryParameter("since", (DateTimeUtil.ToJavaTimeStamp(since)).ToString());
//            }
//            return request;
//        }


//    }
//}
