﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushClientLib.Web.Requests.Builders.NewBuilders
{
    public class AuthRequestBuilder : IAuthRequestBuilder
    {
        private const string SPOTIFY_CODE_AUTH_RESOURCE = "authentication/spotify-code";
        public PushRequest SpotifyCodeAuthRequest(string code)
        {
            PushRequest request = new PushRequest(SPOTIFY_CODE_AUTH_RESOURCE);
            request.AddQueryParam("code", code);
            return request;
        }
    }
}
