﻿using PushClientLib.Models;
using PushClientLib.Web.Models;
using PushClientLib.Web.Requests.Paging;
using RokerPrime.Util;
using System;
using System.Collections.Generic;

namespace PushClientLib.Web.Requests.Builders
{
    internal class UserEntityRequestBuilder<T> : IUserEntityRequestBuilder<T> where T : IBaseEntity
    {
        private static readonly string _baseUserEntityUrl = $"{{entityPath}}";
        private static readonly string _userEntityUrl = $"{_baseUserEntityUrl}/{{entityId}}";

        private readonly string _entityPath;

        public UserEntityRequestBuilder(string entityPath)
        {
            _entityPath = entityPath;
        }

        public PushRequest<T> GetRequest(string entityId)
        {
            PushRequest<T> req = NewEntityRequest<T>(entityId);
            req.Method = PushRequestMethod.GET;
            return req;
        }
        public PushRequest<T> UpdateRequest(T entity)
        {
            PushRequest<T> req = NewEntityRequest<T>(entity.Id);
            req.Method = PushRequestMethod.PUT;
            req.Body = entity;
            return req;
        }

        public PushRequest DeleteRequest(T entity)
        {
            PushRequest req = NewEntityRequest(entity.Id);
            req.Method = PushRequestMethod.DELETE;
            return req;
        }

        public PushRequest<T> AddRequest(T entity)
        {
            PushRequest<T> req = NewRequest<T>();
            req.Method = PushRequestMethod.POST;
            req.Body = entity;
            return req;
        }

        public PushRequest<Page<T>> GetAllRequest(int offset, int limit)
        {
            PushRequest<Page<T>> req = NewPagedRequest(offset, limit);
            req.Method = PushRequestMethod.GET;
            return req;
        }

        public PushRequest<Page<T>> GetAddedSinceRequest(DateTime since, int offset = 0, int limit = 0)
        {
            PushRequest<Page<T>> req = NewSinceRequest<Page<T>>(since, offset, limit);
            req.Resource += "/new";
            return req;
        }

        public PushRequest<Page<T>> GetUpdatedSinceRequest(DateTime since, int offset = 0, int limit = 0)
        {
            PushRequest<Page<T>> req = NewSinceRequest<Page<T>>(since, offset, limit);
            return req;
        }

        public PushRequest<Page<T>> GetDeletedSinceRequest(DateTime since, int offset = 0, int limit = 0)
        {
            PushRequest<Page<T>> req = NewSinceRequest<Page<T>>(since, offset, limit);
            req.Resource += "/deleted";
            return req;
        }

        public PushRequest<long> GetTotalRequest()
        {
            PushRequest<long> req = NewRequest<long>();
            req.Method = PushRequestMethod.GET;
            req.Resource += "/count";

            return req;
        }

        private PushRequest<U> NewRequest<U>()
        {
            PushRequest<U> req = new PushRequest<U>(_baseUserEntityUrl);
            req.AddPathParam("entityPath", _entityPath);
            return req;
        }

        private PushRequest<U> NewEntityRequest<U>(string entityId)
        {
            PushRequest<U> req = new PushRequest<U>(_userEntityUrl);
            req.AddPathParam("entityPath", _entityPath);
            req.AddPathParam("entityId", entityId);
            return req;
        }

        private PushRequest NewRequest()
        {
            PushRequest req = new PushRequest(_baseUserEntityUrl);
            req.AddPathParam("entityPath", _entityPath);
            return req;
        }

        private PushRequest NewEntityRequest(string entityId)
        {
            PushRequest req = new PushRequest(_userEntityUrl);
            req.AddPathParam("entityPath", _entityPath);
            req.AddPathParam("entityId", entityId);
            return req;
        }

        private PushRequest<Page<T>> NewPagedRequest(int offset, int limit)
        {
            PushRequest<Page<T>> req = NewRequest<Page<T>>();
            req.AddQueryParam("offset", offset.ToString());
            req.AddQueryParam("limit", limit.ToString());
            return req;
        }
        private PushRequest<Page<T>> NewSinceRequest<U>(DateTime since, int offset, int limit)
        {
            PushRequest<Page<T>> req = NewPagedRequest(offset, limit);
            req.AddQueryParam("since", (DateTimeUtil.ToJavaTimeStamp(since)).ToString());

            return req;
        }




    }
}
