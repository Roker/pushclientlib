﻿namespace PushClientLib.Web.Requests.Builders.NewBuilders
{
    public interface IAuthRequestBuilder
    {
        PushRequest SpotifyCodeAuthRequest(string code);
    }
}