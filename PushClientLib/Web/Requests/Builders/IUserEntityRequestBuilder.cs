﻿using PushClientLib.Models;
using PushClientLib.Web.Requests.Paging;
using RestSharp;
using System;

namespace PushClientLib.Web.Requests.Builders
{
    internal interface IUserEntityRequestBuilder<T> where T : IBaseEntity
    {
        PushRequest<T> GetRequest(string entityUuid);
        PushRequest<T> UpdateRequest(T entity);
        PushRequest<T> AddRequest(T entity);
        PushRequest DeleteRequest(T entity);
        PushRequest<Page<T>> GetAllRequest(int offset = 0, int count = 0);
        PushRequest<Page<T>> GetUpdatedSinceRequest(DateTime since, int offset = 0, int limit = 0);
        PushRequest<Page<T>> GetAddedSinceRequest(DateTime since, int offset = 0, int limit = 0);
        PushRequest<Page<T>> GetDeletedSinceRequest(DateTime since, int offset = 0, int limit = 0);
       
        PushRequest<long> GetTotalRequest();
        //PushRequest UpdateCheckConflictingRequest(T t, DateTime lastSynced);
    }
}