﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PushClientLib.Web.Requests
{
    public abstract class BaseWebCom
    {
        private Dictionary<string, string> _headers = new Dictionary<string, string>();

        public BaseWebCom(string resource)
        {
            Resource = resource;
        }

        public string Resource { get; set; }
        public PushRequestMethod Method { get; set; } = PushRequestMethod.GET;

        public List<KeyValuePair<string, string>> Headers { get => _headers.ToList(); }

        public void AddHeader(string key, string value)
        {
            _headers[key] = value;
        }
    }

    public enum PushRequestMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
