﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace PushClientLib.Web.Requests
{
    internal class JObjectConverter : JsonConverter<JObject>
    {
        public override JObject ReadJson(JsonReader reader, Type objectType, JObject existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            return JObject.Load(reader);
        }

        public override void WriteJson(JsonWriter writer, JObject value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }
    }
}