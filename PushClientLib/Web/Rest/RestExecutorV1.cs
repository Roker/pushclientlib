﻿//using PushClientLib.Authenticators;
//using PushClientLib.JsonConverters;
//using PushClientLib.Models;
//using PushClientLib.Web.Requests;
//using RestSharp;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading.Tasks;

//namespace PushClientLib.Web.Rest.Requests
//{
//    public class RestExecutor : IRequestExecutor
//    {
//        private readonly string _url;
//        private readonly IUserData _userData;
//        private readonly IRestClient _client;
//        private readonly string _deviceId;


//        public RestExecutor(string url, IUserData userData)
//        {
//            _url = url;
//            _userData = userData;

//            _client = new RestClient(_url);
//            InitClient();
//        }

//        private void InitClient()
//        {
//            _client.UseSerializer<JsonNetSerializer>();
//            _client.Authenticator = new BearerAuthenticator(_userData.AuthToken);
//        }

//        public event EventHandler AuthorizationFailed;


//        public async Task ExecuteAsync(PushRequest pushRequest)
//        {
//            IRestRequest restRequest = ConvertRequest(pushRequest);
//            ConfigRequest(ref restRequest);


//            //TODO: error handling
//            await _client.ExecuteAsync<string>(restRequest).ConfigureAwait(false);
//        }

//        public async Task<T> ExecuteAsync<T>(PushRequest<T> pushRequest)
//        {
//            IRestRequest restRequest = ConvertRequest(pushRequest);
//            ConfigRequest(ref restRequest);

//            IRestResponse<T> restResponse = await _client.ExecuteAsync<T>(restRequest).ConfigureAwait(false);
//            //if(pushRequest.Method == PushRequestMethod.GET)
//            //{
//            //    throw new Exception(restRequest.Resource);
//            //    //throw new Exception(restResponse.Content);
//            //}
//            if (restResponse.IsSuccessful)
//            {
//                return restResponse.Data;
//            } else
//            {
                
//                throw new Exception(restResponse.Content);
//                //throw restResponse.ErrorException;
//            }
//        }


//        protected virtual void ConfigRequest(ref IRestRequest request)
//        {
//            request.RequestFormat = DataFormat.Json;

//            request.AddUrlSegment(PushPathParamKeys.USER_UUID, _userData.CurrentUser.Uuid);
//            request.AddHeader("device_id", _deviceId);
//            request.AddHeader("Accept", "application/json");
//        }

//        private IRestRequest ConvertRequest(PushRequest pushRequest)
//        {
//            IRestRequest restRequest = new RestRequest(pushRequest.Resource);

//            restRequest.Method = ConvertMethod(pushRequest.Method);
//            restRequest.AddJsonBody(pushRequest.Body);
            
//            foreach (KeyValuePair<string, string> header in pushRequest.Headers)
//            {
//                restRequest.AddHeader(header.Key, header.Value);
//            }

//            foreach (KeyValuePair<string, string> param in pushRequest.QueryParams)
//            {
//                restRequest.AddQueryParameter(param.Key, param.Value);
//            }

//            foreach (KeyValuePair<string, string> param in pushRequest.PathParams)
//            {
//                restRequest.AddUrlSegment(param.Key, param.Value);
//            }

//            return restRequest;
//        }

//        private Method ConvertMethod(PushRequestMethod method)
//        {
//            switch (method)
//            {
//                case PushRequestMethod.DELETE:
//                    return Method.DELETE;
//                case PushRequestMethod.GET:
//                    return Method.GET;
//                case PushRequestMethod.PUT:
//                    return Method.PUT;
//                case PushRequestMethod.POST:
//                    return Method.POST;
//                default:
//                    throw new InvalidOperationException("Invalid method type");
//            }
//        }

//    }
//}
