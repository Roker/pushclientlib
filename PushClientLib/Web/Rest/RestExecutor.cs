﻿using PushClientLib.Authenticators;
using PushClientLib.JsonConverters;
using PushClientLib.Models;
using PushClientLib.Web.Requests;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.Web.Rest.Requests
{
    public class RestExecutor : IRequestExecutor
    {
        private const string AUTH_HEADER_KEY = "Authorization";
        private const string BEARER_TOKEN_PREFIX = "Bearer ";


        private readonly string _url;
        private readonly IRestClient _client;
        private readonly string _deviceId;

        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);

        public IUserData UserData { get; set; }


        public RestExecutor(string url)
        {
            _url = url;

            _client = new RestClient(_url);
            InitClient();
        }

        private void InitClient()
        {
            _client.UseSerializer<JsonNetSerializer>();
            //_client.Authenticator = new BearerAuthenticator(_userData.AuthToken);
        }

        public async Task<PushResponse<T>> ExecuteAsync<T>(PushRequest<T> pushRequest)
        {
            IRestRequest restRequest = PushToRestRequest(pushRequest);
            ConfigRequest(ref restRequest);

            await _semaphoreSlim.WaitAsync();
            try
            {
                IRestResponse<T> restResponse = await _client.ExecuteAsync<T>(restRequest).ConfigureAwait(false);
                return RestToPushResponse(restResponse);
            }
            finally
            {
                _semaphoreSlim.Release();
            }
        }

        public async Task<PushResponse> ExecuteAsync(PushRequest pushRequest)
        {
            IRestRequest restRequest = PushToRestRequest(pushRequest);
            ConfigRequest(ref restRequest);

            await _semaphoreSlim.WaitAsync();
            try
            {
                IRestResponse restResponse = await _client.ExecuteAsync(restRequest, restRequest.Method).ConfigureAwait(false);
                return RestToPushResponse(restResponse);
            }
            finally
            {
                _semaphoreSlim.Release();
            }

        }

        private PushResponse RestToPushResponse(IRestResponse restResponse)
        {
            PushResponse pushResponse = new PushResponse(restResponse.Request.Resource);
            pushResponse.StatusCode = restResponse.StatusCode;
            pushResponse.Content = restResponse.Content;
            foreach (Parameter param in restResponse.Headers)
            {
                pushResponse.AddHeader(param.Name, param.Value.ToString());
            }
            return pushResponse;
        }
        private PushResponse<T> RestToPushResponse<T>(IRestResponse<T> restResponse)
        {
            PushResponse<T> pushResponse = new PushResponse<T>(restResponse.Request.Resource);
            pushResponse.StatusCode = restResponse.StatusCode;
            pushResponse.Content = restResponse.Content;
            pushResponse.Data = restResponse.Data;
            foreach(Parameter param in restResponse.Headers)
            {
                pushResponse.AddHeader(param.Name, param.Value.ToString());
            }
            return pushResponse;
        }


        private IRestRequest PushToRestRequest(PushRequest pushRequest)
        {
            IRestRequest restRequest = new RestRequest(pushRequest.Resource);

            restRequest.Method = ConvertMethod(pushRequest.Method);
            restRequest.AddJsonBody(pushRequest.Body);

            foreach (KeyValuePair<string, string> header in pushRequest.Headers)
            {
                restRequest.AddHeader(header.Key, header.Value);
            }

            foreach (KeyValuePair<string, string> param in pushRequest.QueryParams)
            {
                restRequest.AddQueryParameter(param.Key, param.Value);
            }

            foreach (KeyValuePair<string, string> param in pushRequest.PathParams)
            {
                restRequest.AddUrlSegment(param.Key, param.Value);
            }

            return restRequest;
        }

        protected virtual void ConfigRequest(ref IRestRequest request)
        {
            request.RequestFormat = DataFormat.Json;

            if(UserData != null)
            {
                request.AddUrlSegment(PushPathParamKeys.USER_ID, UserData.CurrentUser.Id);
                request.AddHeader(AUTH_HEADER_KEY, BEARER_TOKEN_PREFIX + UserData.AuthToken);
            }
            request.AddHeader("device_id", _deviceId);
            request.AddHeader("Accept", "application/json");
        }


        private Method ConvertMethod(PushRequestMethod method)
        {
            switch (method)
            {
                case PushRequestMethod.DELETE:
                    return Method.DELETE;
                case PushRequestMethod.GET:
                    return Method.GET;
                case PushRequestMethod.PUT:
                    return Method.PUT;
                case PushRequestMethod.POST:
                    return Method.POST;
                default:
                    throw new InvalidOperationException("Invalid method type");
            }
        }

    }
}
