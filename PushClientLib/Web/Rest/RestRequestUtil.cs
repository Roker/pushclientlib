﻿using Newtonsoft.Json;
using PushClientLib.Models;
using PushClientLib.Web.Requests;
using PushClientLib.Web.Requests.Paging;
using PushClientLib.Web.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Rest.Requests
{
    internal static class RestRequestsUtil
    {
        internal static async Task<T> MakeRequestAsync<T>(IRestClient client, IRestRequest request)
        {
            EntityResponse<T> response = await MakeWrappedRequestAsync<T>(client, request).ConfigureAwait(false);
            if (response.Entity == null)
            {
                return default;
            }
            else
            {
                return response.Entity;
            }
        }

        internal static async Task<EntityResponse<T>> MakeWrappedRequestAsync<T>(IRestClient client, IRestRequest request)
        {
            //Console.WriteLine("Making Request..: " + client.BaseUrl + request.Resource);
            //Task<IRestResponse<T>> requestTask = client.ExecuteAsync<T>(request);

            //TODO: confirm works - renamed from executetaskasync
            Task<IRestResponse<T>> requestTask = client.ExecuteAsync<T>(request);
            await requestTask.ConfigureAwait(false);
            //Console.WriteLine("Done Request: " + client.BaseUrl + request.Resource);
            if (requestTask.Exception != null)
            {
                Console.WriteLine(requestTask.Exception.Message);
                throw requestTask.Exception;
            }
            else
            {
                IRestResponse response = requestTask.Result;
                //Console.WriteLine("-----");
                //Console.WriteLine(client.BaseUrl);
                //Console.WriteLine(request.Resource);
                //Console.WriteLine(request.Method);
                //Console.WriteLine(response.StatusCode);
                //Console.WriteLine(response.Content);
                //Console.WriteLine("-----");
                try
                {
                    if (response.Content is T s)
                    {
                        return new EntityResponse<T>(s, response.StatusCode);
                    }
                    //Console.WriteLine("Deserializing response...");
                    T t = JsonConvert.DeserializeObject<T>(response.Content);
                    //Console.WriteLine("Deserialized: " + t);

                    return new EntityResponse<T>(t, response.StatusCode);

                }
                catch (Exception e)
                {
                    Console.WriteLine(response.Content);
                    Console.WriteLine(e.Message);
                    return default;
                }
            }

        }
        internal static async Task<List<T>> GetAllAsync<T>(IRestRequest request, IPushUserRequestService pushRequestService) where T : IBaseUserEntity
        {
            Page<T> page = await pushRequestService.MakeRequestAsync<Page<T>>(request).ConfigureAwait(false);
            return await GetAllAsync(page, pushRequestService).ConfigureAwait(false);
        }
        internal static async Task<List<T>> GetAllAsync<T>(Page<T> page, IPushUserRequestService pushRequestService) where T : IBaseUserEntity
        {
            List<T> entities = new List<T>();

            if (page.Entities.Count > 0)
            {
                entities.AddRange(page.Entities);
            }


            while (!string.IsNullOrEmpty(page.Links.Next))
            {
                page = await pushRequestService.MakeUrlRequestAsync<Page<T>>(page.Links.Next).ConfigureAwait(false);

                entities.AddRange(page.Entities);
            }

            return entities;
        }
    }
}
