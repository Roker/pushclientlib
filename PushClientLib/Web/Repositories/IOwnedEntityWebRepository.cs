﻿using PushClientLib.Web.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Repositories
{
    public interface IOwnedEntityWebRepository<T> : IAsyncRepository<T> where T : BaseUserEntity
    {
        Task<List<T>> GetUpdatedSinceAsync(DateTime since);
    }
}