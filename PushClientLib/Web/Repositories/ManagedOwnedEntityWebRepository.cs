﻿using PushClientLib.KeyValueStorage;
using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Repositories
{
    //queues requests
    //Uses IKeyValueStorage to save uncompleted requests over sessions
    //decorator
    public class ManagedOwnedEntityWebRepository<T> : IOwnedEntityWebRepository<T> where T : BaseUserEntity
    {
        private const string BASE_TASK_LIST_KEY = "TASK_LIST";

        private readonly string _taskListKey;

        private Dictionary<string, OwnedEntityModRequest<T>> _requests
            = new Dictionary<string, OwnedEntityModRequest<T>>();

        private readonly Queue<string> _webTaskQueue = new Queue<string>();

        private readonly IKeyValueStorage _keyValueStorage;

        private Task _requestTask;

        private readonly object _requestsLock = new object();

        private readonly IOwnedEntityWebRepository<T> _webRepo;

        public ManagedOwnedEntityWebRepository(IKeyValueStorage keyValueStorage,
            OwnedEntityWebRepository<T> webRepo)
        {
            _webRepo = webRepo;
            _keyValueStorage = keyValueStorage;

            _taskListKey = BASE_TASK_LIST_KEY + "_" + nameof(T);
            LoadWebTasks();

            StartRequestTask();
        }

        private void StartRequestTask()
        {
            if (_requestTask == null || _requestTask.Status != TaskStatus.Running)
            {
                _requestTask = Task.Run(MakeRequests);
            }
        }

        public Task<T> AddAsync(T entity)
        {
            return QueueAndAwaitRequest(entity, ModRequestType.ADD);
        }

        public Task<T> DeleteAsync(T entity)
        {
            return QueueAndAwaitRequest(entity, ModRequestType.DELETE);
        }

        public Task<T> UpdateAsync(T entity)
        {
            return QueueAndAwaitRequest(entity, ModRequestType.UPDATE);
        }

        public async Task<T> GetAsync(string entityUuid)
        {
            T entity = await _webRepo.GetAsync(entityUuid).ConfigureAwait(false);

            return GetUpToDateEntity(entity);
        }

        public async Task<List<T>> GetAllAsync()
        {
            IEnumerable<T> webEntities = await _webRepo.GetAllAsync().ConfigureAwait(false);
            List<T> returnEntities = new List<T>();
            foreach (T entity in webEntities)
            {
                T upToDateEntity = GetUpToDateEntity(entity);
                if (upToDateEntity != null)
                {
                    returnEntities.Add(upToDateEntity);
                }
            }
            return returnEntities;
        }

        public async Task<int> GetTotalAsync()
        {
            int total = await _webRepo.GetTotalAsync().ConfigureAwait(false);
            foreach (OwnedEntityModRequest<T> req in _requests.Values)
            {
                if (req.ModRequestType == ModRequestType.DELETE)
                {
                    total--;
                }
            }
            return total;
        }

        private async Task MakeRequests()
        {
            Console.WriteLine("QueueSize: " + _webTaskQueue.Count);
            while (_webTaskQueue.Count > 0)
            {
                string key;
                OwnedEntityModRequest<T> request;
                lock (_requestsLock)
                {

                    key = _webTaskQueue.Peek();
                    request = _requests[key];
                }

                Console.WriteLine("making request...");
                try
                {

                    await request.MakeRequest(_webRepo);
                }
                catch (Exception e)
                {
                    Console.WriteLine("MakeRequests EXCEPTION");

                    Console.WriteLine(e.Message);
                    _webTaskQueue.Enqueue(_webTaskQueue.Dequeue());
                    throw e;
                }

                Console.WriteLine("done request");


                lock (_requestsLock)
                {
                    _requests.Remove(key);
                    _webTaskQueue.Dequeue();
                }
            }
        }
        //queues request task and returns result when completed
        private async Task<T> QueueAndAwaitRequest(T entity, ModRequestType taskType)
        {
            Console.WriteLine("Queuing Task");
            OwnedEntityModRequest<T> ownedEntityModRequest;

            lock (_requestsLock)
            {
                //if request for this entity already exists, update it
                try
                {
                    Console.WriteLine("try request update");
                    ownedEntityModRequest = _requests[entity.Uuid];
                    ownedEntityModRequest.UpdateTask(entity, taskType);
                    Console.WriteLine(taskType + "Request updated");
                }
                catch
                {
                    Console.WriteLine("try request add");
                    _webTaskQueue.Enqueue(entity.Uuid);
                    ownedEntityModRequest = new OwnedEntityModRequest<T>(/*this, */taskType, entity);
                    Console.WriteLine(taskType + "Request Created");
                }

                _requests[entity.Uuid] = ownedEntityModRequest;
                SaveWebTasks();
                StartRequestTask();
            }

            //await request
            try
            {
                return await ownedEntityModRequest.TaskProxy;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //returns updated entity if awaiting update request
        //returns null if awaiting delete request
        //otherwise returns entity provided
        private T GetUpToDateEntity(T entity)
        {
            if (_requests.ContainsKey(entity.Uuid))
            {
                OwnedEntityModRequest<T> req = _requests[entity.Uuid];
                if (req.ModRequestType == ModRequestType.DELETE) return null;
                if (req.Entity.Updated > entity.Updated)
                {
                    return req.Entity;
                }
            }
            return entity;
        }

        private void SaveWebTasks()
        {
            lock (_requestsLock)
            {
                _keyValueStorage.Set(_taskListKey, _requests);
            }
        }

        private void LoadWebTasks()
        {
            lock (_requestsLock)
            {
                _requests = _keyValueStorage.Get<Dictionary<string, OwnedEntityModRequest<T>>>(_taskListKey);
                if (_requests == null)
                {
                    _requests = new Dictionary<string, OwnedEntityModRequest<T>>();
                    return;
                }
                foreach (string key in _requests.Keys)
                {
                    _webTaskQueue.Enqueue(key);
                }
            }
        }

        public Task<List<T>> GetUpdatedSinceAsync(DateTime since)
        {
            return _webRepo.GetUpdatedSinceAsync(since);
        }

        public Task<List<T>> GetRangeAsync(int offset = 0, int limit = 20)
        {
            return _webRepo.GetRangeAsync(offset, limit);
        }
    }
}
