﻿using System.Collections.Generic;

namespace PushClientLib.Web.Repositories
{
    public interface IRepository<T>
    {
        T Add(T t);
        int GetTotal();

        List<T> GetAll();
        List<T> GetRange(int offset = 0, int limit = 20);
        //List<T> GetRange<U>(int offset = 0, int limit = 20, Expression<Func<T, U>> orderExp = null);
        T Get(string uuid);
        T Delete(T t);
        T Update(T t);
        //event EventHandler RepositoryChanged;
    }
}
