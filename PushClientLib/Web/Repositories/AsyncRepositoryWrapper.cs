﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Repositories
{
    public class AsyncRepositoryWrapper<T> : IAsyncRepository<T>
    {
        private readonly IRepository<T> _syncRepo;

        public AsyncRepositoryWrapper(IRepository<T> syncRepo)
        {
            _syncRepo = syncRepo;
        }

        //public event EventHandler RepositoryChanged {
        //    add {
        //        _syncRepo.RepositoryChanged += value;
        //    }
        //    remove {
        //        _syncRepo.RepositoryChanged -= value;
        //    }
        //}

        public Task<T> AddAsync(T t)
        {
            return Task.Run(() => _syncRepo.Add(t));
        }

        public Task<T> DeleteAsync(T t)
        {
            return Task.Run(() => _syncRepo.Delete(t));
        }

        public Task<List<T>> GetAllAsync()
        {
            return Task.Run(() => _syncRepo.GetAll());
        }

        public Task<T> GetAsync(string pk)
        {
            return Task.Run(() => _syncRepo.Get(pk));
        }

        public Task<List<T>> GetRangeAsync(int offset = 0, int limit = 20)
        {
            return Task.Run(() => _syncRepo.GetRange(offset, limit));
        }

        public Task<int> GetTotalAsync()
        {
            return Task.Run(() => _syncRepo.GetTotal());
        }

        public Task<T> UpdateAsync(T t)
        {
            return Task.Run(() => _syncRepo.Update(t));
        }
    }
}
