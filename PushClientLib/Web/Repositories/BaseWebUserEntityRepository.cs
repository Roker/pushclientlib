﻿//using PushClientLib.Models;
//using PushClientLib.Repositories;
//using PushClientLib.Web.Requests.Builders;
//using PushClientLib.Web.Rest.Requests;
//using PushClientLib.Web.Services;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace PushClientLib.Web.Repositories
//{
//    public class BaseWebUserEntityRepository<T> : IBaseUserEntityRepository<T> where T : IBaseUserEntity
//    {
//        private readonly IPushUserRequestService _userRequestService;

//        private readonly IUserEntityRequestBuilder<T> _requestBuilder;

//        private string UserUuid => _userRequestService.UserUuid;

//        public BaseWebUserEntityRepository(
//            IPushUserRequestService userRequestService)
//        {
//            _userRequestService = userRequestService;
//            _requestBuilder = UserEntityRequestBuilderFactory.NewUserEntityRequestBuilder<T>();
//        }
//        public virtual Task<T> AddAsync(T t)
//        {
//            return _userRequestService.MakeRequestAsync<T>(_requestBuilder.AddRequest(UserUuid, t));
//        }

//        public virtual Task<T> UpdateAsync(T t)
//        {
//            return _userRequestService.MakeRequestAsync<T>(_requestBuilder.UpdateRequest(UserUuid, t));
//        }

//        public virtual Task<T> DeleteAsync(T t)
//        {
//            return _userRequestService.MakeRequestAsync<T>(_requestBuilder.DeleteRequest(UserUuid, t));
//        }

//        public virtual Task<List<T>> GetAllAsync()
//        {
//            return RestRequestsUtil.GetAllAsync<T>(_requestBuilder.GetAllRequest(UserUuid), _userRequestService);
//        }

//        public virtual Task<T> GetAsync(string entityUuid)
//        {
//            return _userRequestService.MakeRequestAsync<T>(_requestBuilder.GetRequest(UserUuid, entityUuid));
//        }

//        public virtual Task<long> GetTotalAsync()
//        {
//            return _userRequestService.MakeRequestAsync<long>(_requestBuilder.GetTotalRequest(UserUuid));
//        }
//    }
//}
