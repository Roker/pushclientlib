﻿using PushClientLib.Repositories;
using PushClientLib.Web.Models;
using PushClientLib.Web.Requests;
using PushClientLib.Web.Requests.Builders;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Repositories
{
    internal class WebUserEntityRepository<T> : IUserEntityRepository<T> where T : BaseUserEntity
    {
        private readonly IPushRequestHandler _requestHandler;

        protected readonly IUserEntityRequestBuilder<T> _requestBuilder;

        internal WebUserEntityRepository(
            IPushRequestHandler requestHandler)
        {
            _requestBuilder = UserEntityRequestBuilderFactory.NewUserEntityRequestBuilder<T>();
            _requestHandler = requestHandler;
        }

        public virtual Task<T> AddAsync(T t)
        {
            return _requestHandler.ExecuteAsync<T>(_requestBuilder.UpdateRequest(t));
        }

        public virtual Task<T> UpdateAsync(T t)
        {
            return _requestHandler.ExecuteAsync<T>(_requestBuilder.UpdateRequest(t));
        }

        public virtual Task DeleteAsync(T t)
        {
            return _requestHandler.ExecuteAsync(_requestBuilder.DeleteRequest(t));
        }

        public virtual Task<List<T>> GetAllAsync()
        {
            return PushRequestUtil.GetAllPages<T>(_requestBuilder.GetAllRequest(), _requestHandler);
        }

        public virtual Task<T> GetAsync(string entityUuid)
        {
            return _requestHandler.ExecuteAsync<T>(_requestBuilder.GetRequest(entityUuid));
        }


        public virtual Task<long> GetTotalAsync()
        {
            return _requestHandler.ExecuteAsync<long>(_requestBuilder.GetTotalRequest());
        }
    }
}
