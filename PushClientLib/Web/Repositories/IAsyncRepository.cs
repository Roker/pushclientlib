﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Repositories
{
    public interface IAsyncRepository<T>
    {
        Task<T> AddAsync(T t);
        Task<int> GetTotalAsync();
        Task<List<T>> GetAllAsync();
        Task<List<T>> GetRangeAsync(int offset = 0, int limit = 20);
        Task<T> GetAsync(string pk);
        Task<T> DeleteAsync(T t);
        Task<T> UpdateAsync(T t);
    }

    public interface IAsyncObservableRepository<T> : IAsyncRepository<T>, INotifyRepositoryChanged { }

    public interface INotifyRepositoryChanged
    {
        event EventHandler RepositoryChanged;
    }

}

