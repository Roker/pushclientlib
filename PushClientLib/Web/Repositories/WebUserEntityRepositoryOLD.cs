﻿//using PushClientLib.Repositories;
//using PushClientLib.Web.Models;
//using PushClientLib.Web.Requests.Builders;
//using PushClientLib.Web.Services;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace PushClientLib.Web.Repositories
//{
//    public class WebUserEntityRepository<T> : BaseWebUserEntityRepository<T>, IAsyncUserEntityRepository<T> where T : BaseUserEntity
//    {
//        private readonly IPushUserRequestService _userRequestService;

//        private readonly IUserEntityRequestBuilder<T> _requestBuilder;

//        public event EventHandler<T> EntityAdded;
//        public event EventHandler<T> EntityDeleted;
//        public event EventHandler<T> EntityUpdated;


//        public WebUserEntityRepository(
//            IPushUserRequestService userRequestService) : base(userRequestService)
//        {
//            _userRequestService = userRequestService;
//            _requestBuilder = UserEntityRequestBuilderFactory.NewUserEntityRequestBuilder<T>();
//        }

//        public Task<List<T>> GetRangeAsync(int offset = 0, int limit = 20)
//        {
//            throw new NotImplementedException();
//        }

//        public override Task<T> AddAsync(T t)
//        {
//            t.Created = DateTime.UtcNow;
//            t.Updated = DateTime.UtcNow;
//            return base.AddAsync(t);
//        }

//        public override Task<T> UpdateAsync(T t)
//        {
//            t.Updated = DateTime.UtcNow;
//            return base.UpdateAsync(t);
//        }
//    }
//}
