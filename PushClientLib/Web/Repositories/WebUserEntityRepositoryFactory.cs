﻿using PushClientLib.Models;
using PushClientLib.Repositories;
using PushClientLib.Web.Models;
using PushClientLib.Web.Repositories;
using PushClientLib.Web.Requests;
using PushClientLib.Web.Services;
using System;

namespace PushClientLib.Web.Repositories
{
    internal static class WebUserEntityRepositoryFactory
    {
        //reg

        internal static IUserEntityRepository<T> NewWebUserEntityRepository<T>
            (IPushRequestHandler handler) where T : IBaseUserEntity
        {
            if (typeof(T) == typeof(IAlbumList))
            {
                return (IUserEntityRepository<T>)
                    new BaseMappedUserEntityRepository<IAlbumList, AlbumList>(
                        new WebUserEntityRepository<AlbumList>(handler));
            }
            throw new InvalidOperationException("type not found");
        }

        //caching


        internal static IAsyncUserEntityCachingRepository<T> NewWebUserEntityCachingRepository<T>
            (IPushRequestHandler handler) where T : IBaseUserEntity
        {
            if (typeof(T) == typeof(IAlbumList))
            {
                return (IAsyncUserEntityCachingRepository<T>)
                    new MappedUserEntityCachingRepository<IAlbumList, AlbumList>(
                        new WebUserEntityCachingRepository<AlbumList>(handler));
            }
            throw new InvalidOperationException("type not found");
        }

    }
}
