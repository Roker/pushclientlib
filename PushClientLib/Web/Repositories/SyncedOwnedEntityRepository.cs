﻿using PushClientLib.KeyValueStorage;
using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Web.Repositories
{
    public class SyncedOwnedEntityRepository<T> : IAsyncObservableRepository<T> where T : BaseUserEntity
    {
        private readonly IAsyncRepository<T> _localRepository;
        private readonly IOwnedEntityWebRepository<T> _webRepository;

        private const string LAST_WEB_SYNCED_KEY = "LAST_WEB_SYNCED";

        private readonly IKeyValueStorage _keyValueStorage;

        public event EventHandler RepositoryChanged;

        //track web synced ids so that we do not need to raise RepositoryChanged
        //after updating the DB with WebSynced=true
        //a bit of a band-aid solution
        private readonly HashSet<string> _webSyncedIds = new HashSet<string>();

        public SyncedOwnedEntityRepository(
            IAsyncRepository<T> localRepository,
            IOwnedEntityWebRepository<T> webRepository,
            IKeyValueStorage keyValueStorage)
        {
            _localRepository = localRepository;
            _webRepository = webRepository;
            _keyValueStorage = keyValueStorage;

            Task.Run(SyncAsync);
        }

        public Task<T> GetAsync(string pk)
        {
            try
            {
                return _localRepository.GetAsync(pk);
            }
            catch
            {
                return _webRepository.GetAsync(pk);
            }
        }

        public Task<int> GetTotalAsync()
        {
            return _localRepository.GetTotalAsync();
        }

        public Task<List<T>> GetAllAsync()
        {
            return _localRepository.GetAllAsync();
        }
        public Task<List<T>> GetRangeAsync(int offset, int limit)
        {
            return _localRepository.GetRangeAsync(offset, limit);
        }
        public async Task<T> AddAsync(T entity)
        {
            entity = await _localRepository.AddAsync(entity);
            RepositoryChanged?.Invoke(this, EventArgs.Empty);

            StartWebTask(() => WebAddAsync(entity));

            return entity;
        }


        public async Task<T> UpdateAsync(T entity)
        {
            entity = await _localRepository.UpdateAsync(entity);
            RepositoryChanged?.Invoke(this, EventArgs.Empty);

            if (IsWebSynced(entity))
            {
                StartWebTask(() => _webRepository.UpdateAsync(entity));
            }
            else
            {
                StartWebTask(() => WebAddAsync(entity));
            }

            return entity;
        }

        public async Task<T> DeleteAsync(T entity)
        {
            entity = await _localRepository.DeleteAsync(entity);
            RepositoryChanged?.Invoke(this, EventArgs.Empty);

            if (IsWebSynced(entity))
            {
                StartWebTask(() => _webRepository.DeleteAsync(entity));
            }

            return entity;
        }

        private bool IsWebSynced(T entity)
        {
            return entity.WebSynced || _webSyncedIds.Contains(entity.Uuid);
        }
        private void StartWebTask(Func<Task> p)
        {
            Console.WriteLine("QUEUING WEB TASK");
            Task.Run(() => DoWebTask(p));
        }

        private async Task DoWebTask(Func<Task> p)
        {
            Console.WriteLine("Do web task");
            await p.Invoke().ConfigureAwait(false);
            //Console.WriteLine("Do web task middle");
            //await SyncAsync().ConfigureAwait(false);
            Console.WriteLine("Do web task done");
        }
        private async Task SyncAsync()
        {
            bool repoChanged = false;
            DateTime? lastSynced = _keyValueStorage.Get<DateTime?>(LAST_WEB_SYNCED_KEY, null);
            DateTime newUpdatedTime = DateTime.UtcNow;

            if (lastSynced != null)
            {
                IEnumerable<T> newEntities = await _webRepository.GetUpdatedSinceAsync(lastSynced.Value).ConfigureAwait(false);
                if (newEntities != null)
                {
                    foreach (T entity in newEntities)
                    {
                        if (entity.Created > lastSynced)
                        {
                            repoChanged = true;
                            entity.WebSynced = true;
                            await _localRepository.AddAsync(entity).ConfigureAwait(false);
                        }
                        else
                        {
                            repoChanged = true;
                            entity.WebSynced = true;
                            await _localRepository.UpdateAsync(entity).ConfigureAwait(false);
                        }
                    }
                }
            }
            else
            {
                List<T> newEntities = await _webRepository.GetAllAsync().ConfigureAwait(false);

                foreach (T entity in newEntities)
                {
                    entity.WebSynced = true;
                    repoChanged = true;
                    await _localRepository.AddAsync(entity).ConfigureAwait(false);
                }
            }
            _keyValueStorage.Set(LAST_WEB_SYNCED_KEY, newUpdatedTime);
            if (repoChanged)
            {
                RepositoryChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private async Task WebAddAsync(T entity)
        {
            T webList = await _webRepository.AddAsync(entity).ConfigureAwait(false);

            _webSyncedIds.Add(webList.Uuid);
            webList.WebSynced = true;
            await _localRepository.UpdateAsync(webList).ConfigureAwait(false);
        }


    }
}
