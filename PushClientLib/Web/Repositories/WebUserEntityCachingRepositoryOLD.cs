﻿//using PushClientLib.Repositories;
//using PushClientLib.Web.Models;
//using PushClientLib.Web.Requests.Builders;
//using PushClientLib.Web.Rest.Requests;
//using PushClientLib.Web.Services;
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace PushClientLib.Web.Repositories
//{
//    internal class WebUserEntityCachingRepository<T> : WebUserEntityRepository<T>, IAsyncUserEntityCachingRepository<T> where T : BaseUserEntity
//    {
//        private readonly IPushUserRequestService _pushUserRequestService;
//        private readonly IUserEntityRequestBuilder<T> _requestBuilder;

//        private string UserUuid => _pushUserRequestService.UserUuid;

//        public WebUserEntityCachingRepository(
//            IPushUserRequestService pushRequestService) : base(pushRequestService)
//        {
//            _pushUserRequestService = pushRequestService;
//            _requestBuilder = UserEntityRequestBuilderFactory.NewUserEntityRequestBuilder<T>();
//        }

//        public Task<List<T>> GetAddedSinceAsync(DateTime since)
//        {
//            return RestRequestsUtil.GetAllAsync<T>(_requestBuilder.GetAddedSinceRequest(UserUuid, since), _pushUserRequestService);
//        }
//        public Task<List<T>> GetUpdatedSinceAsync(DateTime since)
//        {
//            return RestRequestsUtil.GetAllAsync<T>(_requestBuilder.GetUpdatedSinceRequest(UserUuid, since), _pushUserRequestService);
//        }
//        public Task<List<T>> GetDeletedSinceAsync(DateTime since)
//        {
//            return RestRequestsUtil.GetAllAsync<T>(_requestBuilder.GetDeletedSinceRequest(UserUuid, since), _pushUserRequestService);
//        }
//    }
//}
