﻿using PushClientLib.Repositories;
using PushClientLib.Web.Models;
using PushClientLib.Web.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PushClientLib.Web.Repositories
{
    internal class WebUserEntityCachingRepository<T> : WebUserEntityRepository<T>, IAsyncUserEntityCachingRepository<T> where T : BaseUserEntity
    {
        private readonly IPushRequestHandler _requestHandler;

        internal WebUserEntityCachingRepository(IPushRequestHandler requestHandler) : base(requestHandler)
        {
            _requestHandler = requestHandler;
        }

        public Task<List<T>> GetUpdatedSinceAsync(DateTime since)
        {
            return PushRequestUtil.GetAllPages(_requestBuilder.GetUpdatedSinceRequest(since), _requestHandler);
        }

        public Task<List<T>> GetAddedSinceAsync(DateTime since)
        {
            return PushRequestUtil.GetAllPages(_requestBuilder.GetAddedSinceRequest(since), _requestHandler);
        }

        public Task<List<T>> GetDeletedSinceAsync(DateTime since)
        {
            return PushRequestUtil.GetAllPages(_requestBuilder.GetDeletedSinceRequest(since), _requestHandler);
        }
    }
}
