﻿using PushClientLib.Models;
using System;

namespace PushClientLib.Repositories
{
    internal interface IAsyncUserEntityRepository<T> : IUserEntityRepository<T> where T : IBaseUserEntity
    {
        event EventHandler<T> EntityAdded;
        event EventHandler<T> EntityDeleted;
        event EventHandler<T> EntityUpdated;

        //Task<List<T>> GetRangeAsync(int offset = 0, int limit = 20);

    }
}
