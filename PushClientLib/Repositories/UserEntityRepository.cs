﻿using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Repositories
{
    internal class UserEntityRepository<T> : IAsyncUserEntityRepository<T>
        where T : IBaseUserEntity
    {
        private IUserEntityRepository<T> _baseRepo;

        internal UserEntityRepository(IUserEntityRepository<T> baseRepo)
        {
            _baseRepo = baseRepo;
        }

        public event EventHandler<T> EntityAdded;
        public event EventHandler<T> EntityDeleted;
        public event EventHandler<T> EntityUpdated;


        public async Task<T> AddAsync(T t)
        {
            t.Created = DateTime.UtcNow;
            t.Updated = DateTime.UtcNow;
            t.Uuid = Guid.NewGuid().ToString();

            t = await _baseRepo.AddAsync(t).ConfigureAwait(false);
            EntityAdded?.Invoke(this, t);
            return t;
        }
        public async Task<T> UpdateAsync(T t)
        {
            t.Updated = DateTime.UtcNow;
            t = await _baseRepo.UpdateAsync(t).ConfigureAwait(false);
            EntityUpdated?.Invoke(this, t);

            return t;
        }

        public async Task DeleteAsync(T t)
        {
            t.Updated = DateTime.UtcNow;
            t.Deleted = true;
            t = await _baseRepo.UpdateAsync(t).ConfigureAwait(false);
            EntityDeleted?.Invoke(this, t);
            //return t;
        }

        public Task<List<T>> GetAllAsync()
        {
            return _baseRepo.GetAllAsync();
        }

        public Task<T> GetAsync(string entityUuid)
        {
            return _baseRepo.GetAsync(entityUuid);
        }

        public Task<long> GetTotalAsync()
        {
            return _baseRepo.GetTotalAsync();
        }
    }
}
