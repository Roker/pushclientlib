﻿using PushClientLib.Models;
using PushClientLib.Web.Requests.Paging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Repositories
{
    internal interface IUserEntityRepository<T> where T : IBaseUserEntity
    {
        Task<T> GetAsync(string entityUuid);
        Task<T> AddAsync(T t);
        Task<T> UpdateAsync(T t);
        Task DeleteAsync(T t);
        Task<List<T>> GetAllAsync();
        Task<long> GetTotalAsync();


        //Task<List<T>> GetUpdatedSince(DateTime since);
        //Task<List<T>> GetAddedSince(DateTime since);
        //Task<List<T>> GetDeletedSince(DateTime since);
    }
}