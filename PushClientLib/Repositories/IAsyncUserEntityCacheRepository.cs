﻿//using PushClientLib.Models;
//using PushClientLib.Repositories;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading.Tasks;

//namespace PushClientLib.Repositories
//{
//    public interface IAsyncUserEntityCacheRepository<T> : IAsyncUserEntityRepository<T> where T : IBaseUserEntity
//    {
//        Task CacheAsync(IEnumerable<T> ts);
//        Task UncacheAsync(IEnumerable<T> ts);
//    }
//}
