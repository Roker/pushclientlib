﻿using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Repositories
{
    internal class MappedUserEntityCachingRepository<T, TDB>
        : BaseMappedUserEntityRepository<T, TDB>, IAsyncUserEntityCachingRepository<T>
        where T : IBaseUserEntity
        where TDB : T, new()
    {

        private readonly IAsyncUserEntityCachingRepository<TDB> _baseRepo;

        public MappedUserEntityCachingRepository(IAsyncUserEntityCachingRepository<TDB> baseRepo) : base(baseRepo)
        {
            _baseRepo = baseRepo;
        }

        public Task<List<T>> GetAddedSinceAsync(DateTime since)
        {
            return CastResultsAsync(_baseRepo.GetAddedSinceAsync(since));
        }
        public Task<List<T>> GetUpdatedSinceAsync(DateTime since)
        {
            return CastResultsAsync(_baseRepo.GetUpdatedSinceAsync(since));
        }

        public Task<List<T>> GetDeletedSinceAsync(DateTime since)
        {
            return CastResultsAsync(_baseRepo.GetDeletedSinceAsync(since));
        }

    }
}
