﻿using AutoMapper;
using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushClientLib.Repositories
{
    internal class BaseMappedUserEntityRepository<T, TDB> : IUserEntityRepository<T>
        where T : IBaseUserEntity
        where TDB : T, new()
    {
        private readonly IUserEntityRepository<TDB> _baseRepo;

        public event EventHandler<T> EntityAdded;
        public event EventHandler<T> EntityDeleted;
        public event EventHandler<T> EntityUpdated;

        protected IMapper Mapper => ModelMapper.Mapper;

        internal BaseMappedUserEntityRepository(IUserEntityRepository<TDB> baseRepo)
        {
            _baseRepo = baseRepo;
        }

        public virtual async Task<T> AddAsync(T t)
        {
            await _baseRepo.AddAsync(Mapper.Map<TDB>(t)).ConfigureAwait(false);
            EntityAdded?.Invoke(this, t);
            return t;
        }
        public async Task<T> UpdateAsync(T t)
        {
            await _baseRepo.UpdateAsync(Mapper.Map<TDB>(t)).ConfigureAwait(false);
            EntityUpdated?.Invoke(this, t);
            return t;
        }

        public async Task DeleteAsync(T t)
        {
            await _baseRepo.DeleteAsync(Mapper.Map<TDB>(t)).ConfigureAwait(false);
            EntityDeleted?.Invoke(this, t);
            //return t;
        }



        public virtual async Task<T> GetAsync(string uuid)
        {
            return await _baseRepo.GetAsync(uuid).ConfigureAwait(false);
        }

        public Task<long> GetTotalAsync()
        {
            return _baseRepo.GetTotalAsync();
        }

        public Task<List<T>> GetAllAsync()
        {
            return CastResultsAsync(_baseRepo.GetAllAsync());
        }

        protected async Task<List<T>> CastResultsAsync(Task<List<TDB>> task)
        {
            return (await task.ConfigureAwait(false)).Cast<T>().ToList();
        }


    }
}
