﻿//using PushClientLib.Models;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Threading.Tasks;

//namespace PushClientLib.Repositories
//{
//    public class UserEntityObservableCollection<T> : ObservableCollection<T> where T : IBaseUserEntity
//    {
//        private IAsyncUserEntityRepository<T> _repo;

//        public IAsyncUserEntityRepository<T> Repo {
//            get => _repo;
//            set {
//                if (_repo != null)
//                {
//                    _repo.EntityAdded -= OnEntityAdded;
//                    _repo.EntityDeleted -= OnEntityDeleted;
//                    _repo.EntityUpdated -= OnEntityUpdated;
//                }
//                _repo = value;
//                if (_repo != null)
//                {
//                    _repo.EntityAdded += OnEntityAdded;
//                    _repo.EntityDeleted += OnEntityDeleted;
//                    _repo.EntityUpdated += OnEntityUpdated;
//                }
//            }
//        }

//        private void OnEntityUpdated(object sender, T t)
//        {
//            try
//            {
//                T collectionT = this.First(x => x.Uuid == t.Uuid);
//                int index = this.IndexOf(collectionT);
//                this.RemoveAt(index);
//                this.Insert(index, t);
//            }
//            catch (InvalidOperationException) { }
//        }

//        private void OnEntityDeleted(object sender, T t)
//        {
//            TryDelete(t);
//        }

//        private void TryDelete(T t)
//        {
//            try
//            {
//                Remove(this.First(x => x.Uuid == t.Uuid));
//            }
//            catch (InvalidOperationException) { }
//        }

//        private void OnEntityAdded(object sender, T t)
//        {
//            TryAdd(t);
//        }

//        private void TryAdd(T t)
//        {
//            if (this.Where(x => x.Uuid == t.Uuid).Count() == 0)
//            {
//                Add(t);
//            }
//        }

//        public UserEntityObservableCollection(IAsyncUserEntityRepository<T> repo)
//        {
//            Repo = repo;
//        }

//        public async Task InitCollectionAsync()
//        {
//            List<T> ts = await Repo.GetAllAsync().ConfigureAwait(false);
//            foreach (T t in ts)
//            {
//                Add(t);
//            }
//        }

//    }
//}
