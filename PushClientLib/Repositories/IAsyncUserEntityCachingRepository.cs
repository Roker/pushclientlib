﻿using PushClientLib.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.Repositories
{
    internal interface IAsyncUserEntityCachingRepository<T> : IUserEntityRepository<T> where T : IBaseUserEntity
    {
        Task<List<T>> GetAddedSinceAsync(DateTime since);
        Task<List<T>> GetUpdatedSinceAsync(DateTime since);
        Task<List<T>> GetDeletedSinceAsync(DateTime since);
    }
}
