﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace PushClientLib.CollectionLoading
{
    public interface IMutableCollectionLoaderController : ICollectionLoaderController
    {
        event NotifyCollectionChangedEventHandler CollectionChanged;

        void Add(object item);
        void Insert(int index, object item);
        void Remove(object item);
        void Move(int oldIndex, int newIndex);
    }
    public interface IMutableCollectionLoaderController<T> : ICollectionLoaderController<T>, IMutableCollectionLoaderController
    {
        //IMutableCollectionLoader<T> CollectionLoader { get; set; }
    }
}
