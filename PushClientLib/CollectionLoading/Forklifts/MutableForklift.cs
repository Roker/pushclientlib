﻿//using PushClientLib.CollectionLoading.Forklifts;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Text;

//namespace PushClientLib.CollectionLoading
//{
//    public class MutableForklift : BaseForkliftDecorator
//    {

//        public SortedSet<int> RemovedIndexes { get; private set; } = new SortedSet<int>();
//        public SortedSet<int> InsertedIndexes { get; private set; } = new SortedSet<int>();

//        public MutableForklift(IForklift baseForklift) : base(baseForklift)
//        {
//        }

//        public override void AddOrReplaceItem(int index, object t)
//        {
//            base.AddOrReplaceItem(TrueIndex(index), t);
//        }


//        public int TrueIndex(int rangeLoaderIndex)
//        {
//            int adjustedIndex = rangeLoaderIndex;
//            foreach (int i in RemovedIndexes)
//            {
//                if (i > rangeLoaderIndex)
//                {
//                    break;
//                }
//                adjustedIndex--;
//            }
//            foreach (int i in InsertedIndexes)
//            {
//                if (i > rangeLoaderIndex)
//                {
//                    break;
//                }
//                adjustedIndex++;
//            }
//            return adjustedIndex;
//        }

//        public int RangeLoaderIndex(int trueIndex)
//        {
//            int rangeLoaderIndex = trueIndex;
//            foreach (int i in RemovedIndexes)
//            {
//                if (i > trueIndex)
//                {
//                    break;
//                }
//                rangeLoaderIndex++;
//            }
//            foreach (int i in InsertedIndexes)
//            {
//                if (i > trueIndex)
//                {
//                    break;
//                }
//                trueIndex--;
//            }
//            return rangeLoaderIndex;
//        }
//    }
//}
