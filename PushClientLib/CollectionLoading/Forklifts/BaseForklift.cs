﻿using System.Collections.ObjectModel;

namespace PushClientLib.CollectionLoading
{
    public class BaseForklift : IForklift
    {
        public event ItemLoadedEventHandler ItemLoaded;

        public void AddOrReplaceItem(int index, object t, ObservableCollection<object> observableCollection)
        {
            lock (observableCollection)
            {
                if (observableCollection.Count == index)
                {
                    observableCollection.Add(t);
                }
                else
                {
                    observableCollection[index] = t;
                }
                ItemLoaded?.Invoke(this, new ItemLoadedEventArgs(index, observableCollection[index]));
            }
        }
    }
}
