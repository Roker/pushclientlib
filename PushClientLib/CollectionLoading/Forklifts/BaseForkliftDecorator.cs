﻿using System.Collections.ObjectModel;

namespace PushClientLib.CollectionLoading.Forklifts
{
    public abstract class BaseForkliftDecorator : IForklift
    {
        private IForklift _baseForklift;


        public event ItemLoadedEventHandler ItemLoaded {
            add {
                _baseForklift.ItemLoaded += value;
            }

            remove {
                _baseForklift.ItemLoaded -= value;
            }
        }

        public virtual void AddOrReplaceItem(int index, object t, ObservableCollection<object> observableCollection)
        {
            _baseForklift.AddOrReplaceItem(index, t, observableCollection);
        }

        protected BaseForkliftDecorator(IForklift baseForklift)
        {
            _baseForklift = baseForklift;
        }
    }
}
