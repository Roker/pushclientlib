﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public class CursorCollectionLoader<T> : BaseCollectionLoadingStrategy<T>, ICursorCollectionLoader<T>
//    {
//        private const int REVERSE_BUFFER_SIZE = 10;
//        private const int BUFFER_SIZE = 40;

//        private IObservableCollectionLoader<T> _collectionLoader;



//        public CursorCollectionLoader(IObservableCollectionLoader<T> collectionLoader) : base(collectionLoader)
//        {
//            _collectionLoader = collectionLoader;
//        }


//        public int FirstVisibleIndex { get; set; }
//        public int VisibleItemCount { get; set; }

//        protected override void StartLoadTask(CancellationToken token)
//        {
//            Task.Run(() => _collectionLoader.InflateToAsync(_total, token));
//            Task.Run(() => LoadTask(token));
//        }

//        private async Task LoadTask(CancellationToken token)
//        {
//            Console.WriteLine("load task");
//            while (!token.IsCancellationRequested)
//            {
//                try
//                {
//                    await LoadNearbyItemsAsync(1/*_scrollDir*/, token).ConfigureAwait(false);
//                    //await InflateToBufferAsync(token).ConfigureAwait(false);
//                    await Task.Delay(500).ConfigureAwait(false);
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine("load excp: " + e.Message);
//                }

//            }
//            Console.WriteLine("load cancelled ");
//        }

//        protected Task LoadNearbyItemsAsync(int scrollDir, CancellationToken token)
//        {
//            List<Task> loadTasks = new List<Task>();
//            loadTasks.Add(LoadVisibleItemsAsync(token));
//            loadTasks.Add(LoadBufferItemsAsync(scrollDir, token));
//            return Task.WhenAll(loadTasks);
//        }


//        private Task LoadVisibleItemsAsync(CancellationToken token)
//        {
//            Console.WriteLine(GetHashCode() + " - FirstVis: " + FirstVisibleIndex);
//            return LoadRangeAsync(FirstVisibleIndex, VisibleItemCount, token);
//        }


//        //load items after lastvisibleindex 
//        //limit based on scroll directions
//        private Task LoadBufferItemsAsync(int scrollDir, CancellationToken token)
//        {
//            int start = FirstVisibleIndex + VisibleItemCount;

//            // ---------------------------------------------------- limit based on scroll direction
//            int limit = scrollDir == 1 ? BUFFER_SIZE : REVERSE_BUFFER_SIZE;
//            return LoadRangeAsync(start, limit, token);
//        }
//    }
//}
