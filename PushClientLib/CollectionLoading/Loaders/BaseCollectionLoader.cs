﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    //handles loading of items + inflating collection
//    public class BaseCollectionLoader<T> : ICollectionLoader<T> where T : class
//    {
//        private const int REPLACE_BATCH_SIZE = 12;
//        private const int INFLATE_RATE = 20;

//        private const int BATCH_DELAY = 50;
//        private ObservableCollection<object> _observableCollection;
//        private IForklift _forklift;
//        private IRangeLoader<T> _rangeLoader;

//        private CancellationTokenSource _initCancellationTokenSource;
//        private Task _initTask;

//        private int _total;

//        public virtual ObservableCollection<object> ObservableCollection {
//            get => _observableCollection;
//            set {
//                _observableCollection = value;
//                Forklift.ObservableCollection = value;
//            }
//        }
//        public IRangeLoader<T> RangeLoader {
//            get => _rangeLoader;
//            set {
//                _rangeLoader = value;
//                StartInit();
//            }
//        }

//        private void StartInit()
//        {
//            _initCancellationTokenSource?.Cancel();
//            _initCancellationTokenSource = new CancellationTokenSource();
//            _initTask = InitAsync(_initCancellationTokenSource.Token);
//        }

//        private async Task InitAsync(CancellationToken token)
//        {
//            int total = await RangeLoader.GetTotalAsync().ConfigureAwait(false);
//            token.ThrowIfCancellationRequested();
//            _total = total;
//        }

//        public virtual IForklift Forklift {
//            get => _forklift;
//            set {
//                if (_forklift != null)
//                {
//                    _forklift.ItemLoaded -= Forklift_ItemLoaded;
//                }
//                _forklift = value;
//                if (_forklift != null)
//                {
//                    _forklift.ItemLoaded += Forklift_ItemLoaded;
//                }
//            }
//        }

//        private void Forklift_ItemLoaded(object sender, ItemLoadedEventArgs e)
//        {
//            Console.WriteLine(GetType() + " item loaded");
//            ItemLoaded?.Invoke(this, e);
//        }

//        public event ItemLoadedEventHandler ItemLoaded;

//        public BaseCollectionLoader()
//        {
//            Forklift = new BaseForklift();
//        }

//        public virtual async Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            await _initTask;
//            start = Math.Max(0, start);
//            limit = Math.Min(_total - start, limit);
//            if (_initTask.Status != TaskStatus.RanToCompletion) return;
//            try
//            {
//                Console.WriteLine("LoadRange{0}-{1}", start, start + limit);
//                //load items
//                Task<List<T>> loadTask = RangeLoader.LoadRange(start, limit);

//                //inflate up to beginning of loaded items
//                Task inflateTask = InflateToAsync(start - 1, token);

//                Task[] tasks = new Task[]
//                {
//                    loadTask,
//                    inflateTask
//                };

//                await Task.WhenAll(tasks).ConfigureAwait(false);

//                List<T> ts = loadTask.Result;
//                if (token.IsCancellationRequested) return;
//                await ReplaceItemsAsync(start, ts.ToArray(), token).ConfigureAwait(false);
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(GetType().Name + " exception {0}", e.Message);
//            }
//        }

//        public async Task InflateToAsync(int targetCount, CancellationToken token)
//        {
//            if (token.IsCancellationRequested) return;
//            while (ObservableCollection.Count < targetCount)
//            {
//                int target = Math.Min(targetCount, ObservableCollection.Count + INFLATE_RATE);
//                InflateTo(target, token);
//                await Task.Delay(BATCH_DELAY).ConfigureAwait(false);
//                if (token.IsCancellationRequested) return;
//            }
//        }

//        private async Task ReplaceItemsAsync(int start, T[] ts, CancellationToken token)
//        {
//            int batchIndex = 0;
//            for (int i = 0; i < ts.Length; i++)
//            {
//                if (token.IsCancellationRequested) return;
//                Forklift.AddOrReplaceItem(start + i, ts[i]);
//                //OnItemLoaded(start + i, (T)ObservableCollection[start + i]);

//                batchIndex++;
//                if (batchIndex > REPLACE_BATCH_SIZE)
//                {
//                    batchIndex = 0;
//                    await Task.Delay(BATCH_DELAY).ConfigureAwait(false);
//                }
//            }
//        }


//        private void InflateTo(int targetCount, CancellationToken token)
//        {
//            lock (ObservableCollection)
//            {
//                if (token.IsCancellationRequested) return;
//                while (ObservableCollection.Count < targetCount)
//                {
//                    ObservableCollection.Add(null);
//                }
//            }
//        }
//    }
//}
