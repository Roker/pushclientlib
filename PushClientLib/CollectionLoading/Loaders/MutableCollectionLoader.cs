﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Collections.Specialized;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    internal class MutableCollectionLoader<T> : BaseCollectionLoaderDecorator<T>, IMutableCollectionLoader<T>
//    {
//        public event NotifyCollectionChangedEventHandler CollectionChanged;

//        private MutableForklift _forklift;

//        public MutableCollectionLoader(ICollectionLoader<T> collectionLoader) : base(collectionLoader)
//        {
//            Forklift = collectionLoader.Forklift;
//        }

//        public override IForklift Forklift {
//            get => _forklift;
//            set {
//                _forklift = new MutableForklift(value);
//                base.Forklift = _forklift;
//            }
//        }

//        public override Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            int trueStart = _forklift.TrueIndex(start);
//            int trueEnd = _forklift.TrueIndex(start + limit);
//            int trueLimit = trueEnd - trueStart;
//            return base.LoadRangeAsync(trueStart, trueLimit, token);
//        }

//        public virtual void Add(object item)
//        {
//            lock (ObservableCollection)
//            {
//                ObservableCollection.Add(item);
//                CollectionChanged?.Invoke(this,
//                   new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, ObservableCollection.Count - 1));
//            }
//        }


//        public virtual void Insert(int index, object item)
//        {
//            lock (ObservableCollection)
//            {
//                ObservableCollection.Insert(index, item);
//                _forklift.InsertedIndexes.Add(index);

//                CollectionChanged?.Invoke(this,
//                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
//            }
//        }

//        public virtual void Move(int oldIndex, int newIndex)
//        {
//            lock (ObservableCollection)
//            {
//                object item = ObservableCollection[oldIndex];

//                ObservableCollection.Move(oldIndex, newIndex);

//                CollectionChanged?.Invoke(this,
//                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, item, newIndex, oldIndex));
//            }
//        }
//        public virtual void Remove(object item)
//        {
//            lock (ObservableCollection)
//            {
//                int index = ObservableCollection.IndexOf(item);
//                if (index >= 0)
//                {
//                    ObservableCollection.RemoveAt(index);
//                    _forklift.RemovedIndexes.Add(index);

//                    CollectionChanged?.Invoke(this,
//                  new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
//                }
//            }
//        }
//    }
//}

