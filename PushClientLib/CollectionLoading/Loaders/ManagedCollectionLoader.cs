﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public class ManagedCollectionLoader<T> : BaseCollectionLoaderDecorator<T>
//    {
//        private HashSet<int> _loadingIndexes = new HashSet<int>();

//        private List<bool> _loadedStatuses = new List<bool>();

//        public ManagedCollectionLoader(ICollectionLoader<T> collectionLoader) : base(collectionLoader)
//        {
//        }

//        public override ObservableCollection<object> ObservableCollection {
//            get => base.ObservableCollection;
//            set {
//                if(ObservableCollection != null)
//                {
//                    ObservableCollection.CollectionChanged -= ObservableCollection_CollectionChanged;
//                }
//                base.ObservableCollection = value;
//                if (ObservableCollection != null)
//                {
//                    ObservableCollection.CollectionChanged += ObservableCollection_CollectionChanged;
//                }
//            }
//        }


//        public async override Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            GetRangeAvoidingLoadedItems(ref start, ref limit);
//            if (limit == 0) return;

//            Console.WriteLine("LoadRange: {0}-{1}", start, start + limit - 1);
//            SetLoading(start, limit);

//            InflateLoadedStatus(start);

//            await base.LoadRangeAsync(start, limit, token).ConfigureAwait(false);

//            if (!token.IsCancellationRequested)
//            {
//                for(int i = start; i < start + limit; i++)
//                {
//                    SetLoaded(i);
//                }
//            }

//            SetLoading(start, limit, false);
//        }

//        private void ObservableCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
//        {
//            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
//            {
//                _loadedStatuses.Clear();
//            }
//        }
//        private void InflateLoadedStatus(int start)
//        {
//            lock (_loadedStatuses)
//            {
//                while (_loadedStatuses.Count < start)
//                {
//                    _loadedStatuses.Add(false);
//                }
//            }
//        }

//        private void SetLoaded(int index)
//        {
//            lock (_loadedStatuses)
//            {
//                if (_loadedStatuses.Count == index)
//                {
//                    _loadedStatuses.Add(true);
//                }
//                else
//                {
//                    _loadedStatuses[index] = true;
//                }
//            }
//        }
//        private void SetLoading(int start, int limit, bool loading = true)
//        {
//            lock (_loadingIndexes)
//            {
//                for (int i = start; i < start + limit; i++)
//                {
//                    if (loading)
//                    {
//                        _loadingIndexes.Add(i);
//                    }
//                    else
//                    {
//                        _loadingIndexes.Remove(i);
//                    }
//                }
//            }
//        }

//        private void GetRangeAvoidingLoadedItems(ref int start, ref int limit)
//        {
//            start = Math.Max(0, start);

//            lock (_loadedStatuses)
//            {
//                int newStart = -1;
//                int newLimit = limit;

//                for (int i = start; i < start + limit; i++)
//                {
//                    if (_loadedStatuses.Count <= i || !_loadedStatuses[i] && !_loadingIndexes.Contains(i))
//                    {
//                        newStart = i;
//                        break;
//                    }
//                }

//                if (newStart == -1)
//                {
//                    start = 0;
//                    limit = 0;

//                    return;
//                }

//                for (int i = start + limit; i > newStart; i--)
//                {
//                    if (_loadedStatuses.Count <= i || !_loadedStatuses[i] && !_loadingIndexes.Contains(i))
//                    {
//                        newLimit = i - newStart;
//                        break;
//                    }
//                }

//                start = newStart;
//                limit = newLimit;
//            }
//        }
//    }
//}
