﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading.Loaders
{
    public class BaseCollectionLoader2 : ICollectionLoader
    {
        private const int REPLACE_BATCH_SIZE = 12;
        private const int INFLATE_RATE = 20;

        private const int BATCH_DELAY = 50;

        private ObservableCollection<object> _observableCollection;
        private IForklift _forklift;
        public virtual async Task ReplaceItemsAsync(int start, object[] objs, CancellationToken token)
        {
            try
            {
                Console.WriteLine("PutRange{0}-{1}", start, start + objs.Length);
                //inflate up to beginning of loaded items
                await InflateToAsync(start - 1, token).ConfigureAwait(false);

                if (token.IsCancellationRequested) return;
                await PutItemsAsync(start, objs, token).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Console.WriteLine(GetType().Name + " exception {0}", e.Message);
            }
        }

        public async Task InflateToAsync(int targetCount, CancellationToken token)
        {
            if (token.IsCancellationRequested) return;
            while (_observableCollection.Count < targetCount)
            {
                int target = Math.Min(targetCount, _observableCollection.Count + INFLATE_RATE);
                InflateTo(target, token);
                await Task.Delay(BATCH_DELAY).ConfigureAwait(false);
                if (token.IsCancellationRequested) return;
            }
        }

        private async Task PutItemsAsync(int start, object[] objs, CancellationToken token)
        {
            int batchIndex = 0;
            for (int i = 0; i < objs.Length; i++)
            {
                if (token.IsCancellationRequested) return;
                _forklift.AddOrReplaceItem(start + i, objs[i], _observableCollection);
                //OnItemLoaded(start + i, (T)ObservableCollection[start + i]);

                batchIndex++;
                if (batchIndex > REPLACE_BATCH_SIZE)
                {
                    batchIndex = 0;
                    await Task.Delay(BATCH_DELAY).ConfigureAwait(false);
                }
            }
        }


        private void InflateTo(int targetCount, CancellationToken token)
        {
            lock (_observableCollection)
            {
                if (token.IsCancellationRequested) return;
                while (_observableCollection.Count < targetCount)
                {
                    _observableCollection.Add(null);
                }
            }
        }
    }
}
