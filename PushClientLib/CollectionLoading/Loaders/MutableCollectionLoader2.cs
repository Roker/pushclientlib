﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Collections.Specialized;
//using System.Text;

//namespace PushClientLib.CollectionLoading.Loaders
//{
//    public class MutableCollectionLoader2<T> : BaseCollectionLoaderDecorator<T>, IMutableCollectionLoader<T>
//    {
//        private MutableForklift _forklift;

//        public MutableCollectionLoader2(ICollectionLoader<T> collectionLoader) : base(collectionLoader)
//        {
//            Forklift = collectionLoader.Forklift;

//            ObservableCollection = base.ObservableCollection;
//            base.ObservableCollection = new ObservableCollection<object>();
//            base.ObservableCollection.CollectionChanged += ObservableCollection_CollectionChanged;
//        }

//        public override IForklift Forklift {
//            get => _forklift;
//            set {
//                _forklift = new MutableForklift(value);
//                base.Forklift = _forklift;
//            }
//        }


//        private void ObservableCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
//        {
//            if(e.Action == NotifyCollectionChangedAction.Add)
//            {
//                _observableCollection.Insert(_forklift.TrueIndex(e.NewStartingIndex), e.NewItems[0]);
//            }
//            else if (e.Action == NotifyCollectionChangedAction.Replace)
//            {
//                _observableCollection[_forklift.TrueIndex(e.NewStartingIndex)] = e.NewItems[0];
//            }
//        }

//        private ObservableCollection<object> _observableCollection;

//        public override ObservableCollection<object> ObservableCollection {
//            get => _observableCollection;
//            set => _observableCollection = value;
//        }

//        public event NotifyCollectionChangedEventHandler CollectionChanged;

//        public virtual void Add(object item)
//        {
//            lock (ObservableCollection)
//            {
//                ObservableCollection.Add(item);
//                CollectionChanged?.Invoke(this,
//                   new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, ObservableCollection.Count - 1));
//            }
//        }


//        public virtual void Insert(int index, object item)
//        {
//            lock (ObservableCollection)
//            {
//                ObservableCollection.Insert(index, item);
//                _forklift.InsertedIndexes.Add(index);

//                CollectionChanged?.Invoke(this,
//                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
//            }
//        }

//        public virtual void Move(int oldIndex, int newIndex)
//        {
//            lock (ObservableCollection)
//            {
//                object item = ObservableCollection[oldIndex];

//                ObservableCollection.Move(oldIndex, newIndex);

//                CollectionChanged?.Invoke(this,
//                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, item, newIndex, oldIndex));
//            }
//        }
//        public virtual void Remove(object item)
//        {
//            lock (ObservableCollection)
//            {
//                int index = ObservableCollection.IndexOf(item);
//                if (index >= 0)
//                {
//                    ObservableCollection.RemoveAt(index);
//                    _forklift.RemovedIndexes.Add(index);

//                    CollectionChanged?.Invoke(this,
//                  new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
//                }
//            }
//        }
//    }
//}
