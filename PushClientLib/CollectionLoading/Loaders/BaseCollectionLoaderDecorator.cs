﻿//using System.Collections.ObjectModel;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public abstract class BaseCollectionLoaderDecorator<T> : ICollectionLoader<T>
//    {
//        private ICollectionLoader<T> _baseCollectionLoader;

//        public virtual IForklift Forklift { get => _baseCollectionLoader.Forklift; set => _baseCollectionLoader.Forklift = value; }
//        public virtual ObservableCollection<object> ObservableCollection { get => _baseCollectionLoader.ObservableCollection; set => _baseCollectionLoader.ObservableCollection = value; }
//        public IRangeLoader<T> RangeLoader { get => _baseCollectionLoader.RangeLoader; set => _baseCollectionLoader.RangeLoader = value; }

//        public event ItemLoadedEventHandler ItemLoaded {
//            add {
//                _baseCollectionLoader.ItemLoaded += value;
//            }

//            remove {
//                _baseCollectionLoader.ItemLoaded -= value;
//            }
//        }

//        public virtual Task InflateToAsync(int targetCount, CancellationToken token)
//        {
//            return _baseCollectionLoader.InflateToAsync(targetCount, token);
//        }

//        public virtual Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            return _baseCollectionLoader.LoadRangeAsync(start, limit, token);
//        }

//        protected BaseCollectionLoaderDecorator(ICollectionLoader<T> collectionLoader)
//        {
//            _baseCollectionLoader = collectionLoader;
//        }
//    }
//}