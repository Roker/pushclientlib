﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public class ObservableCollectionLoader<T> : IObservableCollectionLoader<T> where T : class
//    {
//        private const int REPLACE_BATCH_SIZE = 12;
//        private const int INFLATE_RATE = 12;

//        private const int BATCH_DELAY = 100;

//        public ObservableCollection<object> ObservableCollection { get; set; }
//        public IRangeLoader<T> RangeLoader { get; set; }

//        public event ItemLoadedEventHandler ItemLoaded;

//        private Task _initTask;

//        private int _total;


//        public ObservableCollectionLoader(
//            IRangeLoader<T> rangeLoader,
//            ObservableCollection<object> observableCollection)
//        {
//            RangeLoader = rangeLoader;
//            ObservableCollection = observableCollection;

//            _initTask = Task.Run(InitAsync);
//        }

//        private async Task InitAsync()
//        {
//            _total = await RangeLoader.GetTotalAsync().ConfigureAwait(false);
//        }

//        public virtual async Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            try
//            {
//                await _initTask;

//                if (token.IsCancellationRequested) return;
//                start = Math.Max(0, start);
//                limit = Math.Min(_total - start, limit);

//                //load items
//                Task<List<T>> loadTask = RangeLoader.LoadRange(start, limit);

//                //inflate up to beginning of loaded items
//                Task inflateTask = InflateToAsync(start - 1, token);

//                Task[] tasks = new Task[]
//                {
//                loadTask,
//                inflateTask
//                };

//                await Task.WhenAll(tasks).ConfigureAwait(false);

//                List<T> ts = loadTask.Result;
//                if (token.IsCancellationRequested) return;
//                await ReplaceItemsAsync(start, ts.ToArray(), token).ConfigureAwait(false);
//            }
//            catch (Exception e)
//            {
//                Console.WriteLine(GetType().Name + " exception {0}", e.Message);
//            }
//        }

//        public async Task InflateToAsync(int targetCount, CancellationToken token)
//        {
//            await _initTask;
//            if (token.IsCancellationRequested) return;
//            while (ObservableCollection.Count < targetCount)
//            {
//                int target = Math.Min(targetCount, ObservableCollection.Count + INFLATE_RATE);
//                InflateTo(target, token);
//                await Task.Delay(BATCH_DELAY).ConfigureAwait(false);
//                if (token.IsCancellationRequested) return;
//            }
//        }

//        private async Task ReplaceItemsAsync(int start, T[] ts, CancellationToken token)
//        {
//            int batchIndex = 0;
//            for (int i = 0; i < ts.Length; i++)
//            {
//                AddOrReplaceItem(start + i, ts[i], token);
//                batchIndex++;
//                if (batchIndex > REPLACE_BATCH_SIZE)
//                {
//                    batchIndex = 0;
//                    await Task.Delay(BATCH_DELAY).ConfigureAwait(false);
//                    if (token.IsCancellationRequested) return;
//                }
//            }
//        }

//        protected virtual void AddOrReplaceItem(int index, T t, CancellationToken token)
//        {
//            lock (ObservableCollection)
//            {
//                if (token.IsCancellationRequested) return;
//                if (ObservableCollection.Count == index)
//                {
//                    ObservableCollection.Add(t);
//                }
//                else
//                {
//                    ObservableCollection[index] = t;
//                }
//                OnItemLoaded(index, t);
//            }
//        }

//        protected virtual void OnItemLoaded(int index, T t)
//        {
//            ItemLoaded?.Invoke(this, new ItemLoadedEventArgs(index, t));
//        }

//        private void InflateTo(int targetCount, CancellationToken token)
//        {
//            lock (ObservableCollection)
//            {
//                if (token.IsCancellationRequested) return;
//                while (ObservableCollection.Count < targetCount)
//                {
//                    ObservableCollection.Add(null);
//                }
//            }
//        }
//    }
//}
