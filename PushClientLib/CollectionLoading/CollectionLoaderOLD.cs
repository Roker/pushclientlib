﻿//using Roker.Extensions;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Collections.Specialized;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public class CollectionLoader<T> : BaseCollectionLoader<T> where T : class
//    {
//        private const int REVERSE_BUFFER_SIZE = 10;
//        private const int BUFFER_SIZE = 40;

//        private int _scrollDir = 1;

//        public override IRangeLoader<T> RangeLoader {
//            set {
//                base.RangeLoader = value;
//                _rangeLoader = value;
//                ReInit();
//            }
//        }

//        private IRangeLoader<T> _rangeLoader;

//        public override ReadOnlyObservableCollection<object> Items => new ReadOnlyObservableCollection<object>(_items);
//        public override int FirstVisibleIndex { get; set; }
//        public override int VisibleItemCount { get; set; } = 10;

//        private CancellationTokenSource _itemLoadingCancellationTokenSource = new CancellationTokenSource();

//        private int _total;


//        public CollectionLoader()
//        {
//        }
//        public CollectionLoader(IRangeLoader<T> rangeLoader)
//        {
//            RangeLoader = rangeLoader;
//        }

//        private void StartInit()
//        {
//            _itemLoadingCancellationTokenSource = new CancellationTokenSource();
//            Task.Run(() => InitAsync(_itemLoadingCancellationTokenSource.Token));
//        }

//        private async Task InitAsync(CancellationToken token)
//        {
//            _total = await _rangeLoader.GetTotalAsync().ConfigureAwait(false);
//            StartLoadTask(token);
//        }

//        private void StartLoadTask(CancellationToken token)
//        {
//            Task.Run(() => InflateRangeAsync(_total, token));
//            Task.Run(() => LoadTask(token));
//        }

//        private async Task LoadTask(CancellationToken token)
//        {
//            Console.WriteLine("load task");
//            while (!token.IsCancellationRequested)
//            {
//                try
//                {
//                    await LoadNearbyPagesAsync(_scrollDir, token).ConfigureAwait(false);
//                    //await InflateToBufferAsync(token).ConfigureAwait(false);
//                    await Task.Delay(500).ConfigureAwait(false);
//                }catch(Exception e)
//                {
//                    Console.WriteLine("load excp: " + e.Message);
//                }
               
//            }
//            Console.WriteLine("load cancelled ");
//        }
//        protected Task LoadNearbyPagesAsync(int scrollDir, CancellationToken token)
//        {
//            List<Task> loadTasks = new List<Task>();
//            loadTasks.Add(LoadVisibleItemsAsync(token));
//            loadTasks.Add(LoadBufferItemsAsync(scrollDir, token));
//            return Task.WhenAll(loadTasks);
//        }

//        protected override Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            start = Math.Max(0, start);
//            limit = Math.Min(_total - start, limit);
//            return base.LoadRangeAsync(start, limit, token);
//        }

//        private Task LoadVisibleItemsAsync(CancellationToken token)
//        {
//            Console.WriteLine(GetHashCode() + " - FirstVis: " + FirstVisibleIndex);
//            return LoadRangeAsync(FirstVisibleIndex, VisibleItemCount, token);
//        }


//        //load items after lastvisibleindex 
//        //limit based on scroll directions
//        private Task LoadBufferItemsAsync(int scrollDir, CancellationToken token)
//        {
//            int start = FirstVisibleIndex + VisibleItemCount;

//            // ---------------------------------------------------- limit based on scroll direction
//            int limit = scrollDir == 1 ? BUFFER_SIZE : REVERSE_BUFFER_SIZE;
//            return LoadRangeAsync(start, limit, token);
//        }



//        private void ReInit()
//        {
//            Console.WriteLine("reinit");
//            _itemLoadingCancellationTokenSource?.Cancel();
//            StartInit();
//        }

     

//        public override void Reset()
//        {
//            throw new NotImplementedException();
//        }

//        public override void Dispose()
//        {
//            _itemLoadingCancellationTokenSource?.Cancel();
//        }

//        public override void Stop()
//        {
//            _itemLoadingCancellationTokenSource?.Cancel();
//        }

//        public override void Resume()
//        {
//            _itemLoadingCancellationTokenSource = new CancellationTokenSource();
//            StartLoadTask(_itemLoadingCancellationTokenSource.Token);
//        }
//    }
//}
