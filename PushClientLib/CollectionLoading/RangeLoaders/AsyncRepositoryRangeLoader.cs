﻿using PushClientLib.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading
{
    public class AsyncRepositoryRangeLoader<T> : IRangeLoader<T>
    {
        private IAsyncRepository<T> _repo;

        public AsyncRepositoryRangeLoader(IAsyncRepository<T> repo)
        {
            _repo = repo;
        }

        public Task<int> GetTotalAsync()
        {
            return _repo.GetTotalAsync();
        }

        public Task<IEnumerable<T>> LoadRange(int offset, int limit)
        {
            return _repo.GetRangeAsync(offset, limit);
        }
    }
}
