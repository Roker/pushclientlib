﻿using System.Collections.Specialized;

namespace PushClientLib.CollectionLoading
{
    public interface IDynamicRangeLoader<T> : IRangeLoader<T>
    {
        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e);
    }
}
