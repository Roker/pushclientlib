﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace PushClientLib.CollectionLoading
{
    //i dont think this makes sense or is necessary
    //public interface ICollectionLoaderController
    //{
    //    ICollectionLoader CollectionLoader { get; set; }
    //}

    public interface ICollectionLoaderController : IDisposable
    {
        event ItemLoadedEventHandler ItemLoaded;

        IForklift Forklift { get; set; }
        ObservableCollection<object> ObservableCollection { get; set; }
        ICollectionLoaderStrategy CollectionLoaderStrategy { get; set; }
        bool IsRunning { get; }

        void Start();
        void Stop();
        void Reset();
    }
    public interface ICollectionLoaderController<T> : ICollectionLoaderController, INotifyCollectionChanged, IDisposable
    {
        IRangeLoader<T> RangeLoader { get; set; }
        Func<T, bool> FilterFunc { get; set; }
    }
    public delegate void ItemLoadedEventHandler(object sender, ItemLoadedEventArgs e);

    public class ItemLoadedEventArgs
    {
        public ItemLoadedEventArgs(int index, object item)
        {
            Index = index;
            Item = item;
        }

        public int Index { get; set; }
        public object Item { get; set; }
    }

    //    public interface ICursorCollectionLoaderController<T> : ICollectionLoaderController<T>
    //    {
    //        int FirstVisibleIndex { get; set; }
    //        int VisibleItemCount { get; set; }
    //    }
}
