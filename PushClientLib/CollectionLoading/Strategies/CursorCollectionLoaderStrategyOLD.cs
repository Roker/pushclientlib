﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public class CursorCollectionLoaderStrategy : ICollectionLoaderStrategy
//    {
//        public int CursorIndex { get; set; }

//        public int Buffer { get; set; } = 50;
//        public int ReverseBuffer { get; set; } = 10;


//        private ICollectionLoader _collectionLoader;

//        public void StartLoading(ICollectionLoader collectionLoader, CancellationToken token)
//        {
//            _collectionLoader = collectionLoader;
//            Task.Run(() => LoadTask(token));
//        }

//        private async Task LoadTask(CancellationToken token)
//        {
//            Console.WriteLine("load task");
//            while (!token.IsCancellationRequested)
//            {
//                try
//                {
//                    //Console.WriteLine("Load Cancelled:  " + token.IsCancellationRequested);
//                    //Console.WriteLine("Cursor Index:  " + CursorIndex);
  
//                    await _collectionLoader.LoadRangeAsync(
//                        CursorIndex - ReverseBuffer, ReverseBuffer + Buffer, token).ConfigureAwait(false);
//                    //await InflateToBufferAsync(token).ConfigureAwait(false);
//                    await Task.Delay(500).ConfigureAwait(false);
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine("load exception");
//                    Console.WriteLine("load excp: " + e.Message);
//                }

//            }

//            Console.WriteLine("load task cancelled:" + token.IsCancellationRequested);

//        }
//    }
//}
