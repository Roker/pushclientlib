﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading
{
    public interface ICollectionLoaderStrategy
    {
        Func<int, int, CancellationToken, Task> LoadRangeFunc { get; set; }

        void StartLoading(CancellationToken token);
    }

    public delegate void RequestLoadRangeEventHandler(object sender, RequestLoadRangeEventArgs e);

    public class RequestLoadRangeEventArgs : EventArgs
    {
        public RequestLoadRangeEventArgs(int start, int limit, CancellationToken token)
        {
            Start = start;
            Limit = limit;
            Token = token;
        }

        public int Start { get; set; }
        public int Limit { get; set; }
        public CancellationToken Token { get; set; }
    }
}