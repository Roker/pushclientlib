﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading.Strategies
{
    public class CursorCollectionLoaderStrategy : ICollectionLoaderStrategy
    {
        private const int DELAY = 1000;
        public int CursorIndex { get; set; }

        public int Buffer { get; set; } = 50;
        public int ReverseBuffer { get; set; } = 10;
        public Func<int, int, CancellationToken, Task> LoadRangeFunc { get; set; }


        public void StartLoading(CancellationToken token)
        {
            Task.Run(() => LoadTask(token), token);
        }

        private async Task LoadTask(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                await LoadRangeFunc
                    .Invoke(CursorIndex - ReverseBuffer, ReverseBuffer + Buffer, token)
                    .ConfigureAwait(false);
                await Task.Delay(DELAY);
            }
        }
    }
}
