﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public abstract class BaseCollectionLoadingStrategy<T> : ICollectionLoaderStrategy
//    {
//        private ICollectionLoader<T> _collectionLoader;

//        protected int _total;

//        protected BaseCollectionLoadingStrategy(ICollectionLoader<T> collectionLoader)
//        {
//            _collectionLoader = collectionLoader;
//        }

//        protected Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            start = Math.Max(0, start);
//            limit = Math.Min(_total - start, limit);
//            return _collectionLoader.LoadRangeAsync(start, limit, token);
//        }

//        public abstract void StartLoading(ICollectionLoader collectionLoader, CancellationToken token);
//    }
//}
