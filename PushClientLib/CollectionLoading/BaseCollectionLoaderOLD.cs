﻿//using Roker.Extensions;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Collections.Specialized;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    //handles the loading of items, tracking loaded/loading statuses of items
//    //to prevent duplicate item loading
//    public abstract class BaseCollectionLoader<T> : ICollectionLoader<T> where T : class
//    {
//        //indexes of items currently being loaded
//        private HashSet<int> _loadingIndexes = new HashSet<int>();

//        private List<bool> _loadedStatuses = new List<bool>();
//        protected ObservableCollection<object> _items = new ObservableCollection<object>();

//        //loader - need
//        private ObservableCollectionLoader<T> _collectionLoader;

//        public BaseCollectionLoader()
//        {
//        }
//        public BaseCollectionLoader(IRangeLoader<T> rangeLoader)
//        {
//            RangeLoader = rangeLoader;
//        }

//        public virtual IRangeLoader<T> RangeLoader {
//            set {
//                if(_collectionLoader != null)
//                {
//                    _collectionLoader.ItemLoaded -= OnItemLoaded;
//                }
//                _collectionLoader = new ObservableCollectionLoader<T>(value, _items);
//                if (_collectionLoader != null)
//                {
//                    _collectionLoader.ItemLoaded += OnItemLoaded;
//                }
//            }
//        }

//        public bool IsCollectionReady { get; set; }
//        public ReadOnlyObservableCollection<object> Items => new ReadOnlyObservableCollection<object>(_items);

//        IRangeLoader<T> ICollectionLoader<T>.RangeLoader { get; set; }
//        public ObservableCollection<object> ObservableCollection { get; set; }

//        public event NotifyCollectionChangedEventHandler CollectionChanged;
//        public event ItemLoadedEventHandler ItemLoaded;

//        protected Task InflateRangeAsync(int limit, CancellationToken token)
//        {
//            return _collectionLoader.InflateToAsync(limit, token);
//        }

//        protected async virtual Task LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            GetRangeAvoidingLoadedItems(ref start, ref limit);
//            if (limit == 0) return;

//            Console.WriteLine("LoadRange: {0}-{1}", start, start + limit - 1);
//            SetLoading(start, limit);

//            InflateLoadedStatus(start);

//            await _collectionLoader.LoadRangeAsync(start, limit, token).ConfigureAwait(false);

//            SetLoading(start, limit, false);
//        }

//        private void InflateLoadedStatus(int start)
//        {
//            lock (_loadedStatuses)
//            {
//                while(_loadedStatuses.Count < start)
//                {
//                    _loadedStatuses.Add(false);
//                }
//            }
//        }

//        private void SetLoaded(int index)
//        {
//            lock (_loadedStatuses)
//            {
//                if (_loadedStatuses.Count == index)
//                {
//                    _loadedStatuses.Add(true);
//                }
//                else
//                {
//                    _loadedStatuses[index] = true;
//                }
//            }
//        }
//        private void SetLoading(int start, int limit, bool loading = true)
//        {
//            lock (_loadingIndexes)
//            {
//                for (int i = start; i < start + limit; i++)
//                {
//                    if (loading)
//                    {
//                        _loadingIndexes.Add(i);
//                    }
//                    else
//                    {
//                        _loadingIndexes.Remove(i);
//                    }
//                }

//            }
//        }

//        private void OnItemLoaded(object sender, ItemLoadedEventArgs e)
//        {
//            SetLoaded(e.Index);
//            ItemLoaded?.Invoke(this, e);
//        }


//        private void GetRangeAvoidingLoadedItems(ref int start, ref int limit)
//        {
//            lock (_loadedStatuses)
//            {
//                int newStart = -1;
//                int newLimit = limit;

//                for (int i = start; i < start + limit; i++)
//                {
//                    if (_loadedStatuses.Count <= i || !_loadedStatuses[i] && !_loadingIndexes.Contains(i))
//                    {
//                        newStart = i;
//                        break;
//                    }
//                }

//                if (newStart == -1)
//                {
//                    start = 0;
//                    limit = 0;

//                    return;
//                }

//                for (int i = start + limit; i > newStart; i--)
//                {
//                    if (_loadedStatuses.Count <= i || !_loadedStatuses[i] && !_loadingIndexes.Contains(i))
//                    {
//                        newLimit = i - newStart;
//                        break;
//                    }
//                }

//                start = newStart;
//                limit = newLimit;
//            }
//        }

//        public void SetItems(ObservableCollection<object> items)
//        {
//            _items = items;
//        }
//        public abstract void Reset();
//        public abstract void Dispose();
//        public abstract void Stop();
//        public abstract void Resume();

//        public Task InflateToAsync(int targetCount, CancellationToken token)
//        {
//            throw new NotImplementedException();
//        }

//        Task ICollectionLoader.LoadRangeAsync(int start, int limit, CancellationToken token)
//        {
//            throw new NotImplementedException();
//        }
//    }
//}
