﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading
{
    public interface ICollectionLoader
    {
        //IForklift Forklift { get; set; }
        //ObservableCollection<object> ObservableCollection { get; set; }

        //event ItemLoadedEventHandler ItemLoaded;

        Task InflateToAsync(int targetCount, CancellationToken token);
        Task ReplaceItemsAsync(int start, object[] objs, CancellationToken token);
        //Task LoadRangeAsync(int start, int limit, CancellationToken token);
    }

    //public delegate void ItemLoadedEventHandler(object sender, ItemLoadedEventArgs e);


    //public interface ICollectionLoader<T> : ICollectionLoader
    //{
    //    IRangeLoader<T> RangeLoader { get; set; }
    //}

    //public class ItemLoadedEventArgs
    //{
    //    public ItemLoadedEventArgs(int index, object item)
    //    {
    //        Index = index;
    //        Item = item;
    //    }

    //    public int Index { get; set; }
    //    public object Item { get; set; }
    //}
}
