﻿using System.Collections.ObjectModel;

namespace PushClientLib.CollectionLoading
{
    //NEEDS TO BE RENAMED
    //defines placing item for collection loaders
    public interface IForklift
    {
        event ItemLoadedEventHandler ItemLoaded;

        void AddOrReplaceItem(int index, object t, ObservableCollection<object> observableCollection);
    }
}
