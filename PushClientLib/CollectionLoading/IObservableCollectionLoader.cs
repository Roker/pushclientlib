﻿using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading
{
    public interface IObservableCollectionLoader<T>
    {
        ObservableCollection<object> ObservableCollection { get; set; }
        IRangeLoader<T> RangeLoader { get; set; }

        event ItemLoadedEventHandler ItemLoaded;

        Task InflateToAsync(int targetCount, CancellationToken token);
        Task LoadRangeAsync(int start, int limit, CancellationToken token);
    }

    //public delegate void ItemLoadedEventHandler(object sender, ItemLoadedEventArgs e);

    //public class ItemLoadedEventArgs
    //{
    //    public ItemLoadedEventArgs(int index, object item)
    //    {
    //        Index = index;
    //        Item = item;
    //    }

    //    public int Index { get; set; }
    //    public object Item { get; set; }
    //}
}