﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading
{
    public interface IRangeLoader<T>
    {

        Task<int> GetTotalAsync();
        Task<IEnumerable<T>> LoadRange(int offset, int limit);

        //event EventHandler<NotifyCollectionChangedEventArgs> CollectionChanged;

        //void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs);
    }
}
