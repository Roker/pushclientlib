﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading
//{
//    public class CollectionLoaderController<T> : ICollectionLoaderController<T>
//    {
//        private ICollectionLoader<T> _collectionLoader;
//        private CancellationTokenSource _cancellationTokenSource;


//        public ICollectionLoader<T> CollectionLoader { 
//            get => _collectionLoader;
//            set {
//                _collectionLoader = value;
//            }
//        }

//        public ICollectionLoaderStrategy CollectionLoaderStrategy { get; set; }


//        private async Task InitAsync(CancellationToken token)
//        {
//            Console.WriteLine("Controller init: ");

//            CollectionLoaderStrategy.StartLoading(CollectionLoader, token);
//            await InflateAllAsync(token).ConfigureAwait(false);
//        }

//        private async Task InflateAllAsync(CancellationToken token)
//        {
//            Console.WriteLine("inflate all");
//            await Task.Delay(1000).ConfigureAwait(false);
//            Console.WriteLine("inflate all 2");

//            int total = await CollectionLoader.RangeLoader.GetTotalAsync().ConfigureAwait(false);
//            Console.WriteLine("controller total: " + total);
//            await CollectionLoader.InflateToAsync(total, token).ConfigureAwait(false);
//        }

//        public void Start()
//        {
//            Stop();
//            Console.WriteLine("Start");
//            _cancellationTokenSource = new CancellationTokenSource();
//            _ = Task.Run(() => InitAsync(_cancellationTokenSource.Token), _cancellationTokenSource.Token);
//        }

//        public void Stop()
//        {
//            Console.WriteLine("Stop");
//            _cancellationTokenSource?.Cancel();
//        }

//        public void Reset()
//        {
//            Console.WriteLine("Reset");

//            Stop();
//            CollectionLoader.ObservableCollection.Clear();
//            Start();
//        }

//        public void Dispose()
//        {
//            Stop();
//        }
//    }
//}
