﻿//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading.Controllers
//{
//    public class MutableCollectionLoaderController<T> : IMutableCollectionLoaderController<T>
//    {
//        private ICollectionLoaderController<T> _baseCollectionLoaderController;

//        private CancellationTokenSource _loadingCancellationTokenSource;
//        private CancellationTokenSource _inflatingCancellationTokenSource;


//        private int _collectionTotalAdjustment;


//        private IMutableCollectionLoader<T> _collectionLoader;

//        public event NotifyCollectionChangedEventHandler CollectionChanged;



//        public ICollectionLoaderStrategy CollectionLoaderStrategy {
//            get => _baseCollectionLoaderController.CollectionLoaderStrategy;
//            set => _baseCollectionLoaderController.CollectionLoaderStrategy = value;
//        }
//        public ICollectionLoader<T> CollectionLoader { 
//            get => _collectionLoader;
//            set {
//                if(_collectionLoader != null)
//                {
//                    _collectionLoader.CollectionChanged -= CollectionLoader_CollectionChanged;
//                }
//                _collectionLoader = new MutableCollectionLoader<T>(value);
//                _baseCollectionLoaderController.CollectionLoader = _collectionLoader;
//                if (_collectionLoader != null)
//                {
//                    _collectionLoader.CollectionChanged += CollectionLoader_CollectionChanged;
//                }
//            }
//        }

//        public MutableCollectionLoaderController(ICollectionLoaderController<T> baseCollectionLoaderController)
//        {
//            _baseCollectionLoaderController = baseCollectionLoaderController;
//            CollectionLoader = baseCollectionLoaderController.CollectionLoader;
//        }

//        public void Start()
//        {
//            Stop();
//            Console.WriteLine("Start");
//            _loadingCancellationTokenSource = new CancellationTokenSource();
//            _ = Task.Run(() => InitAsync(_loadingCancellationTokenSource.Token), _loadingCancellationTokenSource.Token);
//        }

//        public void Stop()
//        {
//            Console.WriteLine("Stop");
//            _loadingCancellationTokenSource?.Cancel();
//        }

//        public void Reset()
//        {
//            Console.WriteLine("Reset");

//            Stop();
//            CollectionLoader.ObservableCollection.Clear();
//            Start();
//        }

//        public void Dispose()
//        {
//            Stop();
//        }

//        public void Add(object item)
//        {
//            _collectionLoader.Add(item);
//        }

//        public void Insert(int index, object item)
//        {
//            _collectionLoader.Insert(index, item);
//        }

//        public void Remove(object item)
//        {
//            _collectionLoader.Remove(item);
//        }

//        public void Move(int oldIndex, int newIndex)
//        {
//            _collectionLoader.Move(oldIndex, newIndex);
//        }

//        private void CollectionLoader_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
//        {
//            if (e.Action == NotifyCollectionChangedAction.Add
//                || e.Action == NotifyCollectionChangedAction.Remove)
//            {
//                //cancel previous inflate
//                _inflatingCancellationTokenSource.Cancel();

//                if (e.Action == NotifyCollectionChangedAction.Add)
//                {
//                    Console.WriteLine("Collection changed add");
//                    _collectionTotalAdjustment++;
//                }
//                else if (e.Action == NotifyCollectionChangedAction.Remove)
//                {
//                    _collectionTotalAdjustment--;
//                }

//                //start new inflate
//                StartInflate(_loadingCancellationTokenSource.Token);
//            }
//        }

//        private async Task InitAsync(CancellationToken token)
//        {
//            CollectionLoaderStrategy.StartLoading(CollectionLoader, token);
//            StartInflate(token);
//        }

//        private void StartInflate(CancellationToken token)
//        {
//            _inflatingCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(token);
//            //possible Task.Run in Task.Run with initasync
//            Task.Run(() => InflateAllAsync(_inflatingCancellationTokenSource.Token), _inflatingCancellationTokenSource.Token);
//        }

//        private async Task InflateAllAsync(CancellationToken token)
//        {
//            await Task.Delay(1000).ConfigureAwait(false);
//            int total = await CollectionLoader.RangeLoader.GetTotalAsync().ConfigureAwait(false);
//            await CollectionLoader.InflateToAsync(total + _collectionTotalAdjustment, token).ConfigureAwait(false);
//        }


//    }
//}
