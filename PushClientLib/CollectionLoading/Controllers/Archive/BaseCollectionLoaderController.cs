﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace PushClientLib.CollectionLoading.Controllers
//{
//    internal abstract class BaseCollectionLoaderController<T> 
//        : ICollectionLoaderController<T>,
//        ICollectionLoaderControllerInter<T>
//    {
//        private ICollectionLoaderStrategy _collectionLoaderStrategy;

//        private CancellationTokenSource _cancellationTokenSource;

//        private IForklift _forklift = new BaseForklift();

//        public event ItemLoadedEventHandler ItemLoaded;

//        internal event EventHandler InitDone;
//        internal event EventHandler<RangeLoadEventArgs> RangeLoadStart;
//        internal event EventHandler<RangeLoadEventArgs> RangeLoadCancelled;

//        protected int RangeLoaderTotal { get; private set; }

//        public IRangeLoader<T> RangeLoader { get; set; }
//        public IForklift Forklift {
//            get => _forklift;
//            set {
//                if (_forklift != null)
//                {
//                    _forklift.ItemLoaded -= Forklift_ItemLoaded;
//                }
//                _forklift = value;
//                if (_forklift != null)
//                {
//                    _forklift.ItemLoaded += Forklift_ItemLoaded;
//                }
//            }
//        }

//        private void Forklift_ItemLoaded(object sender, ItemLoadedEventArgs e)
//        {
//            ItemLoaded?.Invoke(this, e);
//        }

//        public ObservableCollection<object> ObservableCollection { get; set; }

//        public ICollectionLoaderStrategy CollectionLoaderStrategy {
//            get => _collectionLoaderStrategy;
//            set {
//                _collectionLoaderStrategy = value;
//                if (_collectionLoaderStrategy != null)
//                {
//                    _collectionLoaderStrategy.LoadRangeFunc = LoadAndPutRange;
//                }
//            }
//        }

//        private async Task InitAsync(CancellationToken token)
//        {
//            Console.WriteLine("Controller init: ");
//            RangeLoaderTotal = await RangeLoader.GetTotalAsync().ConfigureAwait(false);
//            token.ThrowIfCancellationRequested();
//            CollectionLoaderStrategy.StartLoading(token);
//        }

//        private void PutRange(int start, List<T> items)
//        {
//            for (int i = 0; i < items.Count; i++)
//            {
//                int index = start + i;
//                Put(index, items[i]);
//            }
//        }

//        private async Task LoadAndPutRange(int start, int limit, CancellationToken token)
//        {
//            AdjustRange(ref start, ref limit);

//            if (limit == 0) return;

//            List<T> items = await LoadItems(start, limit, token).ConfigureAwait(false);

//            Console.WriteLine("LoadRange3: {0}", items?.Count);

//            PutRange(start, items);
//        }


//        public async Task<List<T>> LoadItems(int start, int limit, CancellationToken token)
//        {
//            List<T> items = await RangeLoader.LoadRange(start, limit).ConfigureAwait(false);
//            token.ThrowIfCancellationRequested();
//            return items;
//        }

//        public virtual void AdjustRange(ref int start, ref int limit)
//        {
//            start = Math.Max(0, start);
//            limit = Math.Min(RangeLoaderTotal - start, limit);
//        }

//        public virtual void Put(int index, object item)
//        {
//            Forklift.AddOrReplaceItem(index, item, ObservableCollection);
//        }

//        public void Start()
//        {
//            Stop();
//            Console.WriteLine("Start");
//            _cancellationTokenSource = new CancellationTokenSource();
//            _ = Task.Run(() => InitAsync(_cancellationTokenSource.Token), _cancellationTokenSource.Token);
//        }

//        public void Stop()
//        {
//            Console.WriteLine("Stop");
//            _cancellationTokenSource?.Cancel();
//        }

//        public void Reset()
//        {
//            Console.WriteLine("Reset");

//            Stop();
//            ObservableCollection.Clear();
//            Start();
//        }

//        public void Dispose()
//        {
//            Stop();
//        }

 

//        internal class RangeLoadEventArgs : EventArgs
//        {
//            internal int Start { get; private set; }
//            internal int Limit { get; private set; }

//            public RangeLoadEventArgs(int start, int limit)
//            {
//                Start = start;
//                Limit = limit;
//            }
//        }
//    }
//}
