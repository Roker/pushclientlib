﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading.Controllers
{
    internal class CollectionInflaterFuncs
    {
        private const int DEFAULT_INFLATE_RATE = 40;

        private const int DEFAULT_BATCH_DELAY = 100;
        internal static async Task InflateToAsync(int targetCount,
            ObservableCollection<object> observableCollection,
            CancellationToken token,
            int inflateRate = DEFAULT_INFLATE_RATE,
            int delay = DEFAULT_BATCH_DELAY)
        {
            if (token.IsCancellationRequested) return;
            while (observableCollection.Count < targetCount)
            {
                int target = Math.Min(targetCount, observableCollection.Count + inflateRate);
                InflateTo(target, observableCollection, token);
                await Task.Delay(delay).ConfigureAwait(false);
                if (token.IsCancellationRequested) return;
            }
        }

        private static void InflateTo(int targetCount,
            ObservableCollection<object> observableCollection,
            CancellationToken token)
        {
            lock (observableCollection)
            {
                if (token.IsCancellationRequested) return;
                while (observableCollection.Count < targetCount)
                {
                    observableCollection.Add(null);
                }
            }
        }
    }
}
