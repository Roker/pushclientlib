﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading.Controllers
{
    interface ICollectionLoaderHelper<T>
    {
        Task<List<T>> LoadRange(int start, int limit, CancellationToken token);
        void Put(int index, T item);
    }
}
