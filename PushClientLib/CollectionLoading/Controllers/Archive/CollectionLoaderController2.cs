﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading.Controllers
{
    public class CollectionLoaderController2<T> : ICollectionLoaderController<T>
    {
        private ICollectionLoaderStrategy _collectionLoaderStrategy;

        private CancellationTokenSource _cancellationTokenSource;

        private int _total;

        private LoadedStatusManager _loadedStatusManager = new LoadedStatusManager();

        private IForklift _forklift = new BaseForklift();

        public event ItemLoadedEventHandler ItemLoaded;

        public IRangeLoader<T> RangeLoader { get; set; }
        public IForklift Forklift { 
            get => _forklift;
            set {
                if (_forklift != null)
                {
                    _forklift.ItemLoaded -= Forklift_ItemLoaded;
                }
                _forklift = value;
                if (_forklift != null)
                {
                    _forklift.ItemLoaded += Forklift_ItemLoaded;
                }
            }
        }

        private void Forklift_ItemLoaded(object sender, ItemLoadedEventArgs e)
        {
            ItemLoaded?.Invoke(this, e);
        }

        public ObservableCollection<object> ObservableCollection { get; set; }

        public ICollectionLoaderStrategy CollectionLoaderStrategy {
            get => _collectionLoaderStrategy;
            set {
                _collectionLoaderStrategy = value;
                if (_collectionLoaderStrategy != null)
                {
                    _collectionLoaderStrategy.LoadRangeFunc = LoadRange;
                }
            }
        }

        private async Task InitAsync(CancellationToken token)
        {
            Console.WriteLine("Controller init: ");
            _total = await RangeLoader.GetTotalAsync().ConfigureAwait(false);
            token.ThrowIfCancellationRequested();
            CollectionLoaderStrategy.StartLoading(token);
            await CollectionInflaterFuncs.InflateToAsync(_total, ObservableCollection, token).ConfigureAwait(false);
        }


        private async Task LoadRange(int start, int limit, CancellationToken token)
        {
            Console.WriteLine("LoadRange: {0}-{1}", start, start + limit);

            start = Math.Max(0, start);
            limit = Math.Min(_total - start, limit);
            _loadedStatusManager.GetRangeAvoidingLoadedItems(ref start, ref limit);
            _loadedStatusManager.SetLoading(start, limit);

            Console.WriteLine("LoadRange2: {0}-{1}", start, start + limit);

            if (limit == 0) return;
            List<T> items = await LoadItems(start, limit).ConfigureAwait(false);

            Console.WriteLine("LoadRange3: {0}", items?.Count);

            if (token.IsCancellationRequested)
            {
                _loadedStatusManager.SetLoading(start, limit, false);
                return;
            }
            PutItems(start, items);
            
            _loadedStatusManager.SetLoaded(start, limit);
        }

        private void PutItems(int start, List<T> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                int index = start + i;
                PutItem(index, items[i]);
            }
        }

        protected virtual async Task<List<T>> LoadItems(int start, int limit)
        {
            return await RangeLoader.LoadRange(start, limit).ConfigureAwait(false);
        }

        protected virtual void PutItem(int index, T item)
        {
            Forklift.AddOrReplaceItem(index, item, ObservableCollection);
        }


        public void Start()
        {
            Stop();
            Console.WriteLine("Start");
            _cancellationTokenSource = new CancellationTokenSource();
            _ = Task.Run(() => InitAsync(_cancellationTokenSource.Token), _cancellationTokenSource.Token);
        }

        public void Stop()
        {
            Console.WriteLine("Stop");
            _cancellationTokenSource?.Cancel();
        }

        public void Reset()
        {
            Console.WriteLine("Reset");

            Stop();
            ObservableCollection.Clear();
            Start();
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
