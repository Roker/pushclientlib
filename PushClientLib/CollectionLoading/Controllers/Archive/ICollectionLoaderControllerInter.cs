﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading.Controllers
{
    internal interface ICollectionLoaderControllerInter<T>
    {
        void AdjustRange(ref int start, ref int limit);
        Task<List<T>> LoadItems(int start, int limit, CancellationToken token);
        void Put(int index, object item);
        void StartLoading(CancellationToken token);
    }
}