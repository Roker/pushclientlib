﻿using System;
using System.Collections.Generic;

namespace PushClientLib.CollectionLoading.Controllers
{
    internal class LoadedStatusManager
    {
        private HashSet<int> _loadingIndexes = new HashSet<int>();

        private List<bool> _loadedStatuses = new List<bool>();

        internal void SetLoaded(int start, int limit, bool loaded = true)
        {
            lock (_loadedStatuses)
            {
                InflateLoadedStatusesTo(start);
                for (int i = start; i < start + limit; i++)
                {
                    if (_loadedStatuses.Count == i)
                    {
                        _loadedStatuses.Add(loaded);
                    }
                    else
                    {
                        _loadedStatuses[i] = loaded;
                    }
                }
            }
        }
        internal void SetLoading(int start, int limit, bool loading = true)
        {
            lock (_loadingIndexes)
            {
                for (int i = start; i < start + limit; i++)
                {
                    if (loading)
                    {
                        _loadingIndexes.Add(i);
                    }
                    else
                    {
                        _loadingIndexes.Remove(i);
                    }
                }
            }
        }

        internal void GetRangeAvoidingLoadedItems(ref int start, ref int limit)
        {
            start = Math.Max(0, start);

            lock (_loadedStatuses)
            {
                int newStart = -1;
                int newLimit = limit;

                for (int i = start; i < start + limit; i++)
                {
                    if (_loadedStatuses.Count <= i || !_loadedStatuses[i] && !_loadingIndexes.Contains(i))
                    {
                        newStart = i;
                        break;
                    }
                }

                if (newStart == -1)
                {
                    start = 0;
                    limit = 0;

                    return;
                }

                for (int i = start + limit; i > newStart; i--)
                {
                    if (_loadedStatuses.Count <= i || !_loadedStatuses[i] && !_loadingIndexes.Contains(i))
                    {
                        newLimit = i - newStart;
                        break;
                    }
                }

                start = newStart;
                limit = newLimit;
            }
        }

        private void InflateLoadedStatusesTo(int start)
        {
            lock (_loadedStatuses)
            {
                while (_loadedStatuses.Count < start)
                {
                    _loadedStatuses.Add(false);
                }
            }
        }

        internal void Clear()
        {
            lock (_loadedStatuses)
            {
                _loadedStatuses.Clear();
            }
            lock (_loadingIndexes)
            {
                _loadingIndexes.Clear();
            }
        }
    }
}
