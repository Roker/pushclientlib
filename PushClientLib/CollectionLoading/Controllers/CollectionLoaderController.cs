﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PushClientLib.CollectionLoading.Controllers
{
    //very bloated
    //had trouble splitting into smaller components
    public class CollectionLoaderController<T> : ICollectionLoaderController<T>
    {
        //items inflated per batch
        private const int DEFAULT_INFLATE_RATE = 40;

        //delay between batches
        private const int DEFAULT_BATCH_DELAY = 100;

        //amount past highest loaded index to inflate
        private const int INFLATE_BUFFER = 500;

        private SortedSet<int> _removedIndexes { get; set; } = new SortedSet<int>();
        private SortedDictionary<int, int> _insertedCounts { get; set; } = new SortedDictionary<int, int>();

        private ICollectionLoaderStrategy _collectionLoaderStrategy;

        private CancellationTokenSource _cancellationTokenSource;
        private CancellationTokenSource _inflateCancellationTokenSource;

        private int _total;

        private LoadedStatusManager _loadedStatusManager = new LoadedStatusManager();

        private IForklift _forklift = new BaseForklift();
        private ObservableCollection<object> _observableCollection;

        public event ItemLoadedEventHandler ItemLoaded;

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private Task _inflateTask;

        public IRangeLoader<T> RangeLoader { get; set; }

        public Func<T, bool> FilterFunc { get; set; }

        private bool _listeningToChanges = true;
        public IForklift Forklift {
            get => _forklift;
            set {
                if (_forklift != null)
                {
                    _forklift.ItemLoaded -= Forklift_ItemLoaded;
                }
                _forklift = value;
                if (_forklift != null)
                {
                    _forklift.ItemLoaded += Forklift_ItemLoaded;
                }
            }
        }

        private void Forklift_ItemLoaded(object sender, ItemLoadedEventArgs e)
        {
            ItemLoaded?.Invoke(this, e);
        }

        public ObservableCollection<object> ObservableCollection {
            get => _observableCollection;
            set {
                if (_observableCollection != null)
                {
                    _observableCollection.CollectionChanged -= ObservableCollection_CollectionChanged;
                }
                _observableCollection = value;
                if (_observableCollection != null)
                {
                    _observableCollection.CollectionChanged += ObservableCollection_CollectionChanged;
                }
                Reset();
            }
        }

        private void ObservableCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            lock (ObservableCollection)
            {
                if (_listeningToChanges)
                {
                    CollectionChanged?.Invoke(this, e);
                    if (e.Action == NotifyCollectionChangedAction.Add)
                    {
                        OnItemInserted(e.NewStartingIndex);
                    }
                    else if (e.Action == NotifyCollectionChangedAction.Remove)
                    {
                        OnItemRemoved(e.OldStartingIndex);
                    }
                }
            }
        }

        public ICollectionLoaderStrategy CollectionLoaderStrategy {
            get => _collectionLoaderStrategy;
            set {
                _collectionLoaderStrategy = value;
                if (_collectionLoaderStrategy != null)
                {
                    _collectionLoaderStrategy.LoadRangeFunc = LoadRange;
                }
            }
        }

        public bool IsRunning { get; private set; } = false;

        private async Task InitAsync(CancellationToken token)
        {
            Console.WriteLine("Controller init: ");
            _total = await RangeLoader.GetTotalAsync().ConfigureAwait(false);
            token.ThrowIfCancellationRequested();
            CollectionLoaderStrategy.StartLoading(token);
        }

        private async Task LoadRange(int start, int limit, CancellationToken token)
        {
            CheckInflateTask(start, token);

            int rangeLoaderStart, rangeLoaderEnd;
            GetItemPos(start, out rangeLoaderStart);
            GetItemPos(start + limit, out rangeLoaderEnd);

            start = rangeLoaderStart;
            limit = rangeLoaderEnd - rangeLoaderStart;

            start = Math.Max(0, start);
            limit = Math.Min(_total - start, limit);

            _loadedStatusManager.GetRangeAvoidingLoadedItems(ref start, ref limit);

            if (limit == 0) return;


            _loadedStatusManager.SetLoading(start, limit);

            IEnumerable<T> items = await LoadItems(start, limit);

            Console.WriteLine("LoadRange: {0}", items?.Count());

            if (token.IsCancellationRequested)
            {
                _loadedStatusManager.SetLoading(start, limit, false);
                return;
            }
            PutItems(start, items);

            _loadedStatusManager.SetLoaded(start, limit);
        }

        private void CheckInflateTask(int index, CancellationToken token)
        {
            if (_inflateTask == null || _inflateTask.Status != TaskStatus.Running)
            {
                _inflateCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(token);
                int target = Math.Min(_total - _removedIndexes.Count, index + INFLATE_BUFFER);
                _inflateTask = InflateToAsync(target, _inflateCancellationTokenSource.Token);
            }
        }

        protected virtual async Task<IEnumerable<T>> LoadItems(int start, int limit)
        {
            return await RangeLoader.LoadRange(start, limit).ConfigureAwait(false);
        }
        private void PutItems(int start, IEnumerable<T> items)
        {
            int filteredCount = 0;
            lock (ObservableCollection)
            {
                int i = 0;
                foreach (T t in items)
                {
                    int rangeLoaderIndex = start + i;
                    if (_removedIndexes.Contains(rangeLoaderIndex)) continue;


                    _listeningToChanges = false;

                    try
                    {

                        Console.WriteLine("put item: " + (start + i));
                        if (FilterFunc != null && FilterFunc.Invoke(t))
                        {
                            //item filtered
                            OnItemRemoved(IndexFromRangeLoaderIndex(rangeLoaderIndex));
                            filteredCount++;
                        }
                        else
                        {
                            Forklift.AddOrReplaceItem(
                                IndexFromRangeLoaderIndex(rangeLoaderIndex),
                                t,
                                ObservableCollection);
                        }
                    }
                    catch { }

                    _listeningToChanges = true;
                    i++;
                }

                //remove empty indexes if items have been filtered
                for (int j = 1; j <= filteredCount; j++)
                {
                    ObservableCollection.RemoveAt(start + items.Count() - j);
                }
            }

        }

        public void Start()
        {
            if (IsRunning)
            {
                Stop();
            }
            Console.WriteLine("Start");
            _cancellationTokenSource = new CancellationTokenSource();
            _ = Task.Run(() => InitAsync(_cancellationTokenSource.Token), _cancellationTokenSource.Token);
            IsRunning = true;
        }

        public void Stop()
        {
            IsRunning = false;
            Console.WriteLine("Stop");
            _cancellationTokenSource?.Cancel();
        }

        public void Reset()
        {
            Console.WriteLine("Reset");
            bool wasRunning = IsRunning;
            Stop();
            lock (ObservableCollection)
            {
                ObservableCollection.Clear();
                _removedIndexes.Clear();
                _insertedCounts.Clear();
                _loadedStatusManager.Clear();
            }

            if (wasRunning)
            {
                Start();
            }
        }

        public void Dispose()
        {
            Stop();
        }

        private int IndexFromRangeLoaderIndex(int rangeLoaderIndex)
        {
            lock (_removedIndexes)
            {
                int index = rangeLoaderIndex;
                foreach (int i in _insertedCounts.Keys)
                {
                    if (i > rangeLoaderIndex)
                    {
                        break;
                    }
                    index += _insertedCounts[i];
                }
                foreach (int i in _removedIndexes)
                {
                    if (i > rangeLoaderIndex)
                    {
                        break;
                    }
                    index--;
                }
                return index;
            }
        }

        private void GetItemPos(int index, out int rangeLoaderIndex)
        {
            int sinceRangeLoaderItem;
            ItemPos(index, out rangeLoaderIndex, out sinceRangeLoaderItem);
        }
        private void ItemPos(int index, out int rangeLoaderIndex, out int sinceRangeLoaderItem)
        {
            lock (_removedIndexes)
            {
                rangeLoaderIndex = -1;
                sinceRangeLoaderItem = 0;
                for (int i = 0; i <= index; i++)
                {
                    if (_insertedCounts.ContainsKey(rangeLoaderIndex)
                        && sinceRangeLoaderItem < _insertedCounts[rangeLoaderIndex])
                    {
                        sinceRangeLoaderItem++;
                    }
                    else
                    {
                        rangeLoaderIndex++;
                        sinceRangeLoaderItem = 0;
                        while (_removedIndexes.Contains(rangeLoaderIndex))
                        {
                            rangeLoaderIndex++;
                        }
                    }
                }
            }
        }
        private void OnItemRemoved(int index)
        {
            _inflateCancellationTokenSource.Cancel();
            lock (_removedIndexes)
            {
                Console.WriteLine("onitemremoved:" + index);
                int rangeLoaderIndex, sinceRangeLoaderItem;
                //get nearest (above) rangeloader index, and position below if index is inserted
                ItemPos(index, out rangeLoaderIndex, out sinceRangeLoaderItem);

                //if is inserted item
                if (sinceRangeLoaderItem > 0)
                {
                    //lower inserted item count for rangeloader item above
                    int curCount;
                    //add inserted count from removed item, to the item above
                    _insertedCounts.TryGetValue(rangeLoaderIndex, out curCount);
                    if (curCount == 1)
                    {
                        _insertedCounts.Remove(rangeLoaderIndex);
                    }
                    else
                    {
                        _insertedCounts[rangeLoaderIndex] = _insertedCounts[rangeLoaderIndex] - 1;
                    }
                }
                //if rangeloader item
                else
                {
                    //add to removed indexes
                    _removedIndexes.Add(rangeLoaderIndex);

                    //if has inserted items below
                    if (_insertedCounts.ContainsKey(rangeLoaderIndex))
                    {
                        int newIndex = rangeLoaderIndex - 1;
                        //find nearest rangeloader item above
                        while (_removedIndexes.Contains(newIndex))
                        {
                            newIndex--;
                        }
                        int curCount;
                        //add inserted count from removed item, to the item above
                        _insertedCounts.TryGetValue(newIndex, out curCount);
                        curCount += _insertedCounts[rangeLoaderIndex];
                        _insertedCounts[newIndex] = curCount;

                        //remove old count
                        _insertedCounts.Remove(rangeLoaderIndex);
                    }
                }
            }
        }

        private void OnItemInserted(int index)
        {
            _inflateCancellationTokenSource.Cancel();
            int rangeLoaderIndex;
            //get nearest (above) rangeloader index 
            // we dont care about position inside other inserted indexes
            GetItemPos(index, out rangeLoaderIndex);

            int count;
            _insertedCounts.TryGetValue(rangeLoaderIndex, out count);
            count++;
            _insertedCounts[rangeLoaderIndex] = count;
        }


        private async Task InflateToAsync(int targetCount,
                    CancellationToken token)
        {
            if (token.IsCancellationRequested) return;
            while (ObservableCollection.Count < targetCount)
            {
                int target = Math.Min(targetCount, ObservableCollection.Count + DEFAULT_INFLATE_RATE);
                InflateTo(target, token);
                await Task.Delay(DEFAULT_BATCH_DELAY).ConfigureAwait(false);
                if (token.IsCancellationRequested) return;
            }
        }

        private void InflateTo(int targetCount,
            CancellationToken token)
        {
            lock (ObservableCollection)
            {
                if (token.IsCancellationRequested) return;
                _listeningToChanges = false;
                try
                {
                    while (ObservableCollection.Count < targetCount)
                    {
                        ObservableCollection.Add(null);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("InflateTo exc" + e.Message);
                }
                _listeningToChanges = true;
            }
        }
    }
}