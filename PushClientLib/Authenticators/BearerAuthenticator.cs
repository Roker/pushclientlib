﻿using RestSharp;
using RestSharp.Authenticators;

namespace PushClientLib.Authenticators
{
    public class BearerAuthenticator : IAuthenticator
    {
        public const string AUTH_HEADER_KEY = "Authorization";
        public const string BEARER_TOKEN_PREFIX = "Bearer ";

        private readonly string _token;
        public BearerAuthenticator(string token)
        {
            _token = token;
        }
        public void Authenticate(IRestClient client, IRestRequest request)
        {
            request.AddHeader(AUTH_HEADER_KEY, BEARER_TOKEN_PREFIX + _token);
        }
    }
}
