﻿using PushClientLib.Models;
using PushClientLib.Web.Models;
using PushClientLib.Web.Requests;
using PushClientLib.Web.Requests.Builders;
using PushClientLib.Web.Rest.Requests;
using RokerPrime.Util;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace PushClientLib.Tests.Rest
{
    public class AlbumListRestExecutorTests : IClassFixture<RestTestsFixture>
    {
        private readonly RestTestsFixture _fixture;
        private readonly ITestOutputHelper _output;

        public AlbumListRestExecutorTests(RestTestsFixture fixture, ITestOutputHelper output)
        {
            _fixture = fixture;
            _output = output;
        }

        /*TODO:
         * -error handling
         * -more detailed asserts/testing
         *      -make sure every variable is saved correctly etc
         */
        [Fact]
        public async Task TestAdd()
        {
            AlbumList addedList = await AddNewListAsync().ConfigureAwait(false);
            Assert.Equal(addedList.UserId, RestTestsFixture.PUSH_USER_ID);

            PushRequest<AlbumList> getRequest = _fixture.RequestBuilder.GetRequest(addedList.Id);
            PushResponse<AlbumList> getResponse = await _fixture.RequestExecutor.ExecuteAsync(getRequest);


            Assert.Equal(HttpStatusCode.OK, getResponse.StatusCode);
            
            Assert.True(getResponse.IsSuccessful);
            Assert.Equal(addedList, getResponse.Data);
        }

        [Fact]
        public async Task TestUpdate()
        {
            AlbumList addedList = await AddNewListAsync().ConfigureAwait(false);
            string newName = StringUtil.RandomString(10);
            addedList.Name = newName;

            Assert.Equal(0, addedList.Version);

            PushRequest<AlbumList> updateRequest = _fixture.RequestBuilder.UpdateRequest(addedList);
            PushResponse<AlbumList> updateResponse = await _fixture.RequestExecutor.ExecuteAsync(updateRequest).ConfigureAwait(false);

            Assert.Equal(HttpStatusCode.OK, updateResponse.StatusCode);
            Assert.Equal(1, updateResponse.Data.Version);
            Assert.True(updateResponse.IsSuccessful);
            Assert.Equal(newName, updateResponse.Data.Name);

            PushRequest<AlbumList> getRequest = _fixture.RequestBuilder.GetRequest(addedList.Id);
            PushResponse<AlbumList> getResponse = await _fixture.RequestExecutor.ExecuteAsync(getRequest).ConfigureAwait(false);

            Assert.Equal(newName, getResponse.Data.Name);
        }

        [Fact]
        public async Task TestUpdatingOldVersion()
        {
            AlbumList addedList = await AddNewListAsync().ConfigureAwait(false);
            string newName = StringUtil.RandomString(10);
            addedList.Name = newName;
            Assert.Equal(0, addedList.Version);

            PushRequest<AlbumList> updateRequest = _fixture.RequestBuilder.UpdateRequest(addedList);
            PushResponse<AlbumList> updateResponse = await _fixture.RequestExecutor.ExecuteAsync(updateRequest).ConfigureAwait(false);

            Assert.Equal(HttpStatusCode.OK, updateResponse.StatusCode);
            Assert.True(updateResponse.IsSuccessful);
            Assert.Equal(1, updateResponse.Data.Version);
            Assert.Equal(newName, updateResponse.Data.Name);

            PushRequest<AlbumList> secondUpdateRequest = _fixture.RequestBuilder.UpdateRequest(addedList);
            PushResponse<AlbumList> secondUpdateResponse = await _fixture.RequestExecutor.ExecuteAsync(secondUpdateRequest).ConfigureAwait(false);

            Assert.False(secondUpdateResponse.IsSuccessful);
            Assert.Equal(HttpStatusCode.Conflict, secondUpdateResponse.StatusCode);
        }

        [Fact]
        public async Task TestDelete()
        {
            AlbumList addedList = await AddNewListAsync().ConfigureAwait(false);

            PushRequest deleteRequest = _fixture.RequestBuilder.DeleteRequest(addedList);
            PushResponse deleteResponse = await _fixture.RequestExecutor.ExecuteAsync(deleteRequest).ConfigureAwait(false);

            Assert.Equal(HttpStatusCode.OK, deleteResponse.StatusCode);

            PushRequest<AlbumList> getRequest = _fixture.RequestBuilder.GetRequest(addedList.Id);
            PushResponse<AlbumList> getResponse = await _fixture.RequestExecutor.ExecuteAsync(getRequest).ConfigureAwait(false);

            Assert.False(getResponse.IsSuccessful);
        }

        [Fact]
        public async Task AddAlreadyExisting()
        {
            AlbumList added = await AddNewListAsync().ConfigureAwait(false);

            PushRequest<AlbumList> addRequest = _fixture.RequestBuilder.AddRequest(added);
            PushResponse<AlbumList> addResponse = await _fixture.RequestExecutor.ExecuteAsync(addRequest).ConfigureAwait(false);

            Assert.Equal(HttpStatusCode.Conflict, addResponse.StatusCode);
        }

        protected async Task<AlbumList> AddNewListAsync()
        {
            AlbumList newList = GenerateNewAlbumList();
            PushRequest<AlbumList> addRequest = _fixture.RequestBuilder.AddRequest(newList);
            PushResponse<AlbumList> addResponse = await _fixture.RequestExecutor.ExecuteAsync(addRequest).ConfigureAwait(false);

            Assert.True(addResponse.IsSuccessful);
            Assert.Equal(HttpStatusCode.OK, addResponse.StatusCode);
            return addResponse.Data;
        }

        protected static AlbumList GenerateNewAlbumList()
        {
            AlbumList newList = new AlbumList();
            newList.Uuid = Guid.NewGuid().ToString();
            newList.Created = DateTime.UtcNow;
            newList.Updated = DateTime.UtcNow;
            return newList;
        }
    }

    public class RestTestsFixture : IDisposable
    {
        public const string API_URL = "http://localhost:8080/push-music/api/v1/";

        public const string PUSH_AUTH_TOKEN = "XbNR0m6VPM5HAQ2Tf40X3XSb";
        public const string PUSH_USER_ID = "0eCx25";

        internal IUserEntityRequestBuilder<AlbumList> RequestBuilder => requestBuilder;

        internal IRequestExecutor RequestExecutor => _requestExecutor;

        private readonly IUserData _userData = new UserData()
        {
            AuthToken = PUSH_AUTH_TOKEN,
            User = new User() { Id = PUSH_USER_ID }
        };

        private readonly IRequestExecutor _requestExecutor;
        private readonly IUserEntityRequestBuilder<AlbumList> requestBuilder
            = UserEntityRequestBuilderFactory.NewUserEntityRequestBuilder<AlbumList>();

        public RestTestsFixture()
        {
            _requestExecutor = new RestExecutor(API_URL);
            _requestExecutor.UserData = _userData;
        }



        public void Dispose()
        {
        }
    }
}
